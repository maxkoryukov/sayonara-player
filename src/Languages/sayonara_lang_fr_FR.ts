<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr_FR" sourcelanguage="en" version="2.1">
<context>
    <name>GUI_AlternativeCovers</name>
    <message>
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.ui" line="+27"/>
        <source>Online Search</source>
        <translation>Rechercher en ligne</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Automatic search</source>
        <translation>Recherche automatique</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Text search</source>
        <translation>Recherche textuelle</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Server</source>
        <translation>Serveur</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Local Search</source>
        <translation>Recherche locale</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Select and preview multiple covers</source>
        <translation>Sélectionner et prévisualiser plusieurs pochettes</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Alternative Cover</source>
        <translation>Pochette alternative</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Start search automatically</source>
        <translation>Démarrer la recherche automatiquement</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.cpp" line="+283"/>
        <source>%n cover(s) found</source>
        <translation><numerusform>%n pochette trouvée</numerusform><numerusform>%n pochettes trouvées</numerusform></translation>
    </message>
    <message>
        <location line="+133"/>
        <location line="+31"/>
        <source>Also save cover to %1</source>
        <translation>Enregistrer aussi la pochette dans %1</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Cover web search is not enabled</source>
        <translation>La recherche de pochette sur le Web est désactivée</translation>
    </message>
</context>
<context>
    <name>GUI_History</name>
    <message>
        <location filename="../src/Gui/History/GUI_History.ui" line="+14"/>
        <source>Dialog</source>
        <translation>Dialogue</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Newer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Older</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.ui" line="+20"/>
        <source>Info / Edit</source>
        <translation>Info / Modifier</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Loading files...</source>
        <translation>Chargement des fichiers...</translation>
    </message>
</context>
<context>
    <name>GUI_Lyrics</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.ui" line="+28"/>
        <source>Save Lyrics</source>
        <translation>Enregistrer les paroles dans le ficher</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>switch</source>
        <translation>Basculer</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.cpp" line="+303"/>
        <source>Save lyrics not supported</source>
        <translation>Enregistrement des paroles non supporté</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Overwrite lyrics</source>
        <translation>Écraser les paroles</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save lyrics</source>
        <translation>Enregistrer les paroles</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
</context>
<context>
    <name>GUI_ImportDialog</name>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.ui" line="+14"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Import tracks to library</source>
        <translation>Importer des pistes vers la bibliothèque</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select target folder</source>
        <translation>Sélectionnez le dossier cible</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.cpp" line="+126"/>
        <source>Loading tracks</source>
        <translation>Chargement des pistes</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No tracks</source>
        <translation>Aucune piste</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Importing</source>
        <translation>Importation en cours</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Rollback</source>
        <translation>Annulation</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cancelled</source>
        <translation>Annulé</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Choose target directory</source>
        <translation>Choisissez le dossier cible</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>%1&lt;br /&gt;is no library directory</source>
        <translation>%1&lt;br /&gt; n&apos;est pas un dossier de la bibliothèque</translation>
    </message>
</context>
<context>
    <name>GUI_LocalLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.ui" line="+17"/>
        <source>Library</source>
        <translation>Bibliothèque</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.cpp" line="+352"/>
        <source>Audio files</source>
        <translation>Fichiers audio</translation>
    </message>
</context>
<context>
    <name>GUI_Controls</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Controls.ui" line="+318"/>
        <source>Written by Michael Lugmair (Lucio Carreras)</source>
        <translation>Écrit par Michael Lugmair (Lucio Carreras)</translation>
    </message>
</context>
<context>
    <name>GUI_Player</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Player.ui" line="+14"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
</context>
<context>
    <name>GUI_AudioConverter</name>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.ui" line="+45"/>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="+277"/>
        <source>Start</source>
        <translation>Démarrer</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Stop</source>
        <translation>Arrêt</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>#Threads</source>
        <translation>#Threads</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+187"/>
        <source>Quality</source>
        <translation>Qualité</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+52"/>
        <location line="+89"/>
        <source>Bitrate</source>
        <translation>Débit</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="-164"/>
        <source>Audio Converter</source>
        <translation>Convertisseur audio</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Threads</source>
        <translation>Threads</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+123"/>
        <source>Cannot find encoder</source>
        <translation>Encodeur introuvable</translation>
    </message>
    <message>
        <location line="-113"/>
        <location line="+14"/>
        <source>Playlist does not contain tracks which are supported by the converter</source>
        <translation>La liste de lecture ne contient aucune piste prise en charge par le convertisseur.</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>No track will be converted.</source>
        <translation>Aucune piste ne sera convertie.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>These tracks will be ignored</source>
        <translation>Ces pistes seront ignorées.</translation>
    </message>
    <message numerus="yes">
        <location line="+38"/>
        <source>Failed to convert %n track(s)</source>
        <translation><numerusform>Échec de la conversion de %n pistes</numerusform><numerusform>Échec de la conversion de %n piste</numerusform></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please check the log files</source>
        <translation>Veuillez consulter les fichiers journaux</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>All tracks could be converted</source>
        <translation>Toutes les pistes ont été converties</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully finished</source>
        <translation>Terminé avec succès</translation>
    </message>
</context>
<context>
    <name>GUI_Bookmarks</name>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.ui" line="+116"/>
        <source>Loop</source>
        <translation>En boucle</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.cpp" line="+77"/>
        <location line="+53"/>
        <source>No bookmarks found</source>
        <translation>Aucun signet trouvé</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Sorry, bookmarks can only be set for library tracks at the moment.</source>
        <translation>La création de signets n&apos;est possible que dans la bibliothèque pour l&apos;instant</translation>
    </message>
</context>
<context>
    <name>GUI_Broadcast</name>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.ui" line="+88"/>
        <source>Cannot Broadcast</source>
        <translation>La radiodiffusion n&apos;a pas pu démarrer</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.cpp" line="+104"/>
        <source>Dismiss</source>
        <translation>Rejeter</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dismiss all</source>
        <translation>Tout rejeter</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%n listener(s)</source>
        <translation><numerusform>%n auditeurs</numerusform><numerusform>%n auditeur</numerusform></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot broadcast on port %1</source>
        <translation>La radiodiffusion n&apos;a pas pu démarrer sur le port %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maybe another application is using this port?</source>
        <translation>Une autre application utilise-t-elle ce port ?</translation>
    </message>
</context>
<context>
    <name>GUI_Speed</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.ui" line="+48"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+134"/>
        <source>Speed</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Preserve pitch</source>
        <translation>Préserver la tonalité</translation>
    </message>
    <message>
        <location line="+30"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+1"/>
        <source>Pitch</source>
        <translation>Tonalité</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 et %2</translation>
    </message>
    <message>
        <location line="+70"/>
        <location line="+1"/>
        <source>%1 not found</source>
        <translation>%1 non trouvé(es)</translation>
    </message>
</context>
<context>
    <name>GUI_Style</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Style.ui" line="+14"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Spectrum</source>
        <translation>Spectre</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Vert. spacing</source>
        <translation>Espacement vertical</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+88"/>
        <source>Rect height</source>
        <translation>Altitude de rectangle</translation>
    </message>
    <message>
        <location line="-68"/>
        <location line="+44"/>
        <source>Hor. spacing</source>
        <translation>Espacement horizontal</translation>
    </message>
    <message>
        <location line="-30"/>
        <location line="+75"/>
        <source>Fading steps</source>
        <translation>Étapes de transition (fading)</translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Level</source>
        <translation>Niveau</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Rect width</source>
        <translation>Largeur de rectangle</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ver. spacing</source>
        <translation>Espacement vertical</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Style settings</source>
        <translation>Paramètres du style</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Color 2</source>
        <translation>Couleur 2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 1</source>
        <translation>Couleur 1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color 3</source>
        <translation>Couleur 3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 4</source>
        <translation>Couleur 4</translation>
    </message>
</context>
<context>
    <name>GUI_TargetPlaylistDialog</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_TargetPlaylistDialog.ui" line="+14"/>
        <source>Choose target playlist</source>
        <translation>Choissez la liste de lecture cible</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>relative filepaths</source>
        <translation>chemins relatifs</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save playlist as...</source>
        <translation>Enregistrer la liste de lecture sous...</translation>
    </message>
</context>
<context>
    <name>GUI_StationSearcher</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.ui" line="+14"/>
        <source>Search Radio Station</source>
        <translation>Rechercher une station de radio</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.cpp" line="+58"/>
        <source>Show radio stations from %1 to %2</source>
        <translation>Affichage des stations de radio %1 à %2</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Country</source>
        <translation>Pays</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Url</source>
        <translation>URL</translation>
    </message>
</context>
<context>
    <name>GUI_BroadcastPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.ui" line="+20"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ask for permission</source>
        <translation>Demander la permission</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.cpp" line="+161"/>
        <source>Port %1 already in use</source>
        <translation>Le port %1 est déjà utilisé</translation>
    </message>
</context>
<context>
    <name>GUI_CoverPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Covers/GUI_CoverPreferences.ui" line="+62"/>
        <source>Inactive</source>
        <translation>Inactif</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Delete all covers from the database</source>
        <translation>Supprimer toutes les pochettes de la base de données</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear cache</source>
        <translation>Effacer le cache</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete all covers from the Sayonara directory</source>
        <translation>Supprimer toutes les pochettes du répertoire Sayonara</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete files</source>
        <translation>Supprimer les fichiers</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Save found covers into the library directory where the audio files are located</source>
        <translation>Enregistrer les pochettes trouvées dans le répertoire de la bibliothèque où se trouvent les fichiers audio</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers into library</source>
        <translation>Enregistrer les pochettes trouvées dans la bibliothèque</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fetch missing covers from the internet</source>
        <translation>Récupérer les pochettes manquantes sur Internet</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Saving covers to the database leads to significantly faster access but results in a bigger database</source>
        <translation>Enregistrer les pochettes dans la base de données permet un accès beaucoup plus rapide, mais alourdit la base de données.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers to database</source>
        <translation>Enregistrer les pochettes trouvées dans la base de données</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save found covers into Sayonara directory</source>
        <translation>Enregistrer les pochettes trouvées dans le répertoire Sayonara</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cover name</source>
        <translation>Nom de la pochette</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name of cover file</source>
        <translation>Nom du fichier de pochette</translation>
    </message>
</context>
<context>
    <name>GUI_LanguagePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.ui" line="+29"/>
        <source>Check for update</source>
        <translation>Rechercher des mises à jour</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Download update</source>
        <translation>Télécharger la mise à jour</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>For new languages I am always looking for translators</source>
        <translation>Je recherche toujours des traducteurs pour d&apos;autres langues</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.cpp" line="+63"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Cannot check for language update</source>
        <translation>Impossible de rechercher une mise à jour de la langue</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Language update available</source>
        <translation>Mise à jour disponible pour votre langue</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Language is up to date</source>
        <translation>Votre langue est à jour</translation>
    </message>
    <message>
        <location line="+41"/>
        <location line="+24"/>
        <source>Cannot fetch language update</source>
        <translation>Impossible de récupérer la mise à jour de la langue</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Language was updated successfully</source>
        <translation>La langue a été mise à jour avec succès</translation>
    </message>
</context>
<context>
    <name>GUI_LastFmPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.ui" line="+27"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Scrobble time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>Login now</source>
        <translation>Se connecter maintenant</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.cpp" line="+163"/>
        <source>Logged in</source>
        <translation>Connecté</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not logged in</source>
        <translation>Non connecté</translation>
    </message>
</context>
<context>
    <name>GUI_LibraryPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.ui" line="+27"/>
        <source>Libraries</source>
        <translation>Bibliothèques</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Library-Playlist Interaction</source>
        <translation>Interaction entre bibliothèque et liste de lecture</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>When drag and drop into playlist </source>
        <translation>Quand glissé-déposé dans la liste de lecture</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+36"/>
        <source>do nothing (default)</source>
        <translation>ne rien faire ( défaut )</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>start if stopped and playlist is empty</source>
        <translation>démarrer la lecture si le lecteur est arrêté et si la liste de lecture est vide</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>On double click, create playlist and</source>
        <translation>Au double clic, créer une liste de lecture et</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start playback if stopped</source>
        <translation>démarrer la lecture si le lecteur est arrêté</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>start playback immediately</source>
        <translation>démarrer la lecture immédiatement</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(this is ignored when playlist is in &apos;append mode&apos;)</source>
        <translation>(ignoré lorsque la liste de lecture est en mode « ajouter à la suite »)</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Other</source>
        <translation>Autre</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show &quot;Clear selection&quot; buttons</source>
        <translation>Afficher les boutons pour « effacer la sélection »</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Ignore English article &quot;The&quot; in artist name</source>
        <translation>Ignorer l&apos;article anglais « The » dans le nom de l&apos;artiste</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.cpp" line="+147"/>
        <source>Cannot edit library</source>
        <translation>La bibliothèque n&apos;a pas pu être modifiée</translation>
    </message>
</context>
<context>
    <name>GUI_NotificationPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.ui" line="+17"/>
        <source>Timeout (ms)</source>
        <translation>Délai ( ms ) </translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.cpp" line="+98"/>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
</context>
<context>
    <name>GUI_PlayerPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.ui" line="+44"/>
        <source>Show system tray icon</source>
        <translation>Afficher l&apos;icône dans la zone de notifications</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Hide instead of close</source>
        <translation>Minimiser au lieu de fermer</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Start hidden</source>
        <translation>Minimiser au démarrage</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update notifications</source>
        <translation>Notification de mises à jour</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.cpp" line="+107"/>
        <source>This might cause Sayonara not to show up again.</source>
        <translation>Sayonara pourrait ne plus s’afficher.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>In this case use the &apos;--show&apos; option at the next startup.</source>
        <translation>Si cela se produit, utilisez l’option « --show » au prochain démarrage.</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.ui" line="+36"/>
        <source>Behavior</source>
        <translation>Comportement</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start up</source>
        <translation>Démarrage</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load temporary playlists</source>
        <translation>Charger les listes de lecture temporaires</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load saved playlists</source>
        <translation>Charger les listes de lecture enregistrées</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start playing</source>
        <translation>Démarrer la lecture</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load last track on startup</source>
        <translation>Charger la dernière piste lue au démarage</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remember time of last track</source>
        <translation>Mémoriser la position de lecture</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stop behaviour</source>
        <translation>Comportement de l&apos;action &apos;stop&apos;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load last track after pressing stop</source>
        <translation>Charger la dernière piste après l&apos;appui sur stop</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Look</source>
        <translation>Apparence</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show numbers</source>
        <translation>Afficher les numéros</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>&apos;italic text&apos;</source>
        <translation>&apos;texte italique&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Example</source>
        <translation>Exemple</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>*bold text*</source>
        <translation>*texte gras*</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Show clear button</source>
        <translation>Afficher bouton pour effacer</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Show covers</source>
        <translation>Afficher les pochettes</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show rating</source>
        <translation>Afficher l&apos;appréciation</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.cpp" line="+216"/>
        <source>Playlist look: Invalid expression</source>
        <translation>Apparence de la liste de lecture : Expression non valable</translation>
    </message>
</context>
<context>
    <name>GUI_PreferenceDialog</name>
    <message>
        <location filename="../src/Gui/Preferences/PreferenceDialog/GUI_PreferenceDialog.ui" line="+136"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>OK</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>GUI_ProxyPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.ui" line="+20"/>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Username</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Save username/password</source>
        <translation>Enregistrer nom/mot de passe</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Automatic search</source>
        <translation>Recherche automatique</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.cpp" line="+57"/>
        <source>Proxy</source>
        <translation>Serveur mandataire ( proxy )</translation>
    </message>
</context>
<context>
    <name>GUI_RemoteControlPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.ui" line="+36"/>
        <source>Detectable via UDP</source>
        <translation>Détectable par UDP</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Remote control URL</source>
        <translation>URL de la commande à distance</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>UDP port</source>
        <translation>Port UDP</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.cpp" line="+59"/>
        <source>If activated, Sayonara will answer an UDP request that it is remote controlable</source>
        <translation>Si cette option est activée, Sayonara répondra à une requête UDP contrôlable à distance.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Remote control</source>
        <translation>Commande à distance</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Port %1 already in use</source>
        <translation>Le port %1 est déjà utilisé</translation>
    </message>
</context>
<context>
    <name>GUI_SearchPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Search/GUI_SearchPreferences.ui" line="+23"/>
        <source>Example</source>
        <translation>Exemple</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Case insensitive</source>
        <translation>Insensible à la casse</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Option</source>
        <translation>Option</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Ignore accents</source>
        <translation>Ignorer les accents</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Ignore special characters</source>
        <translation>Ignorer les caractères spéciaux</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.ui" line="+44"/>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="+149"/>
        <source>Press shortcut</source>
        <translation>Tapez votre raccourci</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="-48"/>
        <source>Shortcuts</source>
        <translation>Raccourcis clavier</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Double shortcuts found</source>
        <translation>Conflit détecté dans les raccourcis</translation>
    </message>
</context>
<context>
    <name>GUI_StreamRecorderPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.ui" line="+30"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Target directory</source>
        <translation>Répertoire cible</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Automatic recording</source>
        <translation>Enregistrement automatique</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Create session directory</source>
        <translation>Créer le répertoire de session</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Directory</source>
        <translation>Dossier de session</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Choose available placeholders</source>
        <translation>Caractères de remplacement</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Path template</source>
        <translation>Emplacement des modèles</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Example</source>
        <translation>Exemple</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.cpp" line="+172"/>
        <source>Choose target directory</source>
        <translation>Choisissez le dossier cible</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Target directory is empty</source>
        <translation>Le répertoire cible est vide</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Please choose another directory</source>
        <translation>Choisissez un autre dossier d&apos;écriture</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cannot create %1</source>
        <translation>Impossible de créer %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Template path is not valid</source>
        <translation>Emplacement de modèles invalide</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stream recorder</source>
        <translation>Enregistreur de flux</translation>
    </message>
</context>
<context>
    <name>GUI_StreamPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.ui" line="+17"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Buffer size</source>
        <translation>Mémoire tampon</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show history</source>
        <translation>Afficher l&apos;historique</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Streams in new tab</source>
        <translation>Ouvrir le flux dans un nouvel onglet</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.cpp" line="+57"/>
        <source>%1 and %2</source>
        <translation>%1 et %2</translation>
    </message>
</context>
<context>
    <name>GUI_CssEditor</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_CssEditor.ui" line="+97"/>
        <source>Edit style sheet</source>
        <translation>Modifier la feuille de style</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Dark mode</source>
        <translation>Mode sombre</translation>
    </message>
</context>
<context>
    <name>GUI_FontPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.ui" line="+84"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Font size</source>
        <translation>Taille de la police</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Font name</source>
        <translation>Nom de la police</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.cpp" line="+129"/>
        <location line="+1"/>
        <source>Inherit</source>
        <translation>Hériter</translation>
    </message>
</context>
<context>
    <name>GUI_IconPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.ui" line="+20"/>
        <source>Also apply this icon theme to the dark style</source>
        <translation>Appliquer aussi ce thème d’icônes au style sombre</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.cpp" line="+96"/>
        <location line="+104"/>
        <source>System theme</source>
        <translation>Thème système</translation>
    </message>
    <message>
        <location line="-96"/>
        <source>Icons</source>
        <translation>Icônes</translation>
    </message>
</context>
<context>
    <name>GUI_UiPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.ui" line="+57"/>
        <source>Fading cover</source>
        <translation>Transition des pochettes</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit style sheet</source>
        <translation>Modifier la feuille de style</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.cpp" line="+48"/>
        <source>User Interface</source>
        <translation>Interface utilisateur</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show large cover</source>
        <translation>Pochette plus grande</translation>
    </message>
</context>
<context>
    <name>GUI_Shutdown</name>
    <message>
        <location filename="../src/Gui/Shutdown/GUI_Shutdown.ui" line="+14"/>
        <location line="+69"/>
        <source>Shutdown</source>
        <translation>Éteindre</translation>
    </message>
    <message>
        <location line="-56"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>minutes</source>
        <translation>minutes</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shutdown after playlist finished</source>
        <translation>Éteindre après la fin de la liste de lecture</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Shutdown after</source>
        <translation>Éteindre après </translation>
    </message>
</context>
<context>
    <name>GUI_SomaFM</name>
    <message>
        <location filename="../src/Gui/SomaFM/GUI_SomaFM.ui" line="+42"/>
        <source>Donate to Soma.fm</source>
        <translation>Faire un don a Soma.fm</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Streams</source>
        <translation>Flux Web</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.ui" line="+14"/>
        <source>Search Soundcloud</source>
        <translation>Chercher sur Soundcloud</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Search artist</source>
        <translation>Chercher artiste</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudLibrary</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.ui" line="+26"/>
        <source>Library</source>
        <translation>Bibliothèque</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Search for title, interprets and albums</source>
        <translation>Rechercher des titres, interprètes et albums</translation>
    </message>
</context>
<context>
    <name>GUI_CoverEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.ui" line="+57"/>
        <source>Replace</source>
        <translation>Remplacer par</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="+223"/>
        <source>Original</source>
        <translation>Original</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="-56"/>
        <location line="+9"/>
        <source>File has no cover</source>
        <translation>Le fichier n&apos;a pas de pochette</translation>
    </message>
</context>
<context>
    <name>GUI_FailMessageBox</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.ui" line="+22"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.cpp" line="+55"/>
        <source>File exists</source>
        <translation>Le fichier existe déjà</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Writeable</source>
        <translation>Inscriptible</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Some files could not be saved</source>
        <translation>Certains fichiers n&apos;ont pas pu être enregistrés</translation>
    </message>
</context>
<context>
    <name>GUI_TagEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.ui" line="+115"/>
        <source>Discnumber</source>
        <translation>N° de disque</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Album artist</source>
        <translation>Artiste de l&apos;album</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>all</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tag from path</source>
        <translation>Tag à partir du chemin</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Undo all</source>
        <translation>Annuler tout</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Read only file</source>
        <translation>Fichier en lecture seule</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.cpp" line="+161"/>
        <source>Load complete album</source>
        <translation>Charger l&apos;album complet</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Metadata</source>
        <translation>Métadonnées</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tags from path</source>
        <translation>Étiqueter par chemin d&apos;accès</translation>
    </message>
    <message numerus="yes">
        <location line="+81"/>
        <source>Cannot apply expression to %n track(s)</source>
        <translation><numerusform>Impossible d’appliquer l’expression à %n des pistes</numerusform><numerusform>Impossible d’appliquer l’expression à %n des pistes</numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore these tracks?</source>
        <translation>Ignorer ces pistes ?</translation>
    </message>
    <message>
        <location line="+394"/>
        <source>All changes will be lost</source>
        <translation>Tout changement sera perdu</translation>
    </message>
</context>
<context>
    <name>GUI_TagFromPath</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.ui" line="+17"/>
        <source>Expression</source>
        <translation>Expression</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Disc Nr</source>
        <translation>N° de disque</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Apply to all</source>
        <translation>Appliquer à tout</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.cpp" line="+92"/>
        <source>Tag</source>
        <translation>Étiquette ( tag )</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Please select text first</source>
        <translation>Veuillez d&apos;abord sélectionner le texte</translation>
    </message>
</context>
<context>
    <name>StreamServer</name>
    <message>
        <location filename="../src/Components/Broadcasting/StreamServer.cpp" line="+228"/>
        <source>%1 wants to listen to your music.</source>
        <translation>%1 veut écouter votre musique.</translation>
    </message>
</context>
<context>
    <name>AbstractLibrary</name>
    <message>
        <location filename="../src/Components/Library/AbstractLibrary.cpp" line="+849"/>
        <source>All %1 could be removed</source>
        <translation>Tous les %1 ont été effacés</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 %3 could not be removed</source>
        <translation>%1 %3 sur %2 n&apos;ont pas pu être effacés</translation>
    </message>
</context>
<context>
    <name>Library::Importer</name>
    <message>
        <location filename="../src/Components/Library/Importer/LibraryImporter.cpp" line="+228"/>
        <source>Cannot import tracks</source>
        <translation>Impossible d&apos;importer les pistes</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>All files could be imported</source>
        <translation>Tous les fichiers ont été importés</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 files could be imported</source>
        <translation>%1 fichiers sur %2 ont été importés</translation>
    </message>
</context>
<context>
    <name>Library::ReloadThread</name>
    <message>
        <location filename="../src/Components/Library/Threads/ReloadThread.cpp" line="+219"/>
        <source>Looking for covers</source>
        <translation>Recherche de pochettes</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Reading files</source>
        <translation>Lecture des fichiers en cours</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Deleting orphaned tracks</source>
        <translation>Suppression des pistes orphelines</translation>
    </message>
</context>
<context>
    <name>Lyrics::LookupThread</name>
    <message>
        <location filename="../src/Components/Lyrics/LyricLookup.cpp" line="+171"/>
        <location line="+35"/>
        <source>Cannot fetch lyrics from %1</source>
        <translation>Les paroles de %1 sont introuvables</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No lyrics found</source>
        <translation>Pas de paroles trouvées</translation>
    </message>
</context>
<context>
    <name>Shutdown</name>
    <message>
        <location filename="../src/Components/Shutdown/Shutdown.cpp" line="+100"/>
        <source>Computer will shutdown after playlist has finished</source>
        <translation>L&apos;ordinateur s&apos;éteindra à la fin de la liste de lecture</translation>
    </message>
    <message numerus="yes">
        <location line="+29"/>
        <location line="+35"/>
        <source>Computer will shutdown in %n minute(s)</source>
        <translation><numerusform>L&apos;ordinateur s&apos;éteindra dans %n minute</numerusform><numerusform>L’ordinateur s’éteindra dans %n minutes</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::JsonParser</name>
    <message>
        <location filename="../src/Components/Streaming/Soundcloud/SoundcloudJsonParser.cpp" line="+123"/>
        <source>Website</source>
        <translation>Site internet</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+236"/>
        <source>Permalink Url</source>
        <translation>Lien internet permanent</translation>
    </message>
    <message>
        <location line="-223"/>
        <source>Followers/Following</source>
        <translation>Followers/Following</translation>
    </message>
    <message>
        <location line="+93"/>
        <location line="+134"/>
        <source>Purchase Url</source>
        <translation>Lien pour l&apos;achat</translation>
    </message>
</context>
<context>
    <name>Gui::CoverButton</name>
    <message>
        <location filename="../src/Gui/Covers/CoverButton.cpp" line="+119"/>
        <source>Search an alternative cover</source>
        <translation>Chercher une autre pochette</translation>
    </message>
</context>
<context>
    <name>HistoryContainer</name>
    <message>
        <location filename="../src/Gui/History/HistoryContainer.cpp" line="+25"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
</context>
<context>
    <name>GUI_InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.cpp" line="+92"/>
        <source>Write cover to tracks</source>
        <translation>Ajouter la pochette aux fichiers</translation>
    </message>
</context>
<context>
    <name>Library::CoverViewContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/CoverViewContextMenu.cpp" line="+217"/>
        <source>Toolbar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
</context>
<context>
    <name>Library::GUI_CoverView</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/GUI_CoverView.cpp" line="+214"/>
        <source>Use Ctrl + mouse wheel to zoom</source>
        <translation>Utiliser Ctrl + molette pour zoomer</translation>
    </message>
</context>
<context>
    <name>Directory::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryContextMenu.cpp" line="+295"/>
        <source>Rename by metadata</source>
        <translation>Renommer par métadonnées</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Collapse all</source>
        <translation>Tout réduire</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to another library</source>
        <translation>Déplacer vers une autre bibliothèque</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to another library</source>
        <translation>Copier dans une autre bibliothèque</translation>
    </message>
</context>
<context>
    <name>Directory::TreeView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryTreeView.cpp" line="+410"/>
        <source>Copy here</source>
        <translation>Copier ici</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move here</source>
        <translation>Déplacer ici</translation>
    </message>
</context>
<context>
    <name>GUI_DirectoryView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/GUI_DirectoryView.cpp" line="+183"/>
        <source>Could not create directory</source>
        <translation>Impossible de créer le dossier</translation>
    </message>
</context>
<context>
    <name>Library::GenreView</name>
    <message>
        <location filename="../src/Gui/Library/GenreView.cpp" line="+114"/>
        <source>Updating genres</source>
        <translation>Acutaliser les genres</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Do you really want to remove %1 from all tracks?</source>
        <translation>Voulez-vous vraiment supprimer « %1 » de toutes les pistes ?</translation>
    </message>
</context>
<context>
    <name>Library::GUI_EmptyLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_EmptyLibrary.cpp" line="+94"/>
        <source>Please choose a name for your library</source>
        <translation>Veuillez donner un nom à votre bibliothèque</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Please choose another name for your library</source>
        <translation>Veuillez choisir un autre nom pour votre bibliothèque</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>The file path is invalid</source>
        <translation>Le chemin d&apos;accès est invalide</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library with the same file path already exists</source>
        <translation>Vous avez déjà une bibliothèque dans ce répertoire</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library which contains this file path already exists</source>
        <translation>Une bibliothèque contenant ce chemin d’accès existe déjà</translation>
    </message>
</context>
<context>
    <name>Library::HeaderView</name>
    <message>
        <location filename="../src/Gui/Library/Header/HeaderView.cpp" line="+198"/>
        <source>Resize columns</source>
        <translation>Redimensionner les colonnes</translation>
    </message>
</context>
<context>
    <name>Library::TrackModel</name>
    <message>
        <location filename="../src/Gui/Library/TableView/TrackModel.cpp" line="+144"/>
        <source>kBit/s</source>
        <translation>Kbit/s</translation>
    </message>
</context>
<context>
    <name>GUI_DeleteDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_DeleteDialog.cpp" line="+87"/>
        <source>Only from library</source>
        <translation>Seulement à partir de la bibliothèque</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>You are about to delete %n file(s)</source>
        <translation><numerusform>Vous vous apprêtez à supprimer %n fichiers</numerusform><numerusform>Vous vous apprêtez à supprimer %n fichier</numerusform></translation>
    </message>
</context>
<context>
    <name>Library::GUI_LibraryReloadDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_ReloadLibraryDialog.cpp" line="+78"/>
        <source>Fast scan</source>
        <translation>Analyse rapide</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deep scan</source>
        <translation>Analyse exhaustive</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Only scan for new and deleted files</source>
        <translation>Restreindre l&apos;analyse aux ajouts et suppressions</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scan all files in your library directory</source>
        <translation>Analyser tous les fichiers de votre bibliothèque</translation>
    </message>
</context>
<context>
    <name>Library::LocalLibraryMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/LocalLibraryMenu.cpp" line="+154"/>
        <source>Statistics</source>
        <translation>Statistiques</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit library</source>
        <translation>Modifier la bibliothèque</translation>
    </message>
</context>
<context>
    <name>Gui::MergeMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/MergeMenu.cpp" line="+52"/>
        <location line="+53"/>
        <source>Merge</source>
        <translation>Fusionner</translation>
    </message>
</context>
<context>
    <name>GUI_ControlsBase</name>
    <message>
        <location filename="../src/Gui/Player/GUI_ControlsBase.cpp" line="+71"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Written by %1</source>
        <translation>Écrit par %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
</context>
<context>
    <name>GUI_Logger</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.cpp" line="+275"/>
        <source>Cannot open file</source>
        <translation>Impossible d&apos;ouvrir le fichier</translation>
    </message>
</context>
<context>
    <name>Menubar</name>
    <message>
        <location filename="../src/Gui/Player/GUI_PlayerMenubar.cpp" line="+323"/>
        <source>View</source>
        <translation>Vue</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+18"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Plugins</source>
        <translation>Extensions</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Show large cover</source>
        <translation>Pochette plus grande</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fullscreen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Media files</source>
        <translation>Fichiers multimédia</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Ouvrir des fichiers multimédia</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please visit the forum at</source>
        <translation>Rendez-vous sur le forum via</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>FAQ</source>
        <translation>FAQ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>About Sayonara</source>
        <translation>À propos de Sayonara</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Written by %1</source>
        <translation>Écrit par %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Donate</source>
        <translation>Faire un don</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Special thanks to all the brave translators</source>
        <translation>Un remerciement particulier à tous les braves traducteurs</translation>
    </message>
</context>
<context>
    <name>TrayIconContextMenu</name>
    <message>
        <location filename="../src/Gui/Player/GUI_TrayIcon.cpp" line="+176"/>
        <source>Current song</source>
        <translation>Chanson en cours</translation>
    </message>
</context>
<context>
    <name>VersionChecker</name>
    <message>
        <location filename="../src/Gui/Player/VersionChecker.cpp" line="+77"/>
        <source>A new version is available!</source>
        <translation>Une nouvelle version est disponible !</translation>
    </message>
</context>
<context>
    <name>GUI_Playlist</name>
    <message>
        <location filename="../src/Gui/Playlist/GUI_Playlist.cpp" line="+241"/>
        <source>Playlist empty</source>
        <translation>Liste de lecture vide</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Media files</source>
        <translation>Fichiers multimédia</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Ouvrir des fichiers multimédia</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Playlist name already exists</source>
        <translation>Ce nom de liste de lecture existe déjà</translation>
    </message>
</context>
<context>
    <name>Playlist::ActionMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistActionMenu.cpp" line="+193"/>
        <source>Please set library path first</source>
        <translation>Veuillez d&apos;abord définir le chemin de la bibliothèque</translation>
    </message>
</context>
<context>
    <name>Playlist::BottomBar</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistBottomBar.cpp" line="+242"/>
        <source>Please set library path first</source>
        <translation>Veuillez d&apos;abord définir le chemin de la bibliothèque</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel shutdown?</source>
        <translation>Annuler l&apos;extinction de l&apos;ordinateur ?</translation>
    </message>
</context>
<context>
    <name>Playlist::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistContextMenu.cpp" line="+171"/>
        <source>Jump to current track</source>
        <translation>Aller à la piste actuelle</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show track in library</source>
        <translation>Afficher la piste dans la bibliothèque</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Playlist mode</source>
        <translation>Mode liste de lecture</translation>
    </message>
</context>
<context>
    <name>Playlist::Model</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistModel.cpp" line="+474"/>
        <source>Goto row</source>
        <translation>Aller à la rangée</translation>
    </message>
</context>
<context>
    <name>GUI_Crossfader</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Crossfader.cpp" line="+75"/>
        <location line="+12"/>
        <source>Crossfader</source>
        <translation>Crossfader</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 et %2</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Crossfader does not work with Alsa</source>
        <translation>Le fondu enchaîné ne fonctionne pas avec Alsa.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gapless playback does not work with Alsa</source>
        <translation>La lecture sans interruption ne fonctionne pas avec Alsa.</translation>
    </message>
</context>
<context>
    <name>GUI_Equalizer</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Equalizer.cpp" line="+137"/>
        <location line="+40"/>
        <source>Linked sliders</source>
        <translation>Curseurs liés</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Equalizer</source>
        <translation>Égaliseur</translation>
    </message>
    <message>
        <location line="+248"/>
        <source>Name %1 not allowed</source>
        <translation>Le nom %1 n&apos;est pas autorisé</translation>
    </message>
</context>
<context>
    <name>GUI_LevelPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_LevelPainter.cpp" line="+157"/>
        <source>Level</source>
        <translation>Niveau</translation>
    </message>
</context>
<context>
    <name>GUI_SpectrogramPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_SpectrogramPainter.cpp" line="+77"/>
        <source>Spectrogram</source>
        <translation>Spectrogramme</translation>
    </message>
</context>
<context>
    <name>GUI_Spectrum</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Spectrum.cpp" line="+150"/>
        <source>Spectrum</source>
        <translation>Spectre</translation>
    </message>
</context>
<context>
    <name>GUI_StyleSettings</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_StyleSettings.cpp" line="+203"/>
        <source>There are some unsaved settings&lt;br /&gt;Save now?</source>
        <translation>Il y a des réglages non enregistrés&lt;br /&gt;Enregistrer ?</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Please specify a name</source>
        <translation>Veuillez définir un nom</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Save changes?</source>
        <translation>Enregistrer les changements ?</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistChooser</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_PlaylistChooser.cpp" line="+91"/>
        <location line="+38"/>
        <source>No playlists found</source>
        <translation>Aucune liste de lecture trouvée</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Could not rename playlist</source>
        <translation>Impossible de renommer la liste de lecture</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Name is invalid</source>
        <translation>Le nom est invalide</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Do you really want to delete %1?</source>
        <translation>Voulez-vous vraiment supprimer %1?</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not delete playlist %1</source>
        <translation>Impossible de supprimer la liste de lecture %1</translation>
    </message>
</context>
<context>
    <name>Gui::AbstractStationPlugin</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/AbstractStationPlugin.cpp" line="+243"/>
        <source>Cannot open stream</source>
        <translation>Incapable d&apos;ouvrir le flux</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please choose another name</source>
        <translation>Veuillez choisir un autre nom</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Do you really want to delete %1</source>
        <translation>Voulez-vous vraiment effacer %1</translation>
    </message>
</context>
<context>
    <name>GUI_Podcasts</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Podcasts.cpp" line="+65"/>
        <source>Podcast</source>
        <translation>Podcast</translation>
    </message>
</context>
<context>
    <name>GUI_Stream</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Stream.cpp" line="+68"/>
        <source>Search radio station</source>
        <translation>Rechercher une station de radio</translation>
    </message>
</context>
<context>
    <name>GUI_EnginePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Engine/GUI_EnginePreferences.cpp" line="+53"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutEntry</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutEntry.cpp" line="+55"/>
        <source>Enter shortcut</source>
        <translation>Tapez votre raccourci</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Shortcut already in use</source>
        <translation>Raccourci déjà utilisé</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Test</source>
        <translation>Test</translation>
    </message>
</context>
<context>
    <name>SomaFM::StationModel</name>
    <message>
        <location filename="../src/Gui/SomaFM/SomaFMStationModel.cpp" line="+126"/>
        <source>Cannot fetch stations</source>
        <translation>Impossible de trouver les stations</translation>
    </message>
</context>
<context>
    <name>SC::GUI_ArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.cpp" line="+79"/>
        <source>Query too short</source>
        <translation>La requête est trop courte</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>No artists found</source>
        <translation>Aucun artiste trouvé</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>Found %n artist(s)</source>
        <translation><numerusform>%n artistes trouvés</numerusform><numerusform>%n artiste trouvé</numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+56"/>
        <source>%n playlist(s) found</source>
        <translation><numerusform>%n listes de lecture trouvées</numerusform><numerusform>%n liste de lecture trouvée</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::GUI_Library</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.cpp" line="+56"/>
        <source>Add artist</source>
        <translation>Ajouter artiste</translation>
    </message>
</context>
<context>
    <name>TagTextInput</name>
    <message>
        <location filename="../src/Gui/Tagging/TagTextInput.cpp" line="+126"/>
        <source>Very first letter to upper case</source>
        <translation>Première lettre en majuscule</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First letters to upper case</source>
        <translation>Toutes les premières lettres en majuscules</translation>
    </message>
</context>
<context>
    <name>Library::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Utils/ContextMenu/LibraryContextMenu.cpp" line="+196"/>
        <source>Play in new tab</source>
        <translation>Jouer dans un nouvel onglet</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Standard view</source>
        <translation>Affichage standard</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cover view</source>
        <translation>Afficher les pochettes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory view</source>
        <translation>Affichage répertoires</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>The toolbar is visible when there are tracks with differing file types listed in the track view</source>
        <translation>La barre d&apos;outils s&apos;affiche lorsque des pistes aux types de fichier différents se trouvent dans l&apos;affichage des pistes.</translation>
    </message>
</context>
<context>
    <name>Gui::ImageSelectionDialog</name>
    <message>
        <location filename="../src/Gui/Utils/ImageSelectionDialog.cpp" line="+57"/>
        <source>Image files</source>
        <translation>Fichiers image</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Any files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open image files</source>
        <translation>Ouvrir les fichiers image</translation>
    </message>
</context>
<context>
    <name>Gui::StreamRecorderPreferenceAction</name>
    <message>
        <location filename="../src/Gui/Utils/PreferenceAction.cpp" line="+163"/>
        <location line="+7"/>
        <source>Stream Recorder</source>
        <translation>Enregistreur de flux</translation>
    </message>
</context>
<context>
    <name>Gui::ShortcutPreferenceAction</name>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Shortcuts</source>
        <translation>Raccourcis clavier</translation>
    </message>
</context>
<context>
    <name>Gui::MiniSearcher</name>
    <message>
        <location filename="../src/Gui/Utils/SearchableWidget/MiniSearcher.cpp" line="+92"/>
        <source>Arrow up</source>
        <translation>Flèche haut</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Previous search result</source>
        <translation>Résultat précédent</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Arrow down</source>
        <translation>Flèche bas</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Next search result</source>
        <translation>Résultat suivant</translation>
    </message>
</context>
<context>
    <name>Lang</name>
    <message>
        <location filename="../src/Utils/Language/Language.cpp" line="+79"/>
        <source>About</source>
        <translation>À propos de</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Activate</source>
        <translation>Activer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add tab</source>
        <translation>Ajouter un onglet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artists</source>
        <translation>Artistes de l&apos;album</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Albums</source>
        <translation>Albums</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All</source>
        <translation>Tous</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Append</source>
        <translation>Ajouter après</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Application</source>
        <translation>Application</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artist</source>
        <translation>Artiste</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artists</source>
        <translation>Artistes</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ascending</source>
        <translation>Croissant</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bitrate</source>
        <translation>Débit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bookmarks</source>
        <translation>Signets</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Broadcast</source>
        <translation>Émission de radio</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>by</source>
        <translation>par</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot find Lame MP3 encoder</source>
        <translation>Encodeur Lame MP3 introuvable</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear selection</source>
        <translation>Effacer la sélection</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close others</source>
        <translation>Fermer autres</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close tab</source>
        <translation>Fermer l&apos;onglet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment</source>
        <translation>Commentaire</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Covers</source>
        <translation>Pochettes</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created</source>
        <translation>Création</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create new directory</source>
        <translation>Créer un nouveau dossier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create a new library</source>
        <translation>Créer une nouvelle bibliothèque</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark Mode</source>
        <translation>Mode sombre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Days</source>
        <translation>Jours</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>d</source>
        <translation>j</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Default</source>
        <translation>Défaut</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+84"/>
        <source>Delete</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location line="-82"/>
        <source>Descending</source>
        <translation>Décroissant</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory</source>
        <translation>Dossier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directories</source>
        <translation>Dossiers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disc</source>
        <translation>N° de disque</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dur.</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dynamic playback</source>
        <translation>Lecture dynamique</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit</source>
        <translation>Modifier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Empty input</source>
        <translation>Entrée vide</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter name</source>
        <translation>Saisissez un nom</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please enter new name</source>
        <translation>Veuillez entrer un nouveau nom</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter URL</source>
        <translation>Saisir URL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry</source>
        <translation>Élément</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entries</source>
        <translation>Éléments</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fast</source>
        <translation>Rapide</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filename</source>
        <translation>Nom de fichier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filesize</source>
        <translation>Taille du fichier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File type</source>
        <translation>Type de fichier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>1st</source>
        <translation>1er</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Font</source>
        <translation>Police</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation>Polices</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fulltext</source>
        <translation>Texte intégral</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Gapless playback</source>
        <translation>Jouer sans interruption entre les pistes ( gapless )</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genres</source>
        <translation>Genres</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hide</source>
        <translation>Cacher</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hours</source>
        <translation>Heures</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import directory</source>
        <translation>Importer un dossier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import files</source>
        <translation>Importer des fichiers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation>Inactif</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading</source>
        <translation>Chargement en cours</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading %1</source>
        <translation>Chargement en cours : %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid characters</source>
        <translation>Caractères non valides</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+f</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shift</source>
        <translation>Décalage</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Backspace</source>
        <translation>Correction</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab</source>
        <translation>Onglet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library</source>
        <translation>Bibliothèque</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library path</source>
        <translation>Chemin de la bibliothèque</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library view type</source>
        <translation>Type d’affichage de la bibliothèque</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Listen</source>
        <translation>Écouter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Live Search</source>
        <translation>Recherche instantanée</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Logger</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lyrics</source>
        <translation>Paroles</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>Minimiser</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minutes</source>
        <translation>Minutes</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing</source>
        <translation>Manquant</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Modified</source>
        <translation>Modification</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Months</source>
        <translation>Mois</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute</source>
        <translation>Muet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute off</source>
        <translation>Rétablir le volume</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next page</source>
        <translation>Page suivante</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next track</source>
        <translation>Piste suivante</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No albums</source>
        <translation>Aucun album</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+144"/>
        <source>Tracks</source>
        <translation>Pistes</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Move down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Move up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>on</source>
        <translation>activé</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open directory</source>
        <translation>Ouvrir dossier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open file</source>
        <translation>Ouvrir fichier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>or</source>
        <translation>ou</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Overwrite</source>
        <translation>Écraser</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play</source>
        <translation>Lecture</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playing time</source>
        <translation>Temps de lecture</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play in new tab</source>
        <translation>Jouer dans un nouvel onglet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlist</source>
        <translation>Liste de lecture</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlists</source>
        <translation>Listes de lecture</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play next</source>
        <translation>Lire l&apos;élément suivant</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play/Pause</source>
        <translation>Lecture/Pause</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugin</source>
        <translation>Extension</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Podcasts</source>
        <translation>Podcasts</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous page</source>
        <translation>Page précédente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous track</source>
        <translation>Piste précédente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Purchase Url</source>
        <translation>Lien pour l&apos;achat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio</source>
        <translation>Radio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio Station</source>
        <translation>Station radio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rating</source>
        <translation>Appréciation</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Really</source>
        <translation>Vraiment</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Refresh</source>
        <translation>Actualiser</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload Library</source>
        <translation>Recharger la bibliothèque</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename</source>
        <translation>Renommer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat 1</source>
        <translation>Répéter 1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat all</source>
        <translation>Répéter tout</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Replace</source>
        <translation>Remplacer par</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation>Ré-initialiser</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation>Réessayer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reverse order</source>
        <translation>Ordre inverse</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sampler</source>
        <translation>Échantillonneur</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shuffle</source>
        <translation>Lecture aléatoire</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shutdown</source>
        <translation>Éteindre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save as</source>
        <translation>Enregistrer sous</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save to file</source>
        <translation>Enregistrer vers le fichier</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan for audio files</source>
        <translation>Rechercher des fichiers audio</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Search</source>
        <translation>Chercher</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search next</source>
        <translation>Continuer la recherche</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search previous</source>
        <translation>Rechercher le précédent</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2nd</source>
        <translation>2ème</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seconds</source>
        <translation>Secondes</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek backward</source>
        <translation>Reculer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek forward</source>
        <translation>Avancer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show</source>
        <translation>Afficher</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Album Artists</source>
        <translation>Afficher l&apos;artiste de l&apos;album</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Covers</source>
        <translation>Afficher les pochettes</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Library</source>
        <translation>Afficher la bibliothèque</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Similar artists</source>
        <translation>Artistes similaires</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by</source>
        <translation>Trier par</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stop</source>
        <translation>Arrêt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streams</source>
        <translation>Flux web</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream URL</source>
        <translation>URL du flux</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Success</source>
        <translation>Succès</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>th</source>
        <translation>ème</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3rd</source>
        <translation>3ème</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track</source>
        <translation>Piste</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track number</source>
        <translation>Numéro de piste</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>track on</source>
        <translation>piste par</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Tree</source>
        <translation>Arborescence</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown album</source>
        <translation>Album inconnu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown artist</source>
        <translation>Artiste inconnu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown title</source>
        <translation>Titre inconnu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown genre</source>
        <translation>Genre inconnu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown placeholder</source>
        <translation>Expression inconnue</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown year</source>
        <translation>Année inconnue</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Various</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various albums</source>
        <translation>Albums divers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various artists</source>
        <translation>Artistes divers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various tracks</source>
        <translation>Pistes diverses</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume down</source>
        <translation>Baisser le volume</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume up</source>
        <translation>Augmenter le volume</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation>Avertissement</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Weeks</source>
        <translation>Semaines</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Year</source>
        <translation>Année</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Years</source>
        <translation>Années</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>No directories</source>
        <translation>Aucun dossier</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n directory(s)</source>
        <translation><numerusform>%n dossier</numerusform><numerusform>%n dossiers</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No files</source>
        <translation>Aucun fichier</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n file(s)</source>
        <translation><numerusform>%n fichier</numerusform><numerusform>%n fichiers</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No playlists</source>
        <translation>Aucune liste de lecture</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n playlist(s)</source>
        <translation><numerusform>%n liste de lecture</numerusform><numerusform>%n listes de lecture</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks</source>
        <translation>Aucune piste</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s)</source>
        <translation><numerusform>%n piste</numerusform><numerusform>%n pistes</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks found</source>
        <translation>Aucune piste trouvée</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s) found</source>
        <translation><numerusform>%n piste trouvée</numerusform><numerusform>%n pistes trouvées</numerusform></translation>
    </message>
</context>
</TS>