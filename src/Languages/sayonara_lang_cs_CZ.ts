<?xml version="1.0" ?><!DOCTYPE TS><TS language="cs_CZ" sourcelanguage="en" version="2.1">
<context>
    <name>GUI_AlternativeCovers</name>
    <message>
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.ui" line="+27"/>
        <source>Online Search</source>
        <translation>Hledání na internetu</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Automatic search</source>
        <translation>Automatické hledání</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Text search</source>
        <translation>Textové hledání</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Local Search</source>
        <translation>Místní hledání</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Select and preview multiple covers</source>
        <translation>Výběr a náhled více obalů</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Alternative Cover</source>
        <translation>Náhradní obal</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Start search automatically</source>
        <translation>Začít hledání automaticky</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.cpp" line="+283"/>
        <source>%n cover(s) found</source>
        <translation><numerusform>Nalezen %n obal</numerusform><numerusform>Nalezeny %n obaly</numerusform><numerusform>Nalezeno %n obalů</numerusform><numerusform>Nalezeno %n obalů</numerusform></translation>
    </message>
    <message>
        <location line="+133"/>
        <location line="+31"/>
        <source>Also save cover to %1</source>
        <translation>Obal uložit i do %1</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Cover web search is not enabled</source>
        <translation>Hledání obalů na internetu není povoleno</translation>
    </message>
</context>
<context>
    <name>GUI_History</name>
    <message>
        <location filename="../src/Gui/History/GUI_History.ui" line="+14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Newer</source>
        <translation>Novější</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Older</source>
        <translation>Starší</translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.ui" line="+20"/>
        <source>Info / Edit</source>
        <translation>Informace a úpravy</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Loading files...</source>
        <translation>Nahrávají se soubory...</translation>
    </message>
</context>
<context>
    <name>GUI_Lyrics</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.ui" line="+28"/>
        <source>Save Lyrics</source>
        <translation>Uložit text písně</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>switch</source>
        <translation>Přepnout</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Zoom</source>
        <translation>Přiblížení</translation>
    </message>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.cpp" line="+303"/>
        <source>Save lyrics not supported</source>
        <translation>Uložení textu písně nepodporováno</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Overwrite lyrics</source>
        <translation>Přepsat text písně</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save lyrics</source>
        <translation>Uložit text písně</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
</context>
<context>
    <name>GUI_ImportDialog</name>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.ui" line="+14"/>
        <source>Import</source>
        <translation>Zavést</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Import tracks to library</source>
        <translation>Zavést skladby do sbírky</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select target folder</source>
        <translation>Vybrat cílovou složku</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.cpp" line="+126"/>
        <source>Loading tracks</source>
        <translation>Nahrávají se skladby</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No tracks</source>
        <translation>Žádné skladby</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Importing</source>
        <translation>Zavedení</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>Dokončeno</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Rollback</source>
        <translation>Vrátit zpět</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cancelled</source>
        <translation>Zrušeno</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Choose target directory</source>
        <translation>Vybrat cílový adresář</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>%1&lt;br /&gt;is no library directory</source>
        <translation>%1&lt;br /&gt;není adresářem v hudební sbírce</translation>
    </message>
</context>
<context>
    <name>GUI_LocalLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.ui" line="+17"/>
        <source>Library</source>
        <translation>Sbírka</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.cpp" line="+352"/>
        <source>Audio files</source>
        <translation>Zvukové soubory</translation>
    </message>
</context>
<context>
    <name>GUI_Controls</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Controls.ui" line="+318"/>
        <source>Written by Michael Lugmair (Lucio Carreras)</source>
        <translation>Napsal Michael Lugmair (Lucio Carreras)</translation>
    </message>
</context>
<context>
    <name>GUI_Player</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Player.ui" line="+14"/>
        <source>Sayonara Player</source>
        <translation>Přehrávač Sayonara</translation>
    </message>
</context>
<context>
    <name>GUI_AudioConverter</name>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.ui" line="+45"/>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="+277"/>
        <source>Start</source>
        <translation>Spustit</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Stop</source>
        <translation>Zastavit</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>#Threads</source>
        <translation>#Vlákna</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+187"/>
        <source>Quality</source>
        <translation>Jakost</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+52"/>
        <location line="+89"/>
        <source>Bitrate</source>
        <translation>Datový tok</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="-164"/>
        <source>Audio Converter</source>
        <translation>Převaděč zvuku</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Threads</source>
        <translation>Vlákna</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+123"/>
        <source>Cannot find encoder</source>
        <translation>Nelze najít kodér</translation>
    </message>
    <message>
        <location line="-113"/>
        <location line="+14"/>
        <source>Playlist does not contain tracks which are supported by the converter</source>
        <translation>Seznam skladeb neobsahuje skladby, jež jsou podporovány převaděčem</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>No track will be converted.</source>
        <translation>Nebude převedena žádná skladba.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>These tracks will be ignored</source>
        <translation>Tyto skladby se budou přehlížet</translation>
    </message>
    <message numerus="yes">
        <location line="+38"/>
        <source>Failed to convert %n track(s)</source>
        <translation><numerusform>Nepodařilo se převést %n skladbu</numerusform><numerusform>Nepodařilo se převést %n skladby</numerusform><numerusform>Nepodařilo se převést %n skladeb</numerusform><numerusform>Nepodařilo se převést %n skladeb</numerusform></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please check the log files</source>
        <translation>Prověřte, prosím, soubory se zápisy</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>All tracks could be converted</source>
        <translation>Všechny skladby budou převedeny</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully finished</source>
        <translation>Úspěšně dokončeno</translation>
    </message>
</context>
<context>
    <name>GUI_Bookmarks</name>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.ui" line="+116"/>
        <source>Loop</source>
        <translation>Smyčka</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.cpp" line="+77"/>
        <location line="+53"/>
        <source>No bookmarks found</source>
        <translation>Nenalezeny žádné záložky</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Sorry, bookmarks can only be set for library tracks at the moment.</source>
        <translation>Promiňte, ale v současnosti je záložky možné nastavit pouze pro skladby v hudební sbírce.</translation>
    </message>
</context>
<context>
    <name>GUI_Broadcast</name>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.ui" line="+88"/>
        <source>Cannot Broadcast</source>
        <translation>Nelze vysílat</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.cpp" line="+104"/>
        <source>Dismiss</source>
        <translation>Odmítnout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dismiss all</source>
        <translation>Odmítnout vše</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%n listener(s)</source>
        <translation><numerusform>%n posluchač</numerusform><numerusform>%n posluchači</numerusform><numerusform>%n posluchačů</numerusform><numerusform>%n posluchačů</numerusform></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot broadcast on port %1</source>
        <translation>Nelze vysílat na přípojce %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maybe another application is using this port?</source>
        <translation>Tuto přípojku používá jiný program?</translation>
    </message>
</context>
<context>
    <name>GUI_Speed</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.ui" line="+48"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+134"/>
        <source>Speed</source>
        <translation>Rychlost</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Preserve pitch</source>
        <translation>Zachovat výšku tónu</translation>
    </message>
    <message>
        <location line="+30"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+1"/>
        <source>Pitch</source>
        <translation>Výška tónu</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 a %2</translation>
    </message>
    <message>
        <location line="+70"/>
        <location line="+1"/>
        <source>%1 not found</source>
        <translation>%1 nenalezeno</translation>
    </message>
</context>
<context>
    <name>GUI_Style</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Style.ui" line="+14"/>
        <source>Style</source>
        <translation>Styl</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Spectrum</source>
        <translation>Spektrum</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Vert. spacing</source>
        <translation>Svislý odstup</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+88"/>
        <source>Rect height</source>
        <translation>Výška obdélníku</translation>
    </message>
    <message>
        <location line="-68"/>
        <location line="+44"/>
        <source>Hor. spacing</source>
        <translation>Vodorovný odstup</translation>
    </message>
    <message>
        <location line="-30"/>
        <location line="+75"/>
        <source>Fading steps</source>
        <translation>Rychlost přechodu</translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Level</source>
        <translation>Zvukové úrovně</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Rect width</source>
        <translation>Šířka obdélníku</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ver. spacing</source>
        <translation>Svislý odstup</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Style settings</source>
        <translation>Nastavení stylů</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Color 2</source>
        <translation>Barva 2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 1</source>
        <translation>Barva 1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color 3</source>
        <translation>Barva 3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 4</source>
        <translation>Barva 4</translation>
    </message>
</context>
<context>
    <name>GUI_TargetPlaylistDialog</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_TargetPlaylistDialog.ui" line="+14"/>
        <source>Choose target playlist</source>
        <translation>Vybrat cílový seznam skladeb</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>relative filepaths</source>
        <translation>Relativní souborové cesty</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save playlist as...</source>
        <translation>Uložit seznam skladeb jako...</translation>
    </message>
</context>
<context>
    <name>GUI_StationSearcher</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.ui" line="+14"/>
        <source>Search Radio Station</source>
        <translation>Hledat rozhlasovou stanici</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.cpp" line="+58"/>
        <source>Show radio stations from %1 to %2</source>
        <translation>Ukázat rozhlasové stanice od %1 do %2</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Country</source>
        <translation>Země</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Url</source>
        <translation>Adresa (URL)</translation>
    </message>
</context>
<context>
    <name>GUI_BroadcastPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.ui" line="+20"/>
        <source>Port</source>
        <translation>Přípojka</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ask for permission</source>
        <translation>Požádat o dovolení</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.cpp" line="+161"/>
        <source>Port %1 already in use</source>
        <translation>Přípojka %1 se již používá</translation>
    </message>
</context>
<context>
    <name>GUI_CoverPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Covers/GUI_CoverPreferences.ui" line="+62"/>
        <source>Inactive</source>
        <translation>Nečinné</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>Činné</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Delete all covers from the database</source>
        <translation>Smazat všechny obaly z databáze</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear cache</source>
        <translation>Vyprázdnit vyrovnávací paměť</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete all covers from the Sayonara directory</source>
        <translation>Smazat všechny obaly z adresáře Sayonara</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete files</source>
        <translation>Smazat soubory</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Save found covers into the library directory where the audio files are located</source>
        <translation>Uložit nalezené obaly do adresáře se sbírkou, v němž jsou umístěny zvukové soubory</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers into library</source>
        <translation>Uložit nalezené obaly do sbírky</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fetch missing covers from the internet</source>
        <translation>Natáhnout chybějící obaly z internetu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Saving covers to the database leads to significantly faster access but results in a bigger database</source>
        <translation>Ukládání obalů do databáze vede k podstatně rychlejšímu přístupu, ale výsledkem je větší databáze</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers to database</source>
        <translation>Uložit nalezené obaly do databáze</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save found covers into Sayonara directory</source>
        <translation>Uložit nalezené obaly do adresáře Sayonara</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cover name</source>
        <translation>Název obalu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name of cover file</source>
        <translation>Název souboru s obalem</translation>
    </message>
</context>
<context>
    <name>GUI_LanguagePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.ui" line="+29"/>
        <source>Check for update</source>
        <translation>Zkontrolovat aktualizace</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Download update</source>
        <translation>Stáhnout aktualizaci</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>For new languages I am always looking for translators</source>
        <translation>Vždy hledám překladatele programu do dalších jazyků</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.cpp" line="+63"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Cannot check for language update</source>
        <translation>Nelze zkontrolovat jazykovou aktualizaci</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Language update available</source>
        <translation>Je dostupná jazyková aktualizace</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Language is up to date</source>
        <translation>Jazyk je aktuální</translation>
    </message>
    <message>
        <location line="+41"/>
        <location line="+24"/>
        <source>Cannot fetch language update</source>
        <translation>Nelze natáhnout jazykovou aktualizaci</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Language was updated successfully</source>
        <translation>Jazyk byl úspěšně aktualizován</translation>
    </message>
</context>
<context>
    <name>GUI_LastFmPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.ui" line="+27"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Scrobble time</source>
        <translation>Čas pro odesílání údajů</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Login now</source>
        <translation>Přihlásit nyní</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.cpp" line="+163"/>
        <source>Logged in</source>
        <translation>Přihlášen</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not logged in</source>
        <translation>Nepřihlášen</translation>
    </message>
</context>
<context>
    <name>GUI_LibraryPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.ui" line="+27"/>
        <source>Libraries</source>
        <translation>Sbírky</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Library-Playlist Interaction</source>
        <translation>Spolupráce sbírky a seznamu skladeb</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>When drag and drop into playlist </source>
        <translation>Při tažení a upuštění na seznam skladeb </translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+36"/>
        <source>do nothing (default)</source>
        <translation>Nedělat nic (výchozí)</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>start if stopped and playlist is empty</source>
        <translation>Začít s přehráváním, je-li zastaveno a seznam skladeb je prázdný</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>On double click, create playlist and</source>
        <translation>Při dvojitém poklepání vytvořit seznam skladeb a </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start playback if stopped</source>
        <translation>Začít s přehráváním, je-li zastaveno</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>start playback immediately</source>
        <translation>Začít okamžitě s přehráváním</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(this is ignored when playlist is in &apos;append mode&apos;)</source>
        <translation>(toto se přehlíží, když je seznam skladeb v přidávacím režimu)</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Other</source>
        <translation>Jiné</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show &quot;Clear selection&quot; buttons</source>
        <translation>Ukázat tlačítka pro vyprázdnění výběru</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Ignore English article &quot;The&quot; in artist name</source>
        <translation>Přehlížet anglický člen The ve jméně umělce</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.cpp" line="+147"/>
        <source>Cannot edit library</source>
        <translation>Nelze upravit hudební sbírku</translation>
    </message>
</context>
<context>
    <name>GUI_NotificationPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.ui" line="+17"/>
        <source>Timeout (ms)</source>
        <translation>Zavřít po (ms)</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.cpp" line="+98"/>
        <source>Notifications</source>
        <translation>Oznamování</translation>
    </message>
</context>
<context>
    <name>GUI_PlayerPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.ui" line="+44"/>
        <source>Show system tray icon</source>
        <translation>Ukázat ikonu v oznamovací oblasti panelu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Hide instead of close</source>
        <translation>Skrýt namísto zavření</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Start hidden</source>
        <translation>Spustit skryto</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update notifications</source>
        <translation>Oznamovatel aktualizací</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.cpp" line="+107"/>
        <source>This might cause Sayonara not to show up again.</source>
        <translation>Toto může vést k tomu, že se Sayonara znovu už neukáže.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>In this case use the &apos;--show&apos; option at the next startup.</source>
        <translation>V tom případě použijte při příštím spuštění volbu &apos;--show&apos;.</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.ui" line="+36"/>
        <source>Behavior</source>
        <translation>Chování</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start up</source>
        <translation>Spuštění programu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load temporary playlists</source>
        <translation>Nahrát dočasné seznamy skladeb</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load saved playlists</source>
        <translation>Nahrát uložené seznamy skladeb</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start playing</source>
        <translation>Spustit přehrávání</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load last track on startup</source>
        <translation>Při spuštění nahrát poslední skladbu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remember time of last track</source>
        <translation>Zapamatovat si čas naposledy přehrávané skladby</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stop behaviour</source>
        <translation>Chování při zastavení</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load last track after pressing stop</source>
        <translation>Po zastavení přehrávání nahrát poslední skladbu</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Look</source>
        <translation>Vzhled</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show numbers</source>
        <translation>Ukázat čísla</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>&apos;italic text&apos;</source>
        <translation>&apos;kurzivní text&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Example</source>
        <translation>Příklad</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>*bold text*</source>
        <translation>*tučný text*</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Show clear button</source>
        <translation>Ukázat tlačítko pro vyprázdnění</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Show covers</source>
        <translation>Ukázat obaly</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show rating</source>
        <translation>Ukázat hodnocení</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.cpp" line="+216"/>
        <source>Playlist look: Invalid expression</source>
        <translation>Vzhled seznamu skladeb: Neplatný výraz</translation>
    </message>
</context>
<context>
    <name>GUI_PreferenceDialog</name>
    <message>
        <location filename="../src/Gui/Preferences/PreferenceDialog/GUI_PreferenceDialog.ui" line="+136"/>
        <source>Preferences</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Apply</source>
        <translation>Použít</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>GUI_ProxyPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.ui" line="+20"/>
        <source>Host</source>
        <translation>Server</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Username</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Save username/password</source>
        <translation>Uložit uživatelské jméno/heslo</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Port</source>
        <translation>Přípojka</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Active</source>
        <translation>Činné</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Automatic search</source>
        <translation>Automatické hledání</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.cpp" line="+57"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
</context>
<context>
    <name>GUI_RemoteControlPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.ui" line="+36"/>
        <source>Detectable via UDP</source>
        <translation>Zjistitelné pomocí UDP</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Remote control URL</source>
        <translation>Adresa (URL) dálkového ovládání</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Port</source>
        <translation>Přípojka</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>UDP port</source>
        <translation>Přípojka UDP</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.cpp" line="+59"/>
        <source>If activated, Sayonara will answer an UDP request that it is remote controlable</source>
        <translation>Je-li zapnuto, Sayonara odpoví na požadavek UDP, který je dálkově řiditelný</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Remote control</source>
        <translation>Dálkové ovládání</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Port %1 already in use</source>
        <translation>Přípojka %1 se již používá</translation>
    </message>
</context>
<context>
    <name>GUI_SearchPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Search/GUI_SearchPreferences.ui" line="+23"/>
        <source>Example</source>
        <translation>Příklad</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Case insensitive</source>
        <translation>Rozlišovat velká a malá písmena</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Option</source>
        <translation>Volba</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Ignore accents</source>
        <translation>Přehlížet diakritická znaménka</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Ignore special characters</source>
        <translation>Přehlížet zvláštní znaky</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.ui" line="+44"/>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="+149"/>
        <source>Press shortcut</source>
        <translation>Stiskněte klávesovou zkratku</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="-48"/>
        <source>Shortcuts</source>
        <translation>Klávesové zkratky</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Double shortcuts found</source>
        <translation>Nalezeny zdvojené klávesové zkratky</translation>
    </message>
</context>
<context>
    <name>GUI_StreamRecorderPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.ui" line="+30"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Target directory</source>
        <translation>Cílový adresář</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Automatic recording</source>
        <translation>Automatické nahrávání</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Create session directory</source>
        <translation>Vytvořit adresář se sezením</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Directory</source>
        <translation>Adresář se sezením</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Choose available placeholders</source>
        <translation>Vybrat dostupné zástupné znaky</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Path template</source>
        <translation>Předloha cesty</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Example</source>
        <translation>Příklad</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.cpp" line="+172"/>
        <source>Choose target directory</source>
        <translation>Vybrat cílový adresář</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Target directory is empty</source>
        <translation>Cílový adresář je prázdný</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Please choose another directory</source>
        <translation>Vyberte, prosím, jiný adresář</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cannot create %1</source>
        <translation>Nelze vytvořit %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Template path is not valid</source>
        <translation>Předloha cesty není platná</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stream recorder</source>
        <translation>Nahrávání vysílání</translation>
    </message>
</context>
<context>
    <name>GUI_StreamPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.ui" line="+17"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Buffer size</source>
        <translation>Velikost vyrovnávací paměti</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show history</source>
        <translation>Ukázat historii</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Streams in new tab</source>
        <translation>Otevřít vysílání v nové kartě</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.cpp" line="+57"/>
        <source>%1 and %2</source>
        <translation>%1 a %2</translation>
    </message>
</context>
<context>
    <name>GUI_CssEditor</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_CssEditor.ui" line="+97"/>
        <source>Edit style sheet</source>
        <translation>Upravit stylový list</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Dark mode</source>
        <translation>Tmavý vzhled</translation>
    </message>
</context>
<context>
    <name>GUI_FontPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.ui" line="+84"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Font size</source>
        <translation>Velikost písma</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bold</source>
        <translation>Tučné</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Font name</source>
        <translation>Název písma</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.cpp" line="+129"/>
        <location line="+1"/>
        <source>Inherit</source>
        <translation>Zdědit</translation>
    </message>
</context>
<context>
    <name>GUI_IconPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.ui" line="+20"/>
        <source>Also apply this icon theme to the dark style</source>
        <translation>Tento motiv ikon použít i na tmavý vzhled</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.cpp" line="+96"/>
        <location line="+104"/>
        <source>System theme</source>
        <translation>Motiv systému</translation>
    </message>
    <message>
        <location line="-96"/>
        <source>Icons</source>
        <translation>Ikony</translation>
    </message>
</context>
<context>
    <name>GUI_UiPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.ui" line="+57"/>
        <source>Fading cover</source>
        <translation>Ztrácející se obal</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit style sheet</source>
        <translation>Upravit stylový list</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.cpp" line="+48"/>
        <source>User Interface</source>
        <translation>Rozhraní</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show large cover</source>
        <translation>Obal na střed</translation>
    </message>
</context>
<context>
    <name>GUI_Shutdown</name>
    <message>
        <location filename="../src/Gui/Shutdown/GUI_Shutdown.ui" line="+14"/>
        <location line="+69"/>
        <source>Shutdown</source>
        <translation>Vypnutí</translation>
    </message>
    <message>
        <location line="-56"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>minutes</source>
        <translation>minutách</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shutdown after playlist finished</source>
        <translation>Vypnout po dokončení seznamu skladeb</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Shutdown after</source>
        <translation>Vypnout po</translation>
    </message>
</context>
<context>
    <name>GUI_SomaFM</name>
    <message>
        <location filename="../src/Gui/SomaFM/GUI_SomaFM.ui" line="+42"/>
        <source>Donate to Soma.fm</source>
        <translation>Poskytnout příspěvek Soma.fm</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Streams</source>
        <translation>Vysílání</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.ui" line="+14"/>
        <source>Search Soundcloud</source>
        <translation>Hledat na Soundcloud</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Search artist</source>
        <translation>Hledat umělce</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudLibrary</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.ui" line="+26"/>
        <source>Library</source>
        <translation>Sbírka</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Search for title, interprets and albums</source>
        <translation>Hledat názvy, umělce a alba</translation>
    </message>
</context>
<context>
    <name>GUI_CoverEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.ui" line="+57"/>
        <source>Replace</source>
        <translation>Nahradit</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="+223"/>
        <source>Original</source>
        <translation>Originál</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="-56"/>
        <location line="+9"/>
        <source>File has no cover</source>
        <translation>Soubor nemá obal</translation>
    </message>
</context>
<context>
    <name>GUI_FailMessageBox</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.ui" line="+22"/>
        <source>Details</source>
        <translation>Podrobnosti</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.cpp" line="+55"/>
        <source>File exists</source>
        <translation>Soubor existuje</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Writeable</source>
        <translation>Zapisovatelný</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Some files could not be saved</source>
        <translation>Některé soubory se nepodařilo uložit</translation>
    </message>
</context>
<context>
    <name>GUI_TagEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.ui" line="+115"/>
        <source>Discnumber</source>
        <translation>Číslo disku</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Album artist</source>
        <translation>Umělec alba</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Comment</source>
        <translation>Poznámka</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>all</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tag from path</source>
        <translation>Značka z cesty</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Undo all</source>
        <translation>Zpět vše</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Read only file</source>
        <translation>Číst jen soubor</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.cpp" line="+161"/>
        <source>Load complete album</source>
        <translation>Nahrát celé album</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Metadata</source>
        <translation>Značky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tags from path</source>
        <translation>Značky z cesty</translation>
    </message>
    <message numerus="yes">
        <location line="+81"/>
        <source>Cannot apply expression to %n track(s)</source>
        <translation><numerusform>Výraz nelze použít na %n skladbu</numerusform><numerusform>Výraz nelze použít na %n skladby</numerusform><numerusform>Výraz nelze použít na %n skladeb</numerusform><numerusform>Výraz nelze použít na %n skladeb</numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore these tracks?</source>
        <translation>Přehlížet tyto skladby?</translation>
    </message>
    <message>
        <location line="+394"/>
        <source>All changes will be lost</source>
        <translation>Všechny změny budou ztraceny</translation>
    </message>
</context>
<context>
    <name>GUI_TagFromPath</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.ui" line="+17"/>
        <source>Expression</source>
        <translation>Výraz</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Disc Nr</source>
        <translation>Číslo disku</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Apply to all</source>
        <translation>Použít na vše</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.cpp" line="+92"/>
        <source>Tag</source>
        <translation>Značka</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Please select text first</source>
        <translation>Nejprve, prosím, vyberte text</translation>
    </message>
</context>
<context>
    <name>StreamServer</name>
    <message>
        <location filename="../src/Components/Broadcasting/StreamServer.cpp" line="+228"/>
        <source>%1 wants to listen to your music.</source>
        <translation>%1 chce poslouchat vaši hudbu.</translation>
    </message>
</context>
<context>
    <name>AbstractLibrary</name>
    <message>
        <location filename="../src/Components/Library/AbstractLibrary.cpp" line="+849"/>
        <source>All %1 could be removed</source>
        <translation>Všech %1 se podařilo smazat</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 %3 could not be removed</source>
        <translation>%1 z %2 %3n se nepodařilo smazat</translation>
    </message>
</context>
<context>
    <name>Library::Importer</name>
    <message>
        <location filename="../src/Components/Library/Importer/LibraryImporter.cpp" line="+228"/>
        <source>Cannot import tracks</source>
        <translation>Nelze zavést skladby</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>All files could be imported</source>
        <translation>Všechny soubory se podařilo zavést</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 files could be imported</source>
        <translation>%1 z %2 souborů bylo zavedeno</translation>
    </message>
</context>
<context>
    <name>Library::ReloadThread</name>
    <message>
        <location filename="../src/Components/Library/Threads/ReloadThread.cpp" line="+219"/>
        <source>Looking for covers</source>
        <translation>Hledají se obaly</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Reading files</source>
        <translation>Čtou se soubory</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Deleting orphaned tracks</source>
        <translation>Smazání osiřelých skladeb</translation>
    </message>
</context>
<context>
    <name>Lyrics::LookupThread</name>
    <message>
        <location filename="../src/Components/Lyrics/LyricLookup.cpp" line="+171"/>
        <location line="+35"/>
        <source>Cannot fetch lyrics from %1</source>
        <translation>Text písně nelze natáhnout z adresy %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No lyrics found</source>
        <translation>Nenalezeny žádné texty písní</translation>
    </message>
</context>
<context>
    <name>Shutdown</name>
    <message>
        <location filename="../src/Components/Shutdown/Shutdown.cpp" line="+100"/>
        <source>Computer will shutdown after playlist has finished</source>
        <translation>Počítač bude vypnut po dokončení seznamu skladeb</translation>
    </message>
    <message numerus="yes">
        <location line="+29"/>
        <location line="+35"/>
        <source>Computer will shutdown in %n minute(s)</source>
        <translation><numerusform>Počítač bude vypnut za %1 minutu</numerusform><numerusform>Počítač bude vypnut za %1 minuty</numerusform><numerusform>Počítač bude vypnut za %1 minut</numerusform><numerusform>Počítač bude vypnut za %1 minut</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::JsonParser</name>
    <message>
        <location filename="../src/Components/Streaming/Soundcloud/SoundcloudJsonParser.cpp" line="+123"/>
        <source>Website</source>
        <translation>Stránky</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+236"/>
        <source>Permalink Url</source>
        <translation>Neměnná adresa stránek</translation>
    </message>
    <message>
        <location line="-223"/>
        <source>Followers/Following</source>
        <translation>Sledující/Sleduje</translation>
    </message>
    <message>
        <location line="+93"/>
        <location line="+134"/>
        <source>Purchase Url</source>
        <translation>Koupit u</translation>
    </message>
</context>
<context>
    <name>Gui::CoverButton</name>
    <message>
        <location filename="../src/Gui/Covers/CoverButton.cpp" line="+119"/>
        <source>Search an alternative cover</source>
        <translation>Hledat náhradní obal</translation>
    </message>
</context>
<context>
    <name>HistoryContainer</name>
    <message>
        <location filename="../src/Gui/History/HistoryContainer.cpp" line="+25"/>
        <source>History</source>
        <translation>Historie</translation>
    </message>
</context>
<context>
    <name>GUI_InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.cpp" line="+92"/>
        <source>Write cover to tracks</source>
        <translation>Zapsat obal do skladeb</translation>
    </message>
</context>
<context>
    <name>Library::CoverViewContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/CoverViewContextMenu.cpp" line="+217"/>
        <source>Toolbar</source>
        <translation>Nástrojový pruh</translation>
    </message>
</context>
<context>
    <name>Library::GUI_CoverView</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/GUI_CoverView.cpp" line="+214"/>
        <source>Use Ctrl + mouse wheel to zoom</source>
        <translation>Použijte Ctrl + kolečko myši pro přiblížení/oddálení</translation>
    </message>
</context>
<context>
    <name>Directory::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryContextMenu.cpp" line="+295"/>
        <source>Rename by metadata</source>
        <translation>Přejmenovat podle značek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Collapse all</source>
        <translation>Sbalit vše</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to another library</source>
        <translation>Přesunout do jiné sbírky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to another library</source>
        <translation>Kopírovat do jiné sbírky</translation>
    </message>
</context>
<context>
    <name>Directory::TreeView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryTreeView.cpp" line="+410"/>
        <source>Copy here</source>
        <translation>Kopírovat sem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move here</source>
        <translation>Přesunout sem</translation>
    </message>
</context>
<context>
    <name>GUI_DirectoryView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/GUI_DirectoryView.cpp" line="+183"/>
        <source>Could not create directory</source>
        <translation>Nepodařilo se vytvořit adresář</translation>
    </message>
</context>
<context>
    <name>Library::GenreView</name>
    <message>
        <location filename="../src/Gui/Library/GenreView.cpp" line="+114"/>
        <source>Updating genres</source>
        <translation>Aktualizují se žánry</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Do you really want to remove %1 from all tracks?</source>
        <translation>Opravdu chcete odstranit %1 ze všech skladeb?</translation>
    </message>
</context>
<context>
    <name>Library::GUI_EmptyLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_EmptyLibrary.cpp" line="+94"/>
        <source>Please choose a name for your library</source>
        <translation>Vyberte, prosím, název pro sbírku</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Please choose another name for your library</source>
        <translation>Vyberte, prosím, jiný název pro sbírku</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>The file path is invalid</source>
        <translation>Cesta k souboru není platná</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library with the same file path already exists</source>
        <translation>Již je sbírka se stejnou souborovou cestou</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library which contains this file path already exists</source>
        <translation>Sbírka obsahující tuto souborovou cestu již je</translation>
    </message>
</context>
<context>
    <name>Library::HeaderView</name>
    <message>
        <location filename="../src/Gui/Library/Header/HeaderView.cpp" line="+198"/>
        <source>Resize columns</source>
        <translation>Změnit velikost sloupců</translation>
    </message>
</context>
<context>
    <name>Library::TrackModel</name>
    <message>
        <location filename="../src/Gui/Library/TableView/TrackModel.cpp" line="+144"/>
        <source>kBit/s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_DeleteDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_DeleteDialog.cpp" line="+87"/>
        <source>Only from library</source>
        <translation>Jen ze sbírky</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>You are about to delete %n file(s)</source>
        <translation><numerusform>Chystáte se smazat %1 soubor</numerusform><numerusform>Chystáte se smazat %1 soubory</numerusform><numerusform>Chystáte se smazat %1 souborů</numerusform><numerusform>Chystáte se smazat %1 souborů</numerusform></translation>
    </message>
</context>
<context>
    <name>Library::GUI_LibraryReloadDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_ReloadLibraryDialog.cpp" line="+78"/>
        <source>Fast scan</source>
        <translation>Rychlé prohledání</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deep scan</source>
        <translation>Hluboké prohledání</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Only scan for new and deleted files</source>
        <translation>Hledat pouze nové a smazané soubory</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scan all files in your library directory</source>
        <translation>Prohledávat všechny soubory v adresáři se sbírkou</translation>
    </message>
</context>
<context>
    <name>Library::LocalLibraryMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/LocalLibraryMenu.cpp" line="+154"/>
        <source>Statistics</source>
        <translation>Statistika</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit library</source>
        <translation>Upravit sbírku</translation>
    </message>
</context>
<context>
    <name>Gui::MergeMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/MergeMenu.cpp" line="+52"/>
        <location line="+53"/>
        <source>Merge</source>
        <translation>Sloučit</translation>
    </message>
</context>
<context>
    <name>GUI_ControlsBase</name>
    <message>
        <location filename="../src/Gui/Player/GUI_ControlsBase.cpp" line="+71"/>
        <source>Sayonara Player</source>
        <translation>Přehrávač Sayonara</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Written by %1</source>
        <translation>Napsal %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright</source>
        <translation>Autorské právo</translation>
    </message>
</context>
<context>
    <name>GUI_Logger</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.cpp" line="+275"/>
        <source>Cannot open file</source>
        <translation>Nelze otevřít soubor</translation>
    </message>
</context>
<context>
    <name>Menubar</name>
    <message>
        <location filename="../src/Gui/Player/GUI_PlayerMenubar.cpp" line="+323"/>
        <source>View</source>
        <translation>Pohled</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+18"/>
        <source>Help</source>
        <translation>Nápověda</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Show large cover</source>
        <translation>Obal na střed</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fullscreen</source>
        <translation>Celá obrazovka</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Media files</source>
        <translation>Zvukové soubory a seznamy skladeb</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Otevřít zvukové soubory a seznamy skladeb</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please visit the forum at</source>
        <translation>Navštivte, prosím, fórum na adrese</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>FAQ</source>
        <translation>Často kladené otázky</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>About Sayonara</source>
        <translation>O programu Sayonara</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Written by %1</source>
        <translation>Napsal %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>License</source>
        <translation>Povolení</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Donate</source>
        <translation>Dary</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Special thanks to all the brave translators</source>
        <translation>Zvláštní poděkování všem odvážným a udatným překladatelům</translation>
    </message>
</context>
<context>
    <name>TrayIconContextMenu</name>
    <message>
        <location filename="../src/Gui/Player/GUI_TrayIcon.cpp" line="+176"/>
        <source>Current song</source>
        <translation>Nynější píseň</translation>
    </message>
</context>
<context>
    <name>VersionChecker</name>
    <message>
        <location filename="../src/Gui/Player/VersionChecker.cpp" line="+77"/>
        <source>A new version is available!</source>
        <translation>Je dostupná nová verze!</translation>
    </message>
</context>
<context>
    <name>GUI_Playlist</name>
    <message>
        <location filename="../src/Gui/Playlist/GUI_Playlist.cpp" line="+241"/>
        <source>Playlist empty</source>
        <translation>Seznam skladeb je prázdný</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Media files</source>
        <translation>Zvukové soubory a seznamy skladeb</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Otevřít zvukové soubory a seznamy skladeb</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Playlist name already exists</source>
        <translation>Název seznamu skladeb již je</translation>
    </message>
</context>
<context>
    <name>Playlist::ActionMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistActionMenu.cpp" line="+193"/>
        <source>Please set library path first</source>
        <translation>Nejprve, prosím, zadejte cestu k hudební sbírce</translation>
    </message>
</context>
<context>
    <name>Playlist::BottomBar</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistBottomBar.cpp" line="+242"/>
        <source>Please set library path first</source>
        <translation>Nejprve, prosím, zadejte cestu k hudební sbírce</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel shutdown?</source>
        <translation>Zrušit vypnutí?</translation>
    </message>
</context>
<context>
    <name>Playlist::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistContextMenu.cpp" line="+171"/>
        <source>Jump to current track</source>
        <translation>Skočit na nynější skladbu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show track in library</source>
        <translation>Ukázat skladbu ve sbírce</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Playlist mode</source>
        <translation>Režim seznamu skladeb</translation>
    </message>
</context>
<context>
    <name>Playlist::Model</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistModel.cpp" line="+474"/>
        <source>Goto row</source>
        <translation>Jít na řádek</translation>
    </message>
</context>
<context>
    <name>GUI_Crossfader</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Crossfader.cpp" line="+75"/>
        <location line="+12"/>
        <source>Crossfader</source>
        <translation>Prolínač</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 a %2</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Crossfader does not work with Alsa</source>
        <translation>Prolínač nepracuje s ALSA</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gapless playback does not work with Alsa</source>
        <translation>Přehrávání bez mezer nepracuje s ALSA</translation>
    </message>
</context>
<context>
    <name>GUI_Equalizer</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Equalizer.cpp" line="+137"/>
        <location line="+40"/>
        <source>Linked sliders</source>
        <translation>Propojené posuvníky</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Equalizer</source>
        <translation>Ekvalizér</translation>
    </message>
    <message>
        <location line="+248"/>
        <source>Name %1 not allowed</source>
        <translation>Název %1 nepovolen</translation>
    </message>
</context>
<context>
    <name>GUI_LevelPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_LevelPainter.cpp" line="+157"/>
        <source>Level</source>
        <translation>Zvukové úrovně</translation>
    </message>
</context>
<context>
    <name>GUI_SpectrogramPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_SpectrogramPainter.cpp" line="+77"/>
        <source>Spectrogram</source>
        <translation>Spektrogram</translation>
    </message>
</context>
<context>
    <name>GUI_Spectrum</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Spectrum.cpp" line="+150"/>
        <source>Spectrum</source>
        <translation>Spektrum</translation>
    </message>
</context>
<context>
    <name>GUI_StyleSettings</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_StyleSettings.cpp" line="+203"/>
        <source>There are some unsaved settings&lt;br /&gt;Save now?</source>
        <translation>Nastavení ještě nejsou uložena&lt;br /&gt;Uložit?</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Please specify a name</source>
        <translation>Zadejte, prosím, název</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Save changes?</source>
        <translation>Uložit změny?</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistChooser</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_PlaylistChooser.cpp" line="+91"/>
        <location line="+38"/>
        <source>No playlists found</source>
        <translation>Nenalezen žádný seznam skladeb</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Could not rename playlist</source>
        <translation>Nepodařilo se přejmenovat seznam skladeb</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Name is invalid</source>
        <translation>Název není platný</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Do you really want to delete %1?</source>
        <translation>Opravdu chcete smazat %1?</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not delete playlist %1</source>
        <translation>Nepodařilo se smazat seznam skladeb %1</translation>
    </message>
</context>
<context>
    <name>Gui::AbstractStationPlugin</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/AbstractStationPlugin.cpp" line="+243"/>
        <source>Cannot open stream</source>
        <translation>Nelze otevřít vysílání (datový proud)</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please choose another name</source>
        <translation>Vyberte, prosím, jiný název</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Do you really want to delete %1</source>
        <translation>Opravdu chcete smazat %1</translation>
    </message>
</context>
<context>
    <name>GUI_Podcasts</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Podcasts.cpp" line="+65"/>
        <source>Podcast</source>
        <translation>Zvukový záznam</translation>
    </message>
</context>
<context>
    <name>GUI_Stream</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Stream.cpp" line="+68"/>
        <source>Search radio station</source>
        <translation>Hledat rozhlasovou stanici</translation>
    </message>
</context>
<context>
    <name>GUI_EnginePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Engine/GUI_EnginePreferences.cpp" line="+53"/>
        <source>Audio</source>
        <translation>Zvuk</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutEntry</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutEntry.cpp" line="+55"/>
        <source>Enter shortcut</source>
        <translation>Zadat klávesovou zkratku</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Shortcut already in use</source>
        <translation>Zkratka se již používá</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Test</source>
        <translation>Vyzkoušet</translation>
    </message>
</context>
<context>
    <name>SomaFM::StationModel</name>
    <message>
        <location filename="../src/Gui/SomaFM/SomaFMStationModel.cpp" line="+126"/>
        <source>Cannot fetch stations</source>
        <translation>Nelze natáhnout stanice</translation>
    </message>
</context>
<context>
    <name>SC::GUI_ArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.cpp" line="+79"/>
        <source>Query too short</source>
        <translation>Dotaz příliš krátký</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>No artists found</source>
        <translation>Nenalezeni žádní umělci</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>Found %n artist(s)</source>
        <translation><numerusform>Nalezen %1 umělec</numerusform><numerusform>Nalezeni %1 umělci</numerusform><numerusform>Nalezeno %1 umělců</numerusform><numerusform>Nalezeno %1 umělců</numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+56"/>
        <source>%n playlist(s) found</source>
        <translation><numerusform>Nalezen %1 seznam skladeb</numerusform><numerusform>Nalezeny %1 seznamy skladeb</numerusform><numerusform>Nalezeno %1 seznamů skladeb</numerusform><numerusform>Nalezeno %1 seznamů skladeb</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::GUI_Library</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.cpp" line="+56"/>
        <source>Add artist</source>
        <translation>Přidat umělce</translation>
    </message>
</context>
<context>
    <name>TagTextInput</name>
    <message>
        <location filename="../src/Gui/Tagging/TagTextInput.cpp" line="+126"/>
        <source>Very first letter to upper case</source>
        <translation>Úplně první písmeno na velké písmeno</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First letters to upper case</source>
        <translation>První písmena na velká písmena</translation>
    </message>
</context>
<context>
    <name>Library::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Utils/ContextMenu/LibraryContextMenu.cpp" line="+196"/>
        <source>Play in new tab</source>
        <translation>Přehrát v nové kartě</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Standard view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cover view</source>
        <translation>Pohled s obaly alb</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbar</source>
        <translation>Nástrojový pruh</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>The toolbar is visible when there are tracks with differing file types listed in the track view</source>
        <translation>Nástrojový pruh je viditelný, když jsou v pohledu na skladby vypsány skladby s lišícími se souborovými typy</translation>
    </message>
</context>
<context>
    <name>Gui::ImageSelectionDialog</name>
    <message>
        <location filename="../src/Gui/Utils/ImageSelectionDialog.cpp" line="+57"/>
        <source>Image files</source>
        <translation>Obrázkové soubory</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Any files</source>
        <translation>Všechny soubory</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open image files</source>
        <translation>Otevřít soubory s obrázky</translation>
    </message>
</context>
<context>
    <name>Gui::StreamRecorderPreferenceAction</name>
    <message>
        <location filename="../src/Gui/Utils/PreferenceAction.cpp" line="+163"/>
        <location line="+7"/>
        <source>Stream Recorder</source>
        <translation>Nahrávání vysílání</translation>
    </message>
</context>
<context>
    <name>Gui::ShortcutPreferenceAction</name>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Shortcuts</source>
        <translation>Klávesové zkratky</translation>
    </message>
</context>
<context>
    <name>Gui::MiniSearcher</name>
    <message>
        <location filename="../src/Gui/Utils/SearchableWidget/MiniSearcher.cpp" line="+92"/>
        <source>Arrow up</source>
        <translation>Šipka nahoru</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Previous search result</source>
        <translation>Předchozí výsledek hledání</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Arrow down</source>
        <translation>Šipka dolů</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Next search result</source>
        <translation>Další výsledek hledání</translation>
    </message>
</context>
<context>
    <name>Lang</name>
    <message>
        <location filename="../src/Utils/Language/Language.cpp" line="+79"/>
        <source>About</source>
        <translation>O programu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>Činnost</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Actions</source>
        <translation>Činnosti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Activate</source>
        <translation>Zapnout</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>Činné</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add tab</source>
        <translation>Přidat kartu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artists</source>
        <translation>Umělci alba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Albums</source>
        <translation>Alba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Append</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Application</source>
        <translation>Program</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>Použít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artist</source>
        <translation>Umělec</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artists</source>
        <translation>Umělci</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ascending</source>
        <translation>Vzestupně</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic</source>
        <translation>Automaticky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bitrate</source>
        <translation>Datový tok</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bookmarks</source>
        <translation>Záložky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Broadcast</source>
        <translation>Vysílání</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>by</source>
        <translation> od </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot find Lame MP3 encoder</source>
        <translation>Nelze najít kodér MP3 Lame</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>Vyprázdnit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear selection</source>
        <translation>Vyprázdnit výběr</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close others</source>
        <translation>Zavřít ostatní</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close tab</source>
        <translation>Zavřít kartu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment</source>
        <translation>Poznámka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Covers</source>
        <translation>Obaly</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created</source>
        <translation>Vytvořeno</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create new directory</source>
        <translation>Vytvořit nový adresář</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create a new library</source>
        <translation>Vytvořit novou sbírku</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark Mode</source>
        <translation>Tmavý vzhled</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Days</source>
        <translation>Dny</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>d</source>
        <translation> d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Default</source>
        <translation>Výchozí</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+84"/>
        <source>Delete</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location line="-82"/>
        <source>Descending</source>
        <translation>Sestupně</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory</source>
        <translation>Adresář</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directories</source>
        <translation>Adresáře</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disc</source>
        <translation>Disk</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Duration</source>
        <translation>Doba trvání</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dur.</source>
        <translation>Doba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dynamic playback</source>
        <translation>Dynamické přehrávání</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Empty input</source>
        <translation>Prázdný vstup</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter name</source>
        <translation>Zadat název</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please enter new name</source>
        <translation>Zadejte, prosím, nový název</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter URL</source>
        <translation>Zadat adresu (URL)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry</source>
        <translation>Záznam</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entries</source>
        <translation>Záznamy</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fast</source>
        <translation>Rychle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filename</source>
        <translation>Název souboru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Files</source>
        <translation>Soubory</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filesize</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File type</source>
        <translation>Typ souboru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filter</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>1st</source>
        <translation>1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Font</source>
        <translation>Písmo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation>Písma</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fulltext</source>
        <translation>Vyhledávání v celém textu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Gapless playback</source>
        <translation>Přehrávání bez mezer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genre</source>
        <translation>Žánr</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genres</source>
        <translation>Žánry</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hide</source>
        <translation>Skrýt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hours</source>
        <translation>Hodin</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>h</source>
        <translation> h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import directory</source>
        <translation>Zavést adresář</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import files</source>
        <translation>Zavést soubory</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation>Nečinné</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Info</source>
        <translation>Informace</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading</source>
        <translation>Nahrávání</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading %1</source>
        <translation>Nahrává se %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid characters</source>
        <translation>Neplatné znaky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+f</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab</source>
        <translation>Karta</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library</source>
        <translation>Sbírka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library path</source>
        <translation>Cesta ke sbírce</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library view type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Listen</source>
        <translation>Poslech</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Live Search</source>
        <translation>Živé hledání</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Logger</source>
        <translation>Zápis činnosti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lyrics</source>
        <translation>Text písně</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Menu</source>
        <translation>Nabídka</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>Minimalizovat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minutes</source>
        <translation>Minut</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>m</source>
        <translation> m</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing</source>
        <translation>Chybí</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Modified</source>
        <translation>Změněno</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Months</source>
        <translation>Měsíce</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute</source>
        <translation>Ztlumit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute off</source>
        <translation>Zhlasit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation>Nový</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next page</source>
        <translation>Další strana</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next track</source>
        <translation>Další skladba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No albums</source>
        <translation>Žádná alba</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+144"/>
        <source>Tracks</source>
        <translation>Skladby</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Move down</source>
        <translation>Posunout dolů</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Move up</source>
        <translation>Posunout nahoru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>on</source>
        <translation>na</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Otevřít</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open directory</source>
        <translation>Otevřít adresář</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open file</source>
        <translation>Otevřít soubor</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>or</source>
        <translation>nebo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Overwrite</source>
        <translation>Přepsat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pause</source>
        <translation>Pozastavit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play</source>
        <translation>Přehrát</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playing time</source>
        <translation>Doba přehrávání</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play in new tab</source>
        <translation>Přehrát v nové kartě</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlist</source>
        <translation>Seznam skladeb</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlists</source>
        <translation>Seznamy skladeb</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play next</source>
        <translation>Přehrát jako další</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play/Pause</source>
        <translation>Přehrát/Pozastavit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugin</source>
        <translation>Modul</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Podcasts</source>
        <translation>Zvukové záznamy</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous page</source>
        <translation>Předchozí strana</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous track</source>
        <translation>Předchozí skladba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Purchase Url</source>
        <translation>Koupit u</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Quit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio</source>
        <translation>Rádio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio Station</source>
        <translation>Rozhlasová stanice</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rating</source>
        <translation>Hodnocení</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Really</source>
        <translation>Skutečně</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Refresh</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload Library</source>
        <translation>Nahrát sbírku znovu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename</source>
        <translation>Přejmenovat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat 1</source>
        <translation>Opakovat 1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat all</source>
        <translation>Opakovat vše</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Replace</source>
        <translation>Nahradit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation>Obnovit výchozí</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation>Opakovat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reverse order</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Sampler</source>
        <translation>Míchání hudby </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shuffle</source>
        <translation>Zamíchat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shutdown</source>
        <translation>Vypnutí</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save as</source>
        <translation>Uložit jako</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save to file</source>
        <translation>Uložit do souboru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan for audio files</source>
        <translation>Prohledat na zvukové soubory</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Search</source>
        <translation>Hledání</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search next</source>
        <translation>Hledat další</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search previous</source>
        <translation>Hledat předchozí</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2nd</source>
        <translation>2.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seconds</source>
        <translation>Sekund</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>s</source>
        <translation> s</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek backward</source>
        <translation>Přetočit zpět</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek forward</source>
        <translation>Přetočit vpřed</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show</source>
        <translation>Ukázat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Album Artists</source>
        <translation>Ukázat umělce alba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Covers</source>
        <translation>Ukázat obaly</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Library</source>
        <translation>Ukázat sbírku</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Similar artists</source>
        <translation>Podobní umělci</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by</source>
        <translation>Řadit dle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stop</source>
        <translation>Zastavit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streams</source>
        <translation>Vysílání</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream URL</source>
        <translation>Adresa (URL) vysílání (datového proudu)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Success</source>
        <translation>Úspěch</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>th</source>
        <translation>.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3rd</source>
        <translation>3.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Title</source>
        <translation>Název</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track</source>
        <translation>Skladba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track number</source>
        <translation>Číslo skladby</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>track on</source>
        <translation>skladba na</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Tree</source>
        <translation>Strom</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Undo</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown album</source>
        <translation>Neznámé album</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown artist</source>
        <translation>Neznámý umělec</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown title</source>
        <translation>Neznámý název</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown genre</source>
        <translation>Neznámý žánr</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown placeholder</source>
        <translation>Neznámý zástupný znak</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown year</source>
        <translation>Neznámý rok</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Various</source>
        <translation>Různé</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various albums</source>
        <translation>Různá alba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various artists</source>
        <translation>Různí umělci</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various tracks</source>
        <translation>Různé skladby</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume down</source>
        <translation>Snížit hlasitost</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume up</source>
        <translation>Zvýšit hlasitost</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Weeks</source>
        <translation>Týdny</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Year</source>
        <translation>Rok</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Years</source>
        <translation>Roky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>Přiblížení</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>No directories</source>
        <translation>Žádné adresáře</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n directory(s)</source>
        <translation><numerusform>%n adresář</numerusform><numerusform>%n adresáře</numerusform><numerusform>%n adresářů</numerusform><numerusform>%n adresářů</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No files</source>
        <translation>Žádné soubory</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n file(s)</source>
        <translation><numerusform>%n soubor</numerusform><numerusform>%n soubory</numerusform><numerusform>%n souborů</numerusform><numerusform>%n souborů</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No playlists</source>
        <translation>Žádné seznamy skladeb</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n playlist(s)</source>
        <translation><numerusform>%n seznam skladeb</numerusform><numerusform>%n seznamy skladeb</numerusform><numerusform>%n seznamů skladeb</numerusform><numerusform>%n seznamů skladeb</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks</source>
        <translation>Žádné skladby</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s)</source>
        <translation><numerusform>%n skladba</numerusform><numerusform>%n skladby</numerusform><numerusform>%n skladeb</numerusform><numerusform>%n skladeb</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks found</source>
        <translation>Nenalezeny žádné skladby</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s) found</source>
        <translation><numerusform>Nalezena %1 skladba</numerusform><numerusform>Nalezeny %1 skladby</numerusform><numerusform>Nalezeno %1 skladeb</numerusform><numerusform>Nalezeno %1 skladeb</numerusform></translation>
    </message>
</context>
</TS>