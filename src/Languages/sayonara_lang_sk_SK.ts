<?xml version="1.0" ?><!DOCTYPE TS><TS language="sk_SK" sourcelanguage="en" version="2.1">
<context>
    <name>GUI_AlternativeCovers</name>
    <message>
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.ui" line="+27"/>
        <source>Online Search</source>
        <translation>Online vyhľadávanie</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Automatic search</source>
        <translation>Automatické vyhľadávanie</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Text search</source>
        <translation>Textové vyhľadávanie</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Local Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Select and preview multiple covers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+108"/>
        <source>Alternative Cover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Start search automatically</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.cpp" line="+283"/>
        <source>%n cover(s) found</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+133"/>
        <location line="+31"/>
        <source>Also save cover to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-11"/>
        <source>Cover web search is not enabled</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_History</name>
    <message>
        <location filename="../src/Gui/History/GUI_History.ui" line="+14"/>
        <source>Dialog</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>Newer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Older</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.ui" line="+20"/>
        <source>Info / Edit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+247"/>
        <source>Loading files...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Lyrics</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.ui" line="+28"/>
        <source>Save Lyrics</source>
        <translation>Uložiť text</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>switch</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+56"/>
        <source>Zoom</source>
        <translation>Priblíženie</translation>
    </message>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.cpp" line="+303"/>
        <source>Save lyrics not supported</source>
        <translation>Ukladanie textu nie je podporované</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Overwrite lyrics</source>
        <translation>Prepísať text</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save lyrics</source>
        <translation>Uložiť text</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
</context>
<context>
    <name>GUI_ImportDialog</name>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.ui" line="+14"/>
        <source>Import</source>
        <translation>Importovať</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Import tracks to library</source>
        <translation>Importovať skladby do knižnice</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select target folder</source>
        <translation>Zvoliť cieľový adresár</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.cpp" line="+126"/>
        <source>Loading tracks</source>
        <translation>Načítavam skladby</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No tracks</source>
        <translation>Žiadne skladby</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Importing</source>
        <translation>Importujem</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>Dokončené</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Rollback</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Cancelled</source>
        <translation>Zrušené</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Choose target directory</source>
        <translation>Zvoliť cieľový priečinok</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>%1&lt;br /&gt;is no library directory</source>
        <translation>%1&lt;br /&gt;nie je priečinok knižnice</translation>
    </message>
</context>
<context>
    <name>GUI_LocalLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.ui" line="+17"/>
        <source>Library</source>
        <translation>Knižnica</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.cpp" line="+352"/>
        <source>Audio files</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Controls</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Controls.ui" line="+318"/>
        <source>Written by Michael Lugmair (Lucio Carreras)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Player</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Player.ui" line="+14"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
</context>
<context>
    <name>GUI_AudioConverter</name>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.ui" line="+45"/>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="+277"/>
        <source>Start</source>
        <translation>Spustiť</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Stop</source>
        <translation>Zastaviť</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>#Threads</source>
        <translation>#Vlákna</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+187"/>
        <source>Quality</source>
        <translation>Kvalita</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+52"/>
        <location line="+89"/>
        <source>Bitrate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="-164"/>
        <source>Audio Converter</source>
        <translation>Konvertor zvuku</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Threads</source>
        <translation>Vlákna</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+123"/>
        <source>Cannot find encoder</source>
        <translation>Nepodarilo sa nájsť kodér</translation>
    </message>
    <message>
        <location line="-113"/>
        <location line="+14"/>
        <source>Playlist does not contain tracks which are supported by the converter</source>
        <translation>Playlist neobsahuje skladby, ktoré by tento konvertor podporoval</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>No track will be converted.</source>
        <translation>Neskonvertujú sa žiadne skladby.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>These tracks will be ignored</source>
        <translation>Tieto skladby budú ignorované</translation>
    </message>
    <message numerus="yes">
        <location line="+38"/>
        <source>Failed to convert %n track(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please check the log files</source>
        <translation>Prosím, skontrolujte záznamy aplikácie</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>All tracks could be converted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully finished</source>
        <translation>Úspešne dokončené</translation>
    </message>
</context>
<context>
    <name>GUI_Bookmarks</name>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.ui" line="+116"/>
        <source>Loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.cpp" line="+77"/>
        <location line="+53"/>
        <source>No bookmarks found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+122"/>
        <source>Sorry, bookmarks can only be set for library tracks at the moment.</source>
        <translation>Prepáčte, záložky je v tejto chvíli možné nastaviť len pre skladby</translation>
    </message>
</context>
<context>
    <name>GUI_Broadcast</name>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.ui" line="+88"/>
        <source>Cannot Broadcast</source>
        <translation>Nemôžem vysielať</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.cpp" line="+104"/>
        <source>Dismiss</source>
        <translation>Odmietnuť</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dismiss all</source>
        <translation>Odmietnuť všetky</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%n listener(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot broadcast on port %1</source>
        <translation>Nemožno vysielať na porte %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maybe another application is using this port?</source>
        <translation>Možno tento port používa iná aplikácia?</translation>
    </message>
</context>
<context>
    <name>GUI_Speed</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.ui" line="+48"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+134"/>
        <source>Speed</source>
        <translation>Rýchlosť</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Preserve pitch</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+1"/>
        <source>Pitch</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+2"/>
        <source>%1 and %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+70"/>
        <location line="+1"/>
        <source>%1 not found</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Style</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Style.ui" line="+14"/>
        <source>Style</source>
        <translation>Vzhľad</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Spectrum</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Vert. spacing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+88"/>
        <source>Rect height</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-68"/>
        <location line="+44"/>
        <source>Hor. spacing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-30"/>
        <location line="+75"/>
        <source>Fading steps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-51"/>
        <source>Level</source>
        <translation>Úroveň</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Rect width</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Ver. spacing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+54"/>
        <source>Style settings</source>
        <translation>Nastavenia vzhľadu</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Color 2</source>
        <translation>Farba 2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 1</source>
        <translation>Farba 1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color 3</source>
        <translation>Farba 3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 4</source>
        <translation>Farba 4</translation>
    </message>
</context>
<context>
    <name>GUI_TargetPlaylistDialog</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_TargetPlaylistDialog.ui" line="+14"/>
        <source>Choose target playlist</source>
        <translation>Zvoľte cieľový playlist</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>relative filepaths</source>
        <translation>relatívne cesty súborov</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Close</source>
        <translation>Zatvoriť</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save</source>
        <translation>Uložiť</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save playlist as...</source>
        <translation>Uložiť playlist ako...</translation>
    </message>
</context>
<context>
    <name>GUI_StationSearcher</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.ui" line="+14"/>
        <source>Search Radio Station</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.cpp" line="+58"/>
        <source>Show radio stations from %1 to %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+204"/>
        <source>Country</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+79"/>
        <source>Type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Url</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_BroadcastPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.ui" line="+20"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ask for permission</source>
        <translation>Vypýtať povolenie</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.cpp" line="+161"/>
        <source>Port %1 already in use</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_CoverPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Covers/GUI_CoverPreferences.ui" line="+62"/>
        <source>Inactive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>Aktívne</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Delete all covers from the database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear cache</source>
        <translation>Vymazať vyrovnávaciu pamäť</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete all covers from the Sayonara directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete files</source>
        <translation>Odstrániť súbory</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Save found covers into the library directory where the audio files are located</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers into library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Fetch missing covers from the internet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Saving covers to the database leads to significantly faster access but results in a bigger database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers to database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Save found covers into Sayonara directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Cover name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Name of cover file</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_LanguagePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.ui" line="+29"/>
        <source>Check for update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Download update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+33"/>
        <source>English</source>
        <translation>Angličtina</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>For new languages I am always looking for translators</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.cpp" line="+63"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Cannot check for language update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>Language update available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Language is up to date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+41"/>
        <location line="+24"/>
        <source>Cannot fetch language update</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-10"/>
        <source>Language was updated successfully</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_LastFmPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.ui" line="+27"/>
        <source>Username</source>
        <translation>Používateľské meno</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Scrobble time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>Login now</source>
        <translation>Prihlásiť sa</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.cpp" line="+163"/>
        <source>Logged in</source>
        <translation>Prihlásený</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not logged in</source>
        <translation>Neprihlásený</translation>
    </message>
</context>
<context>
    <name>GUI_LibraryPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.ui" line="+27"/>
        <source>Libraries</source>
        <translation>Knižnice</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Library-Playlist Interaction</source>
        <translation>Interakcia knižnice a playlistu</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>When drag and drop into playlist </source>
        <translation>Pri pretiahnutí do playlistu</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+36"/>
        <source>do nothing (default)</source>
        <translation>nespraviť nič (predvolené)</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>start if stopped and playlist is empty</source>
        <translation>spustiť ak je zastavené a playlist je prázdny</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>On double click, create playlist and</source>
        <translation>Pri dvojkliku vytvoriť playlist a</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start playback if stopped</source>
        <translation>spustiť prehrávanie ak je zastavené</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>start playback immediately</source>
        <translation>spustiť prehrávanie okamžite</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(this is ignored when playlist is in &apos;append mode&apos;)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Other</source>
        <translation>Ostatné</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show &quot;Clear selection&quot; buttons</source>
        <translation>Zobraziť tlačidlo &quot;Vyčistiť výber&quot;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Ignore English article &quot;The&quot; in artist name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.cpp" line="+147"/>
        <source>Cannot edit library</source>
        <translation>Nemôžem upraviť knižnicu</translation>
    </message>
</context>
<context>
    <name>GUI_NotificationPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.ui" line="+17"/>
        <source>Timeout (ms)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.cpp" line="+98"/>
        <source>Notifications</source>
        <translation>Notifikácie</translation>
    </message>
</context>
<context>
    <name>GUI_PlayerPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.ui" line="+44"/>
        <source>Show system tray icon</source>
        <translation>Zobraziť ikonu v oznamovacej oblasti</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Hide instead of close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Start hidden</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Update notifications</source>
        <translation>Aktualizovať notifikácie</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.cpp" line="+107"/>
        <source>This might cause Sayonara not to show up again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>In this case use the &apos;--show&apos; option at the next startup.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_PlaylistPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.ui" line="+36"/>
        <source>Behavior</source>
        <translation>Správanie</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Load temporary playlists</source>
        <translation>Načítať dočasné playlisty</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load saved playlists</source>
        <translation>Načítať uložené playlisty</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start playing</source>
        <translation>Spustiť prehrávanie</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load last track on startup</source>
        <translation>Načítať poslednú skladbu pri štarte</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remember time of last track</source>
        <translation>Zapamätať čas poslednej skladby</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stop behaviour</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Load last track after pressing stop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Look</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Show numbers</source>
        <translation>Zobraziť čísla</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>&apos;italic text&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Example</source>
        <translation>Príklad</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>*bold text*</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+72"/>
        <source>Show clear button</source>
        <translation>Zobraziť tlačidlo vyčistenia</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Show covers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Show rating</source>
        <translation>Zobraziť hodnotenie</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.cpp" line="+216"/>
        <source>Playlist look: Invalid expression</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_PreferenceDialog</name>
    <message>
        <location filename="../src/Gui/Preferences/PreferenceDialog/GUI_PreferenceDialog.ui" line="+136"/>
        <source>Preferences</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Apply</source>
        <translation>Použiť</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>GUI_ProxyPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.ui" line="+20"/>
        <source>Host</source>
        <translation>Hostiteľ</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Username</source>
        <translation>Používateľské meno</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Save username/password</source>
        <translation>Uložiť meno a heslo</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Active</source>
        <translation>Aktívne</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Automatic search</source>
        <translation>Automatické vyhľadávanie</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.cpp" line="+57"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
</context>
<context>
    <name>GUI_RemoteControlPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.ui" line="+36"/>
        <source>Detectable via UDP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Remote control URL</source>
        <translation>URL vzdialeného ovládania</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>UDP port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.cpp" line="+59"/>
        <source>If activated, Sayonara will answer an UDP request that it is remote controlable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+36"/>
        <source>Remote control</source>
        <translation>Vzdialené ovládanie</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Port %1 already in use</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_SearchPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Search/GUI_SearchPreferences.ui" line="+23"/>
        <source>Example</source>
        <translation>Príklad</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Case insensitive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Option</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Ignore accents</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>Ignore special characters</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_ShortcutPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.ui" line="+44"/>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="+149"/>
        <source>Press shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="-48"/>
        <source>Shortcuts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+93"/>
        <source>Double shortcuts found</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_StreamRecorderPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.ui" line="+30"/>
        <source>General</source>
        <translation>Všeobecné</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Target directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+40"/>
        <source>Automatic recording</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+45"/>
        <source>Create session directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Choose available placeholders</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+37"/>
        <source>Path template</source>
        <translation>Šablóna cesty</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Example</source>
        <translation>Príklad</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.cpp" line="+172"/>
        <source>Choose target directory</source>
        <translation>Zvoľte cieľový priečinok</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Target directory is empty</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Please choose another directory</source>
        <translation>Prosím, vyberte iný priečinok</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cannot create %1</source>
        <translation>Nemôžem vytvoriť %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Template path is not valid</source>
        <translation>Šablóna cesty nie je platná</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stream recorder</source>
        <translation>Nahrávač vysielania</translation>
    </message>
</context>
<context>
    <name>GUI_StreamPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.ui" line="+17"/>
        <source> ms</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Buffer size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Show history</source>
        <translation>Zobraziť históriu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Streams in new tab</source>
        <translation>Otvoriť Vysielania v novej karte</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.cpp" line="+57"/>
        <source>%1 and %2</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_CssEditor</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_CssEditor.ui" line="+97"/>
        <source>Edit style sheet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Dark mode</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_FontPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.ui" line="+84"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Font size</source>
        <translation>Veľkosť písma</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bold</source>
        <translation>Tučné</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Font name</source>
        <translation>Názov písma</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.cpp" line="+129"/>
        <location line="+1"/>
        <source>Inherit</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_IconPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.ui" line="+20"/>
        <source>Also apply this icon theme to the dark style</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.cpp" line="+96"/>
        <location line="+104"/>
        <source>System theme</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-96"/>
        <source>Icons</source>
        <translation>Ikony</translation>
    </message>
</context>
<context>
    <name>GUI_UiPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.ui" line="+57"/>
        <source>Fading cover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit style sheet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.cpp" line="+48"/>
        <source>User Interface</source>
        <translation>Používateľské rozhranie</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>General</source>
        <translation>Všeobecné</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show large cover</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Shutdown</name>
    <message>
        <location filename="../src/Gui/Shutdown/GUI_Shutdown.ui" line="+14"/>
        <location line="+69"/>
        <source>Shutdown</source>
        <translation>Vypnúť</translation>
    </message>
    <message>
        <location line="-56"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>minutes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shutdown after playlist finished</source>
        <translation>Vypnúť po skončení playlistu</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Shutdown after</source>
        <translation>Vypnúť po</translation>
    </message>
</context>
<context>
    <name>GUI_SomaFM</name>
    <message>
        <location filename="../src/Gui/SomaFM/GUI_SomaFM.ui" line="+42"/>
        <source>Donate to Soma.fm</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+102"/>
        <source>Streams</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_SoundcloudArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.ui" line="+14"/>
        <source>Search Soundcloud</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add</source>
        <translation>Pridať</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Search artist</source>
        <translation>Vyhľadať autora</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudLibrary</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.ui" line="+26"/>
        <source>Library</source>
        <translation>Knižnica</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Search for title, interprets and albums</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_CoverEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.ui" line="+57"/>
        <source>Replace</source>
        <translation>Nahradiť</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="+223"/>
        <source>Original</source>
        <translation>Originál</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="-56"/>
        <location line="+9"/>
        <source>File has no cover</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_FailMessageBox</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.ui" line="+22"/>
        <source>Details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.cpp" line="+55"/>
        <source>File exists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Writeable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+70"/>
        <source>Some files could not be saved</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_TagEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.ui" line="+115"/>
        <source>Discnumber</source>
        <translation>Číslo disku</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Album artist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+113"/>
        <source>Comment</source>
        <translation>Komentár</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>all</source>
        <translation>všetky</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tag from path</source>
        <translation>Štítok z cesty</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Undo all</source>
        <translation>Vrátiť všetko</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Read only file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.cpp" line="+161"/>
        <source>Load complete album</source>
        <translation>Načítať úplný album</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Metadata</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Tags from path</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+81"/>
        <source>Cannot apply expression to %n track(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore these tracks?</source>
        <translation>Ignorovať tieto skladby?</translation>
    </message>
    <message>
        <location line="+394"/>
        <source>All changes will be lost</source>
        <translation>Všetky zmeny budú stratené</translation>
    </message>
</context>
<context>
    <name>GUI_TagFromPath</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.ui" line="+17"/>
        <source>Expression</source>
        <translation>Výraz</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Disc Nr</source>
        <translation>Číslo disku</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Apply to all</source>
        <translation>Použiť pre všetky</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.cpp" line="+92"/>
        <source>Tag</source>
        <translation>Štítok</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Please select text first</source>
        <translation>Prosím, najprv vyberte text</translation>
    </message>
</context>
<context>
    <name>StreamServer</name>
    <message>
        <location filename="../src/Components/Broadcasting/StreamServer.cpp" line="+228"/>
        <source>%1 wants to listen to your music.</source>
        <translation>%1 chce počúvať vašu hudbu.</translation>
    </message>
</context>
<context>
    <name>AbstractLibrary</name>
    <message>
        <location filename="../src/Components/Library/AbstractLibrary.cpp" line="+849"/>
        <source>All %1 could be removed</source>
        <translation>Všetkých %1 bolo odstránených</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 %3 could not be removed</source>
        <translation>%1 z %2 %3 sa nepodarilo odstrániť</translation>
    </message>
</context>
<context>
    <name>Library::Importer</name>
    <message>
        <location filename="../src/Components/Library/Importer/LibraryImporter.cpp" line="+228"/>
        <source>Cannot import tracks</source>
        <translation>Nemôžem importovať skladby</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>All files could be imported</source>
        <translation>Všetky súbory boli importované</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 files could be imported</source>
        <translation>Importovalo sa %1 z %2 súborov</translation>
    </message>
</context>
<context>
    <name>Library::ReloadThread</name>
    <message>
        <location filename="../src/Components/Library/Threads/ReloadThread.cpp" line="+219"/>
        <source>Looking for covers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+40"/>
        <source>Reading files</source>
        <translation>Čítam súbory</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Deleting orphaned tracks</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Lyrics::LookupThread</name>
    <message>
        <location filename="../src/Components/Lyrics/LyricLookup.cpp" line="+171"/>
        <location line="+35"/>
        <source>Cannot fetch lyrics from %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>No lyrics found</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Shutdown</name>
    <message>
        <location filename="../src/Components/Shutdown/Shutdown.cpp" line="+100"/>
        <source>Computer will shutdown after playlist has finished</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+29"/>
        <location line="+35"/>
        <source>Computer will shutdown in %n minute(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
<context>
    <name>SC::JsonParser</name>
    <message>
        <location filename="../src/Components/Streaming/Soundcloud/SoundcloudJsonParser.cpp" line="+123"/>
        <source>Website</source>
        <translation>Webstránka</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+236"/>
        <source>Permalink Url</source>
        <translation>Permanentná URL</translation>
    </message>
    <message>
        <location line="-223"/>
        <source>Followers/Following</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+93"/>
        <location line="+134"/>
        <source>Purchase Url</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::CoverButton</name>
    <message>
        <location filename="../src/Gui/Covers/CoverButton.cpp" line="+119"/>
        <source>Search an alternative cover</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>HistoryContainer</name>
    <message>
        <location filename="../src/Gui/History/HistoryContainer.cpp" line="+25"/>
        <source>History</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.cpp" line="+92"/>
        <source>Write cover to tracks</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::CoverViewContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/CoverViewContextMenu.cpp" line="+217"/>
        <source>Toolbar</source>
        <translation>Panel nástrojov</translation>
    </message>
</context>
<context>
    <name>Library::GUI_CoverView</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/GUI_CoverView.cpp" line="+214"/>
        <source>Use Ctrl + mouse wheel to zoom</source>
        <translation>Použite Ctrl + koliesko myši pre priblíženie</translation>
    </message>
</context>
<context>
    <name>Directory::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryContextMenu.cpp" line="+295"/>
        <source>Rename by metadata</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Collapse all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to another library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to another library</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Directory::TreeView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryTreeView.cpp" line="+410"/>
        <source>Copy here</source>
        <translation>Kopírovať sem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move here</source>
        <translation>Presunúť sem</translation>
    </message>
</context>
<context>
    <name>GUI_DirectoryView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/GUI_DirectoryView.cpp" line="+183"/>
        <source>Could not create directory</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::GenreView</name>
    <message>
        <location filename="../src/Gui/Library/GenreView.cpp" line="+114"/>
        <source>Updating genres</source>
        <translation>Aktualizujem žánre</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Do you really want to remove %1 from all tracks?</source>
        <translation>Naozaj chcete odstrániť %1 zo všetkých skladieb?</translation>
    </message>
</context>
<context>
    <name>Library::GUI_EmptyLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_EmptyLibrary.cpp" line="+94"/>
        <source>Please choose a name for your library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Please choose another name for your library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>The file path is invalid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>A library with the same file path already exists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>A library which contains this file path already exists</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::HeaderView</name>
    <message>
        <location filename="../src/Gui/Library/Header/HeaderView.cpp" line="+198"/>
        <source>Resize columns</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::TrackModel</name>
    <message>
        <location filename="../src/Gui/Library/TableView/TrackModel.cpp" line="+144"/>
        <source>kBit/s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_DeleteDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_DeleteDialog.cpp" line="+87"/>
        <source>Only from library</source>
        <translation>Len z knižnice</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>You are about to delete %n file(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
<context>
    <name>Library::GUI_LibraryReloadDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_ReloadLibraryDialog.cpp" line="+78"/>
        <source>Fast scan</source>
        <translation>Rýchle prehľadávanie</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deep scan</source>
        <translation>Hĺbkové prehľadávanie</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Only scan for new and deleted files</source>
        <translation>Nájsť len nové a vymazané súbory</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scan all files in your library directory</source>
        <translation>Hľadať všteky súbory v priečinku knižnice</translation>
    </message>
</context>
<context>
    <name>Library::LocalLibraryMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/LocalLibraryMenu.cpp" line="+154"/>
        <source>Statistics</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit library</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::MergeMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/MergeMenu.cpp" line="+52"/>
        <location line="+53"/>
        <source>Merge</source>
        <translation>Spojiť</translation>
    </message>
</context>
<context>
    <name>GUI_ControlsBase</name>
    <message>
        <location filename="../src/Gui/Player/GUI_ControlsBase.cpp" line="+71"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
</context>
<context>
    <name>GUI_Logger</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.cpp" line="+275"/>
        <source>Cannot open file</source>
        <translation>Nepodarilo sa otvoriť súbor</translation>
    </message>
</context>
<context>
    <name>Menubar</name>
    <message>
        <location filename="../src/Gui/Player/GUI_PlayerMenubar.cpp" line="+323"/>
        <source>View</source>
        <translation>Zobraziť</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+18"/>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Show large cover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Fullscreen</source>
        <translation>Na celú obrazovku</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Media files</source>
        <translation>Mediálne súbory</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Otvoriť mediálne súbory</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please visit the forum at</source>
        <translation>Prosím, navštívte fórum na</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>FAQ</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>About Sayonara</source>
        <translation>O Sayonara</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Donate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Special thanks to all the brave translators</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TrayIconContextMenu</name>
    <message>
        <location filename="../src/Gui/Player/GUI_TrayIcon.cpp" line="+176"/>
        <source>Current song</source>
        <translation>Súčasná skladba</translation>
    </message>
</context>
<context>
    <name>VersionChecker</name>
    <message>
        <location filename="../src/Gui/Player/VersionChecker.cpp" line="+77"/>
        <source>A new version is available!</source>
        <translation>Nová verzia dostupná!</translation>
    </message>
</context>
<context>
    <name>GUI_Playlist</name>
    <message>
        <location filename="../src/Gui/Playlist/GUI_Playlist.cpp" line="+241"/>
        <source>Playlist empty</source>
        <translation>Playlist prázdny</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Media files</source>
        <translation>Mediálne súbory</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Otvoriť mediálne súbory</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Playlist name already exists</source>
        <translation>Playlist s rovnakým názvom už existuje</translation>
    </message>
</context>
<context>
    <name>Playlist::ActionMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistActionMenu.cpp" line="+193"/>
        <source>Please set library path first</source>
        <translation>Prosím, najprv nastavte cestu ku knižnici</translation>
    </message>
</context>
<context>
    <name>Playlist::BottomBar</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistBottomBar.cpp" line="+242"/>
        <source>Please set library path first</source>
        <translation>Prosím, najprv nastavte cestu ku knižnici</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel shutdown?</source>
        <translation>Zrušiť vypnutie?</translation>
    </message>
</context>
<context>
    <name>Playlist::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistContextMenu.cpp" line="+171"/>
        <source>Jump to current track</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show track in library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Playlist mode</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Playlist::Model</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistModel.cpp" line="+474"/>
        <source>Goto row</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Crossfader</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Crossfader.cpp" line="+75"/>
        <location line="+12"/>
        <source>Crossfader</source>
        <translation>Prelínač</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 and %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+67"/>
        <source>Crossfader does not work with Alsa</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Gapless playback does not work with Alsa</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Equalizer</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Equalizer.cpp" line="+137"/>
        <location line="+40"/>
        <source>Linked sliders</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-10"/>
        <source>Equalizer</source>
        <translation>Ekvalizér</translation>
    </message>
    <message>
        <location line="+248"/>
        <source>Name %1 not allowed</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_LevelPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_LevelPainter.cpp" line="+157"/>
        <source>Level</source>
        <translation>Úroveň</translation>
    </message>
</context>
<context>
    <name>GUI_SpectrogramPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_SpectrogramPainter.cpp" line="+77"/>
        <source>Spectrogram</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Spectrum</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Spectrum.cpp" line="+150"/>
        <source>Spectrum</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_StyleSettings</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_StyleSettings.cpp" line="+203"/>
        <source>There are some unsaved settings&lt;br /&gt;Save now?</source>
        <translation>Nejaké zmeny nie sú uložené.&lt;br /&gt;Uložiť teraz?</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Please specify a name</source>
        <translation>Prosím, zadajte názov</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Save changes?</source>
        <translation>Uložiť zmeny?</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistChooser</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_PlaylistChooser.cpp" line="+91"/>
        <location line="+38"/>
        <source>No playlists found</source>
        <translation>Nenašli sa žiadne playlisty</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Could not rename playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Name is invalid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Do you really want to delete %1?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not delete playlist %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::AbstractStationPlugin</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/AbstractStationPlugin.cpp" line="+243"/>
        <source>Cannot open stream</source>
        <translation>Nemôžem otvoriť stream</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please choose another name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+36"/>
        <source>Do you really want to delete %1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Podcasts</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Podcasts.cpp" line="+65"/>
        <source>Podcast</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Stream</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Stream.cpp" line="+68"/>
        <source>Search radio station</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_EnginePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Engine/GUI_EnginePreferences.cpp" line="+53"/>
        <source>Audio</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_ShortcutEntry</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutEntry.cpp" line="+55"/>
        <source>Enter shortcut</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Shortcut already in use</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+59"/>
        <source>Test</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SomaFM::StationModel</name>
    <message>
        <location filename="../src/Gui/SomaFM/SomaFMStationModel.cpp" line="+126"/>
        <source>Cannot fetch stations</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SC::GUI_ArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.cpp" line="+79"/>
        <source>Query too short</source>
        <translation>Dopyt príliš krátky</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>No artists found</source>
        <translation>Nenašli sa žiadni autori</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>Found %n artist(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+56"/>
        <source>%n playlist(s) found</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
<context>
    <name>SC::GUI_Library</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.cpp" line="+56"/>
        <source>Add artist</source>
        <translation>Pridať autora</translation>
    </message>
</context>
<context>
    <name>TagTextInput</name>
    <message>
        <location filename="../src/Gui/Tagging/TagTextInput.cpp" line="+126"/>
        <source>Very first letter to upper case</source>
        <translation>Veľké len prvé písmeno</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First letters to upper case</source>
        <translation>Kapitalizovať prvé písmená slov</translation>
    </message>
</context>
<context>
    <name>Library::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Utils/ContextMenu/LibraryContextMenu.cpp" line="+196"/>
        <source>Play in new tab</source>
        <translation>Prehrať na novej karte</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Standard view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cover view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbar</source>
        <translation>Panel nástrojov</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>The toolbar is visible when there are tracks with differing file types listed in the track view</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::ImageSelectionDialog</name>
    <message>
        <location filename="../src/Gui/Utils/ImageSelectionDialog.cpp" line="+57"/>
        <source>Image files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Any files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Open image files</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::StreamRecorderPreferenceAction</name>
    <message>
        <location filename="../src/Gui/Utils/PreferenceAction.cpp" line="+163"/>
        <location line="+7"/>
        <source>Stream Recorder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::ShortcutPreferenceAction</name>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Shortcuts</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Gui::MiniSearcher</name>
    <message>
        <location filename="../src/Gui/Utils/SearchableWidget/MiniSearcher.cpp" line="+92"/>
        <source>Arrow up</source>
        <translation>Šípka hore</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Previous search result</source>
        <translation>Predchádzajúci výsledok vyhľadávania</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Arrow down</source>
        <translation>Šípka dole</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Next search result</source>
        <translation>Ďalší výsledok vyhľadávania</translation>
    </message>
</context>
<context>
    <name>Lang</name>
    <message>
        <location filename="../src/Utils/Language/Language.cpp" line="+79"/>
        <source>About</source>
        <translation>O programe</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>Akcia</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Actions</source>
        <translation>Akcie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Activate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>Aktívne</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add</source>
        <translation>Pridať</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add tab</source>
        <translation>Pridať kartu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Albums</source>
        <translation>Albumy</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Append</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Application</source>
        <translation>Aplikácia</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>Použiť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Artists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Ascending</source>
        <translation>Vzostupne</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Bitrate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Bookmarks</source>
        <translation>Záložky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Broadcast</source>
        <translation>Vysielanie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>by</source>
        <translation>od</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot find Lame MP3 encoder</source>
        <translation>Nepodarilo sa nájsť Lame MP3 kodér</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>Vyčistiť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear selection</source>
        <translation>Vyčistiť výber</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Zatvoriť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close others</source>
        <translation>Zatvoriť ostatné</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close tab</source>
        <translation>Zatvoriť kartu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment</source>
        <translation>Komentár</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue</source>
        <translation>Pokračovať</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Covers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Created</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Create new directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Create a new library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark Mode</source>
        <translation>Tmavý režim</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Days</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>d</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Default</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <location line="+84"/>
        <source>Delete</source>
        <translation>Odstrániť</translation>
    </message>
    <message>
        <location line="-82"/>
        <source>Descending</source>
        <translation>Zostupne</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory</source>
        <translation>Priečinok</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directories</source>
        <translation>Priečinky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disc</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Duration</source>
        <translation>Trvanie</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dur.</source>
        <translation>Trv.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dynamic playback</source>
        <translation>Dynamické prehrávanie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit</source>
        <translation>Upraviť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Empty input</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Please enter new name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter URL</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Entries</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fast</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Filename</source>
        <translation>Názov súboru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Files</source>
        <translation>Súbory</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filesize</source>
        <translation>Veľkosť súboru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File type</source>
        <translation>Typ súboru</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>1st</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Font</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Fulltext</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Gapless playback</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Genre</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Genres</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Hide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Hours</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>h</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Import directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Import files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading</source>
        <translation>Načítavam</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid characters</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+f</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Backspace</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library</source>
        <translation>Knižnica</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Library view type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Listen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Live Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Logger</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Lyrics</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Menu</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Minutes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>m</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Modified</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Months</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute</source>
        <translation>Stlmiť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute off</source>
        <translation>Vypnúť stlmenie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Názov</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation>Nový</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Next track</source>
        <translation>Ďalšia skladba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation>Nie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No albums</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <location line="+144"/>
        <source>Tracks</source>
        <translation>Skladby</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Move down</source>
        <translation>Presunúť nadol</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Move up</source>
        <translation>Presunúť nahor</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>on</source>
        <translation>na</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Otvoriť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open directory</source>
        <translation>Otvoriť adresár</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open file</source>
        <translation>Otvoriť súbor</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>or</source>
        <translation>alebo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Overwrite</source>
        <translation>Prepísať</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pause</source>
        <translation>Pauza</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play</source>
        <translation>Prehrať</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playing time</source>
        <translation>Čas prehrávania</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play in new tab</source>
        <translation>Prehrať na novej karte</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlists</source>
        <translation>Playlisty</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play next</source>
        <translation>Prehrať ďalšie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play/Pause</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Podcasts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences</source>
        <translation>Nastavenia</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous page</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous track</source>
        <translation>Predchádzajúca skladba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Purchase Url</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Quit</source>
        <translation>Ukončiť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio Station</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Rating</source>
        <translation>Hodnotenie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Really</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Refresh</source>
        <translation>Obnoviť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload Library</source>
        <translation>Obnoviť knižnicu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove</source>
        <translation>Odstrániť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename</source>
        <translation>Premenovať</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat 1</source>
        <translation>Opakovať 1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat all</source>
        <translation>Opakovať všetky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Replace</source>
        <translation>Nahradiť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Reverse order</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Sampler</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Shuffle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Shutdown</source>
        <translation>Vypnúť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Uložiť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save as</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Save to file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan for audio files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Search next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Search previous</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>2nd</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Seconds</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek backward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek forward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Show</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Album Artists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Covers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Library</source>
        <translation>Zobraziť knižnicu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Similar artists</source>
        <translation>Podobní autori</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by</source>
        <translation>Zoradiť podľa</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stop</source>
        <translation>Zastaviť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streams</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream URL</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Success</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>th</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>3rd</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Track</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Track number</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>track on</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Tree</source>
        <translation>Strom</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Undo</source>
        <translation>Späť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown album</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown artist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown genre</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown placeholder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown year</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Various</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Various albums</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Various artists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Various tracks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Version</source>
        <translation>Verzia</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume down</source>
        <translation>Znížiť hlasitosť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume up</source>
        <translation>Zvýšiť hlasitosť</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation>Upozornenie</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Weeks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Year</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Years</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>Priblíženie</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>No directories</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n directory(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No files</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n file(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No playlists</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n playlist(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks</source>
        <translation>Žiadne skladby</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks found</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s) found</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
</TS>