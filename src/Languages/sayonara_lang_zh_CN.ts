<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_CN" sourcelanguage="en" version="2.1">
<context>
    <name>GUI_AlternativeCovers</name>
    <message>
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.ui" line="+27"/>
        <source>Online Search</source>
        <translation>全网搜索</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Automatic search</source>
        <translation>自动搜索</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Text search</source>
        <translation>文本搜索</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Server</source>
        <translation>服务器</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Local Search</source>
        <translation>本地搜索</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Select and preview multiple covers</source>
        <translation>选择并预览多个封面</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Alternative Cover</source>
        <translation>其他封面</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Start search automatically</source>
        <translation>自动开始搜索</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.cpp" line="+283"/>
        <source>%n cover(s) found</source>
        <translation><numerusform>找到了 %n 个封面</numerusform></translation>
    </message>
    <message>
        <location line="+133"/>
        <location line="+31"/>
        <source>Also save cover to %1</source>
        <translation>同时保存封面到 %1</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Cover web search is not enabled</source>
        <translation>未启用封面在线搜索</translation>
    </message>
</context>
<context>
    <name>GUI_History</name>
    <message>
        <location filename="../src/Gui/History/GUI_History.ui" line="+14"/>
        <source>Dialog</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>Newer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Older</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.ui" line="+20"/>
        <source>Info / Edit</source>
        <translation>信息 / 编辑</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Loading files...</source>
        <translation>正在读取文件...</translation>
    </message>
</context>
<context>
    <name>GUI_Lyrics</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.ui" line="+28"/>
        <source>Save Lyrics</source>
        <translation>保存歌词</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>switch</source>
        <translation>切换</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Zoom</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.cpp" line="+303"/>
        <source>Save lyrics not supported</source>
        <translation>不支持保存歌词</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Overwrite lyrics</source>
        <translation>覆盖歌词</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save lyrics</source>
        <translation>保存歌词</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source</source>
        <translation>来源</translation>
    </message>
</context>
<context>
    <name>GUI_ImportDialog</name>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.ui" line="+14"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Import tracks to library</source>
        <translation>导入到媒体库</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select target folder</source>
        <translation>选择目标文件夹</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.cpp" line="+126"/>
        <source>Loading tracks</source>
        <translation>正在载入歌曲</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No tracks</source>
        <translation>无歌曲</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Importing</source>
        <translation>正在导入</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>已完成</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Rollback</source>
        <translation>回滚</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cancelled</source>
        <translation>已取消</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Choose target directory</source>
        <translation>选择目标目录</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>%1&lt;br /&gt;is no library directory</source>
        <translation>%1&lt;br /&gt;不是一个媒体库路径</translation>
    </message>
</context>
<context>
    <name>GUI_LocalLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.ui" line="+17"/>
        <source>Library</source>
        <translation>媒体库</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.cpp" line="+352"/>
        <source>Audio files</source>
        <translation>音频文件</translation>
    </message>
</context>
<context>
    <name>GUI_Controls</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Controls.ui" line="+318"/>
        <source>Written by Michael Lugmair (Lucio Carreras)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Player</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Player.ui" line="+14"/>
        <source>Sayonara Player</source>
        <translation>Sayonara 播放器</translation>
    </message>
</context>
<context>
    <name>GUI_AudioConverter</name>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.ui" line="+45"/>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="+277"/>
        <source>Start</source>
        <translation>播放</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>#Threads</source>
        <translation>#线程</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+187"/>
        <source>Quality</source>
        <translation>质量</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+52"/>
        <location line="+89"/>
        <source>Bitrate</source>
        <translation>比特率</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="-164"/>
        <source>Audio Converter</source>
        <translation>音频转换器</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Threads</source>
        <translation>线程</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+123"/>
        <source>Cannot find encoder</source>
        <translation>找不到编码器</translation>
    </message>
    <message>
        <location line="-113"/>
        <location line="+14"/>
        <source>Playlist does not contain tracks which are supported by the converter</source>
        <translation>播放列表中没有转换器支持的曲目</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>No track will be converted.</source>
        <translation>没有曲目会被转换。</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>These tracks will be ignored</source>
        <translation>这些曲目会被忽略</translation>
    </message>
    <message numerus="yes">
        <location line="+38"/>
        <source>Failed to convert %n track(s)</source>
        <translation><numerusform>%n 首曲目转换失败</numerusform></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please check the log files</source>
        <translation>请检查日志文件</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>All tracks could be converted</source>
        <translation>所有曲目都能被转换</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully finished</source>
        <translation>成功完成</translation>
    </message>
</context>
<context>
    <name>GUI_Bookmarks</name>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.ui" line="+116"/>
        <source>Loop</source>
        <translation>循环</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.cpp" line="+77"/>
        <location line="+53"/>
        <source>No bookmarks found</source>
        <translation>未发现书签</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Sorry, bookmarks can only be set for library tracks at the moment.</source>
        <translation>抱歉，目前仅能对媒体库中的曲目加书签。</translation>
    </message>
</context>
<context>
    <name>GUI_Broadcast</name>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.ui" line="+88"/>
        <source>Cannot Broadcast</source>
        <translation>无法广播</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.cpp" line="+104"/>
        <source>Dismiss</source>
        <translation>放弃</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dismiss all</source>
        <translation>放弃所有</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%n listener(s)</source>
        <translation><numerusform>%n 名听众</numerusform></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot broadcast on port %1</source>
        <translation>无法在 %1 端口广播</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maybe another application is using this port?</source>
        <translation>也许另一个程序正在使用此端口?</translation>
    </message>
</context>
<context>
    <name>GUI_Speed</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.ui" line="+48"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+134"/>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Preserve pitch</source>
        <translation>保持音高</translation>
    </message>
    <message>
        <location line="+30"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+1"/>
        <source>Pitch</source>
        <translation>音高</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 与 %2</translation>
    </message>
    <message>
        <location line="+70"/>
        <location line="+1"/>
        <source>%1 not found</source>
        <translation>未找到 %1</translation>
    </message>
</context>
<context>
    <name>GUI_Style</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Style.ui" line="+14"/>
        <source>Style</source>
        <translation>风格</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Spectrum</source>
        <translation>频谱</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Vert. spacing</source>
        <translation>垂直间距</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+88"/>
        <source>Rect height</source>
        <translation>矩形高度</translation>
    </message>
    <message>
        <location line="-68"/>
        <location line="+44"/>
        <source>Hor. spacing</source>
        <translation>水平间距</translation>
    </message>
    <message>
        <location line="-30"/>
        <location line="+75"/>
        <source>Fading steps</source>
        <translation>渐隐步数</translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Level</source>
        <translation>声压</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Rect width</source>
        <translation>矩形宽度</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ver. spacing</source>
        <translation>垂直间距</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Style settings</source>
        <translation>样式设置</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Color 2</source>
        <translation>色彩2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 1</source>
        <translation>色彩1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color 3</source>
        <translation>色彩3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 4</source>
        <translation>色彩4</translation>
    </message>
</context>
<context>
    <name>GUI_TargetPlaylistDialog</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_TargetPlaylistDialog.ui" line="+14"/>
        <source>Choose target playlist</source>
        <translation>选择目标播放列表</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>relative filepaths</source>
        <translation>相对文件路径</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save playlist as...</source>
        <translation>播放列表另存为...</translation>
    </message>
</context>
<context>
    <name>GUI_StationSearcher</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.ui" line="+14"/>
        <source>Search Radio Station</source>
        <translation>搜索广播电台</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.cpp" line="+58"/>
        <source>Show radio stations from %1 to %2</source>
        <translation>显示从 %1 到 %2 的广播电台</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Country</source>
        <translation>国家</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
</context>
<context>
    <name>GUI_BroadcastPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.ui" line="+20"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ask for permission</source>
        <translation>请求许可</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.cpp" line="+161"/>
        <source>Port %1 already in use</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_CoverPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Covers/GUI_CoverPreferences.ui" line="+62"/>
        <source>Inactive</source>
        <translation>未启用</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>启用</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Delete all covers from the database</source>
        <translation>从媒体库删除所有封面</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear cache</source>
        <translation>清空缓存</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete all covers from the Sayonara directory</source>
        <translation>从Sayonara目录删除所有封面</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete files</source>
        <translation>删除文件</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Save found covers into the library directory where the audio files are located</source>
        <translation>保存找到的封面到音频所在的媒体库目录</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers into library</source>
        <translation>保存找到的封面到媒体库</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fetch missing covers from the internet</source>
        <translation>从互联网获取缺失封面</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Saving covers to the database leads to significantly faster access but results in a bigger database</source>
        <translation>保存封面到数据库将显著加速访问，但会使数据库更大</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers to database</source>
        <translation>保存找到的封面到数据库</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save found covers into Sayonara directory</source>
        <translation>保存找到的封面到 Sayonara 目录</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cover name</source>
        <translation>封面名称</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name of cover file</source>
        <translation>封面文件的名称</translation>
    </message>
</context>
<context>
    <name>GUI_LanguagePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.ui" line="+29"/>
        <source>Check for update</source>
        <translation>检测更新</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Download update</source>
        <translation>下载更新</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>English</source>
        <translation>英文</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>For new languages I am always looking for translators</source>
        <translation>我们一直在寻找新的语言翻译</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.cpp" line="+63"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Cannot check for language update</source>
        <translation>无法检测语言更新</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Language update available</source>
        <translation>有更新可用</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Language is up to date</source>
        <translation>语言数据已是最新</translation>
    </message>
    <message>
        <location line="+41"/>
        <location line="+24"/>
        <source>Cannot fetch language update</source>
        <translation>无法获取语言更新</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Language was updated successfully</source>
        <translation>成功更新语言</translation>
    </message>
</context>
<context>
    <name>GUI_LastFmPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.ui" line="+27"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Scrobble time</source>
        <translation>scrobble 时间</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Login now</source>
        <translation>现在登陆</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.cpp" line="+163"/>
        <source>Logged in</source>
        <translation>已登录</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not logged in</source>
        <translation>未登录</translation>
    </message>
</context>
<context>
    <name>GUI_LibraryPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.ui" line="+27"/>
        <source>Libraries</source>
        <translation>媒体库</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Library-Playlist Interaction</source>
        <translation>媒体库-播放列表互动</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>When drag and drop into playlist </source>
        <translation>当拖放到播放列表中时</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+36"/>
        <source>do nothing (default)</source>
        <translation>不做任何操作（默认）</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>start if stopped and playlist is empty</source>
        <translation>如果未在播放且播放列表为空则播放</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>On double click, create playlist and</source>
        <translation>双击创建一个播放列表</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start playback if stopped</source>
        <translation>如果未在播放则开始播放</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>start playback immediately</source>
        <translation>立即开始播放</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(this is ignored when playlist is in &apos;append mode&apos;)</source>
        <translation>（当播放列表处于“附加模式”时无效）</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show &quot;Clear selection&quot; buttons</source>
        <translation>显示“取消选中”按钮</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Ignore English article &quot;The&quot; in artist name</source>
        <translation>忽略英文专辑名开头的&quot;The&quot;</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.cpp" line="+147"/>
        <source>Cannot edit library</source>
        <translation>无法编辑媒体库</translation>
    </message>
</context>
<context>
    <name>GUI_NotificationPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.ui" line="+17"/>
        <source>Timeout (ms)</source>
        <translation>超时 (ms)</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.cpp" line="+98"/>
        <source>Notifications</source>
        <translation>通知</translation>
    </message>
</context>
<context>
    <name>GUI_PlayerPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.ui" line="+44"/>
        <source>Show system tray icon</source>
        <translation>显示系统托盘图标</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Hide instead of close</source>
        <translation>最小化而非关闭</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Start hidden</source>
        <translation>以最小化启动</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update notifications</source>
        <translation>更新通知</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.cpp" line="+107"/>
        <source>This might cause Sayonara not to show up again.</source>
        <translation>这可能让 Sayonara 无法再次显示。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>In this case use the &apos;--show&apos; option at the next startup.</source>
        <translation>此时，在下次启动时加上&quot;--show&quot;参数。</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.ui" line="+36"/>
        <source>Behavior</source>
        <translation>行为</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start up</source>
        <translation>启动</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load temporary playlists</source>
        <translation>加载临时播放列表</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load saved playlists</source>
        <translation>加载保存的播放列表</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start playing</source>
        <translation>开始播放</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load last track on startup</source>
        <translation>启动时加载最后一首曲目</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remember time of last track</source>
        <translation>记录曲目播放进度</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stop behaviour</source>
        <translation>停止时的行为</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load last track after pressing stop</source>
        <translation>按下停止后加载最后一首曲目</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Look</source>
        <translation>外观</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show numbers</source>
        <translation>显示播放次数</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>&apos;italic text&apos;</source>
        <translation>&apos;斜体文本&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Example</source>
        <translation>示例</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>*bold text*</source>
        <translation>*粗体文本*</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Show clear button</source>
        <translation>显示清除按钮</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Show covers</source>
        <translation>显示封面</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show rating</source>
        <translation>显示评级</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.cpp" line="+216"/>
        <source>Playlist look: Invalid expression</source>
        <translation>播放列表外观：无效的表达式</translation>
    </message>
</context>
<context>
    <name>GUI_PreferenceDialog</name>
    <message>
        <location filename="../src/Gui/Preferences/PreferenceDialog/GUI_PreferenceDialog.ui" line="+136"/>
        <source>Preferences</source>
        <translation>偏好</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>GUI_ProxyPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.ui" line="+20"/>
        <source>Host</source>
        <translation>主机</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Username</source>
        <translation>用户名称</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Save username/password</source>
        <translation>保存用户名和密码</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Active</source>
        <translation>启用</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Automatic search</source>
        <translation>自动搜索</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.cpp" line="+57"/>
        <source>Proxy</source>
        <translation>代理服务器</translation>
    </message>
</context>
<context>
    <name>GUI_RemoteControlPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.ui" line="+36"/>
        <source>Detectable via UDP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Remote control URL</source>
        <translation>远程控制URL</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>UDP port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.cpp" line="+59"/>
        <source>If activated, Sayonara will answer an UDP request that it is remote controlable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+36"/>
        <source>Remote control</source>
        <translation>远程控制</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Port %1 already in use</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_SearchPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Search/GUI_SearchPreferences.ui" line="+23"/>
        <source>Example</source>
        <translation>示例</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Case insensitive</source>
        <translation>不区分大小写</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Option</source>
        <translation>选项</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Ignore accents</source>
        <translation>忽略重音</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Ignore special characters</source>
        <translation>忽略特殊字符</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.ui" line="+44"/>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="+149"/>
        <source>Press shortcut</source>
        <translation>按下快捷键</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="-48"/>
        <source>Shortcuts</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Double shortcuts found</source>
        <translation>发现重复快捷键</translation>
    </message>
</context>
<context>
    <name>GUI_StreamRecorderPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.ui" line="+30"/>
        <source>General</source>
        <translation>通用</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Target directory</source>
        <translation>目标目录</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Automatic recording</source>
        <translation>自动录制</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Create session directory</source>
        <translation>创建会话目录</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Directory</source>
        <translation>会话目录</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Choose available placeholders</source>
        <translation>选择可用的占位符</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Path template</source>
        <translation>目录模板</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Example</source>
        <translation>示例</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.cpp" line="+172"/>
        <source>Choose target directory</source>
        <translation>选择目标目录</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Target directory is empty</source>
        <translation>目标目录是空的</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Please choose another directory</source>
        <translation>请重新选择目录</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cannot create %1</source>
        <translation>无法创建 %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Template path is not valid</source>
        <translation>目录模板无效</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stream recorder</source>
        <translation>流录音机</translation>
    </message>
</context>
<context>
    <name>GUI_StreamPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.ui" line="+17"/>
        <source> ms</source>
        <translation>毫秒</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Buffer size</source>
        <translation>缓冲区大小</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show history</source>
        <translation>显示历史</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Streams in new tab</source>
        <translation>在新标签打开流</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.cpp" line="+57"/>
        <source>%1 and %2</source>
        <translation>%1 与 %2</translation>
    </message>
</context>
<context>
    <name>GUI_CssEditor</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_CssEditor.ui" line="+97"/>
        <source>Edit style sheet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Dark mode</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_FontPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.ui" line="+84"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Font size</source>
        <translation>字体大小</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bold</source>
        <translation>粗体</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Font name</source>
        <translation>字体名称</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.cpp" line="+129"/>
        <location line="+1"/>
        <source>Inherit</source>
        <translation>来自系统</translation>
    </message>
</context>
<context>
    <name>GUI_IconPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.ui" line="+20"/>
        <source>Also apply this icon theme to the dark style</source>
        <translation>也对暗色模式应用图标主题</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.cpp" line="+96"/>
        <location line="+104"/>
        <source>System theme</source>
        <translation>系统主题</translation>
    </message>
    <message>
        <location line="-96"/>
        <source>Icons</source>
        <translation>图标</translation>
    </message>
</context>
<context>
    <name>GUI_UiPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.ui" line="+57"/>
        <source>Fading cover</source>
        <translation>封面渐隐</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit style sheet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.cpp" line="+48"/>
        <source>User Interface</source>
        <translation>用户界面</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>General</source>
        <translation>通用</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show large cover</source>
        <translation>显示大封面</translation>
    </message>
</context>
<context>
    <name>GUI_Shutdown</name>
    <message>
        <location filename="../src/Gui/Shutdown/GUI_Shutdown.ui" line="+14"/>
        <location line="+69"/>
        <source>Shutdown</source>
        <translation>关机</translation>
    </message>
    <message>
        <location line="-56"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>minutes</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shutdown after playlist finished</source>
        <translation>播放列表结束后关闭</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Shutdown after</source>
        <translation>在这之后关闭：</translation>
    </message>
</context>
<context>
    <name>GUI_SomaFM</name>
    <message>
        <location filename="../src/Gui/SomaFM/GUI_SomaFM.ui" line="+42"/>
        <source>Donate to Soma.fm</source>
        <translation>捐赠给Soma.fm</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Streams</source>
        <translation>流</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.ui" line="+14"/>
        <source>Search Soundcloud</source>
        <translation>搜索 Soundcloud</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Search artist</source>
        <translation>搜索艺术家</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudLibrary</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.ui" line="+26"/>
        <source>Library</source>
        <translation>媒体库</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Search for title, interprets and albums</source>
        <translation>搜索歌名，专辑名和介绍</translation>
    </message>
</context>
<context>
    <name>GUI_CoverEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.ui" line="+57"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="+223"/>
        <source>Original</source>
        <translation>原版的</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="-56"/>
        <location line="+9"/>
        <source>File has no cover</source>
        <translation>文件不含封面</translation>
    </message>
</context>
<context>
    <name>GUI_FailMessageBox</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.ui" line="+22"/>
        <source>Details</source>
        <translation>详细</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.cpp" line="+55"/>
        <source>File exists</source>
        <translation>文件存在</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Writeable</source>
        <translation>可写</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Some files could not be saved</source>
        <translation>无法保存一些文件</translation>
    </message>
</context>
<context>
    <name>GUI_TagEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.ui" line="+115"/>
        <source>Discnumber</source>
        <translation>光盘编号</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Album artist</source>
        <translation>专辑艺术家</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Comment</source>
        <translation>备注</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>all</source>
        <translation>全部</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tag from path</source>
        <translation>从路径获取标签</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Undo all</source>
        <translation>撤销全部</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Read only file</source>
        <translation>只读文件</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.cpp" line="+161"/>
        <source>Load complete album</source>
        <translation>读取完整专辑</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Metadata</source>
        <translation>元数据</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tags from path</source>
        <translation>从路径获取标签</translation>
    </message>
    <message numerus="yes">
        <location line="+81"/>
        <source>Cannot apply expression to %n track(s)</source>
        <translation><numerusform>无法对 %n 首曲目应用表达式</numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore these tracks?</source>
        <translation>忽略这些曲目？</translation>
    </message>
    <message>
        <location line="+394"/>
        <source>All changes will be lost</source>
        <translation>你的更改将被丢弃</translation>
    </message>
</context>
<context>
    <name>GUI_TagFromPath</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.ui" line="+17"/>
        <source>Expression</source>
        <translation>表达式</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Disc Nr</source>
        <translation>盘号</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Apply to all</source>
        <translation>应用到全部</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.cpp" line="+92"/>
        <source>Tag</source>
        <translation>标签</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Please select text first</source>
        <translation>请先选择文本</translation>
    </message>
</context>
<context>
    <name>StreamServer</name>
    <message>
        <location filename="../src/Components/Broadcasting/StreamServer.cpp" line="+228"/>
        <source>%1 wants to listen to your music.</source>
        <translation>%1 想聆听您的音乐。</translation>
    </message>
</context>
<context>
    <name>AbstractLibrary</name>
    <message>
        <location filename="../src/Components/Library/AbstractLibrary.cpp" line="+849"/>
        <source>All %1 could be removed</source>
        <translation>全部 %1 都能被删除</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 %3 could not be removed</source>
        <translation>%1 个（共%2个) %3 不能被删除</translation>
    </message>
</context>
<context>
    <name>Library::Importer</name>
    <message>
        <location filename="../src/Components/Library/Importer/LibraryImporter.cpp" line="+228"/>
        <source>Cannot import tracks</source>
        <translation>无法导入曲目</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>All files could be imported</source>
        <translation>所有文件可被导入</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 files could be imported</source>
        <translation>%1  个（共 %2 个）文件可被导入</translation>
    </message>
</context>
<context>
    <name>Library::ReloadThread</name>
    <message>
        <location filename="../src/Components/Library/Threads/ReloadThread.cpp" line="+219"/>
        <source>Looking for covers</source>
        <translation>正在搜索封面</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Reading files</source>
        <translation>正在读取文件</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Deleting orphaned tracks</source>
        <translation>正在删除孤立的曲目</translation>
    </message>
</context>
<context>
    <name>Lyrics::LookupThread</name>
    <message>
        <location filename="../src/Components/Lyrics/LyricLookup.cpp" line="+171"/>
        <location line="+35"/>
        <source>Cannot fetch lyrics from %1</source>
        <translation>无法从 %1 获得歌词</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No lyrics found</source>
        <translation>未找到歌词</translation>
    </message>
</context>
<context>
    <name>Shutdown</name>
    <message>
        <location filename="../src/Components/Shutdown/Shutdown.cpp" line="+100"/>
        <source>Computer will shutdown after playlist has finished</source>
        <translation>将在播放列表结束后关机</translation>
    </message>
    <message numerus="yes">
        <location line="+29"/>
        <location line="+35"/>
        <source>Computer will shutdown in %n minute(s)</source>
        <translation><numerusform>将在 %n 分钟后关机</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::JsonParser</name>
    <message>
        <location filename="../src/Components/Streaming/Soundcloud/SoundcloudJsonParser.cpp" line="+123"/>
        <source>Website</source>
        <translation>网站</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+236"/>
        <source>Permalink Url</source>
        <translation>永久链接</translation>
    </message>
    <message>
        <location line="-223"/>
        <source>Followers/Following</source>
        <translation>粉丝/关注</translation>
    </message>
    <message>
        <location line="+93"/>
        <location line="+134"/>
        <source>Purchase Url</source>
        <translation>购买链接</translation>
    </message>
</context>
<context>
    <name>Gui::CoverButton</name>
    <message>
        <location filename="../src/Gui/Covers/CoverButton.cpp" line="+119"/>
        <source>Search an alternative cover</source>
        <translation>搜索替代封面</translation>
    </message>
</context>
<context>
    <name>HistoryContainer</name>
    <message>
        <location filename="../src/Gui/History/HistoryContainer.cpp" line="+25"/>
        <source>History</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.cpp" line="+92"/>
        <source>Write cover to tracks</source>
        <translation>将封面写入曲目</translation>
    </message>
</context>
<context>
    <name>Library::CoverViewContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/CoverViewContextMenu.cpp" line="+217"/>
        <source>Toolbar</source>
        <translation>工具栏</translation>
    </message>
</context>
<context>
    <name>Library::GUI_CoverView</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/GUI_CoverView.cpp" line="+214"/>
        <source>Use Ctrl + mouse wheel to zoom</source>
        <translation>用 Ctrl + 鼠标滚轮缩放</translation>
    </message>
</context>
<context>
    <name>Directory::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryContextMenu.cpp" line="+295"/>
        <source>Rename by metadata</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Collapse all</source>
        <translation>全部折叠</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to another library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to another library</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Directory::TreeView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryTreeView.cpp" line="+410"/>
        <source>Copy here</source>
        <translation>拷贝到此处</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move here</source>
        <translation>移动到此处</translation>
    </message>
</context>
<context>
    <name>GUI_DirectoryView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/GUI_DirectoryView.cpp" line="+183"/>
        <source>Could not create directory</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::GenreView</name>
    <message>
        <location filename="../src/Gui/Library/GenreView.cpp" line="+114"/>
        <source>Updating genres</source>
        <translation>正在更新流派</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Do you really want to remove %1 from all tracks?</source>
        <translation>你确定要从所有曲目中移除 %1 吗？</translation>
    </message>
</context>
<context>
    <name>Library::GUI_EmptyLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_EmptyLibrary.cpp" line="+94"/>
        <source>Please choose a name for your library</source>
        <translation>请为媒体库起一个名称</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Please choose another name for your library</source>
        <translation>请重新为媒体库起一个名称</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>The file path is invalid</source>
        <translation>路径无效</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library with the same file path already exists</source>
        <translation>相同路径已有媒体库</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library which contains this file path already exists</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::HeaderView</name>
    <message>
        <location filename="../src/Gui/Library/Header/HeaderView.cpp" line="+198"/>
        <source>Resize columns</source>
        <translation>调整列宽</translation>
    </message>
</context>
<context>
    <name>Library::TrackModel</name>
    <message>
        <location filename="../src/Gui/Library/TableView/TrackModel.cpp" line="+144"/>
        <source>kBit/s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_DeleteDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_DeleteDialog.cpp" line="+87"/>
        <source>Only from library</source>
        <translation>仅自媒体库</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>You are about to delete %n file(s)</source>
        <translation><numerusform>你将删除 %n 个文件</numerusform></translation>
    </message>
</context>
<context>
    <name>Library::GUI_LibraryReloadDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_ReloadLibraryDialog.cpp" line="+78"/>
        <source>Fast scan</source>
        <translation>快速扫描</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deep scan</source>
        <translation>深度扫描</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Only scan for new and deleted files</source>
        <translation>仅扫描新的与被删除的文件</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scan all files in your library directory</source>
        <translation>在你的媒体库目录扫描所有文件</translation>
    </message>
</context>
<context>
    <name>Library::LocalLibraryMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/LocalLibraryMenu.cpp" line="+154"/>
        <source>Statistics</source>
        <translation>统计数据</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit library</source>
        <translation>编辑媒体库</translation>
    </message>
</context>
<context>
    <name>Gui::MergeMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/MergeMenu.cpp" line="+52"/>
        <location line="+53"/>
        <source>Merge</source>
        <translation>合并</translation>
    </message>
</context>
<context>
    <name>GUI_ControlsBase</name>
    <message>
        <location filename="../src/Gui/Player/GUI_ControlsBase.cpp" line="+71"/>
        <source>Sayonara Player</source>
        <translation>Sayonara播放器</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright</source>
        <translation>版权所有</translation>
    </message>
</context>
<context>
    <name>GUI_Logger</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.cpp" line="+275"/>
        <source>Cannot open file</source>
        <translation>无法打开文件</translation>
    </message>
</context>
<context>
    <name>Menubar</name>
    <message>
        <location filename="../src/Gui/Player/GUI_PlayerMenubar.cpp" line="+323"/>
        <source>View</source>
        <translation>视图</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+18"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Show large cover</source>
        <translation>显示大封面</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fullscreen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Media files</source>
        <translation>媒体文件</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>打开媒体文件</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please visit the forum at</source>
        <translation>请访问论坛</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>FAQ</source>
        <translation>FAQ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>About Sayonara</source>
        <translation>关于 Sayonara 播放器</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>License</source>
        <translation>许可证</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Donate</source>
        <translation>捐助</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Special thanks to all the brave translators</source>
        <translation>特别感谢所有译者</translation>
    </message>
</context>
<context>
    <name>TrayIconContextMenu</name>
    <message>
        <location filename="../src/Gui/Player/GUI_TrayIcon.cpp" line="+176"/>
        <source>Current song</source>
        <translation>当前歌曲</translation>
    </message>
</context>
<context>
    <name>VersionChecker</name>
    <message>
        <location filename="../src/Gui/Player/VersionChecker.cpp" line="+77"/>
        <source>A new version is available!</source>
        <translation>有新版本可用！</translation>
    </message>
</context>
<context>
    <name>GUI_Playlist</name>
    <message>
        <location filename="../src/Gui/Playlist/GUI_Playlist.cpp" line="+241"/>
        <source>Playlist empty</source>
        <translation>播放列表为空</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Media files</source>
        <translation>媒体文件</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>打开媒体文件</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Playlist name already exists</source>
        <translation>播放列表名已存在</translation>
    </message>
</context>
<context>
    <name>Playlist::ActionMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistActionMenu.cpp" line="+193"/>
        <source>Please set library path first</source>
        <translation>请先设置媒体库路径</translation>
    </message>
</context>
<context>
    <name>Playlist::BottomBar</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistBottomBar.cpp" line="+242"/>
        <source>Please set library path first</source>
        <translation>请先设置媒体库路径</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel shutdown?</source>
        <translation>取消关机？</translation>
    </message>
</context>
<context>
    <name>Playlist::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistContextMenu.cpp" line="+171"/>
        <source>Jump to current track</source>
        <translation>跳转到当前曲目</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show track in library</source>
        <translation>在媒体库中显示歌曲</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Playlist mode</source>
        <translation>播放列表模式</translation>
    </message>
</context>
<context>
    <name>Playlist::Model</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistModel.cpp" line="+474"/>
        <source>Goto row</source>
        <translation>跳到行</translation>
    </message>
</context>
<context>
    <name>GUI_Crossfader</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Crossfader.cpp" line="+75"/>
        <location line="+12"/>
        <source>Crossfader</source>
        <translation>交叉渐变器</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 与 %2</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Crossfader does not work with Alsa</source>
        <translation>交叉渐变器不支持 Alsa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gapless playback does not work with Alsa</source>
        <translation>无缝播放不支持 Alsa</translation>
    </message>
</context>
<context>
    <name>GUI_Equalizer</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Equalizer.cpp" line="+137"/>
        <location line="+40"/>
        <source>Linked sliders</source>
        <translation>关联滑动条</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Equalizer</source>
        <translation>均衡器</translation>
    </message>
    <message>
        <location line="+248"/>
        <source>Name %1 not allowed</source>
        <translation>名称 %1 不可用</translation>
    </message>
</context>
<context>
    <name>GUI_LevelPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_LevelPainter.cpp" line="+157"/>
        <source>Level</source>
        <translation>声压</translation>
    </message>
</context>
<context>
    <name>GUI_SpectrogramPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_SpectrogramPainter.cpp" line="+77"/>
        <source>Spectrogram</source>
        <translation>声谱</translation>
    </message>
</context>
<context>
    <name>GUI_Spectrum</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Spectrum.cpp" line="+150"/>
        <source>Spectrum</source>
        <translation>频谱</translation>
    </message>
</context>
<context>
    <name>GUI_StyleSettings</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_StyleSettings.cpp" line="+203"/>
        <source>There are some unsaved settings&lt;br /&gt;Save now?</source>
        <translation>有未保存的设置&lt;br /&gt;保存吗？</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Please specify a name</source>
        <translation>请指定一个名称</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Save changes?</source>
        <translation>保存更改?</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistChooser</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_PlaylistChooser.cpp" line="+91"/>
        <location line="+38"/>
        <source>No playlists found</source>
        <translation>未找到播放列表</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Could not rename playlist</source>
        <translation>无法重命名播放列表</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Name is invalid</source>
        <translation>名称无效</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Do you really want to delete %1?</source>
        <translation>你确定要删除 %1 吗？</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not delete playlist %1</source>
        <translation>无法删除播放列表 %1</translation>
    </message>
</context>
<context>
    <name>Gui::AbstractStationPlugin</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/AbstractStationPlugin.cpp" line="+243"/>
        <source>Cannot open stream</source>
        <translation>无法打开流</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please choose another name</source>
        <translation>请重新选择名称</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Do you really want to delete %1</source>
        <translation>你确定要删除 %1 吗</translation>
    </message>
</context>
<context>
    <name>GUI_Podcasts</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Podcasts.cpp" line="+65"/>
        <source>Podcast</source>
        <translation>播客</translation>
    </message>
</context>
<context>
    <name>GUI_Stream</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Stream.cpp" line="+68"/>
        <source>Search radio station</source>
        <translation>搜索广播电台</translation>
    </message>
</context>
<context>
    <name>GUI_EnginePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Engine/GUI_EnginePreferences.cpp" line="+53"/>
        <source>Audio</source>
        <translation>音频</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutEntry</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutEntry.cpp" line="+55"/>
        <source>Enter shortcut</source>
        <translation>输入快捷键</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Shortcut already in use</source>
        <translation>快捷键已被使用</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Test</source>
        <translation>测试</translation>
    </message>
</context>
<context>
    <name>SomaFM::StationModel</name>
    <message>
        <location filename="../src/Gui/SomaFM/SomaFMStationModel.cpp" line="+126"/>
        <source>Cannot fetch stations</source>
        <translation>无法获取电台</translation>
    </message>
</context>
<context>
    <name>SC::GUI_ArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.cpp" line="+79"/>
        <source>Query too short</source>
        <translation>搜索内容过短</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>No artists found</source>
        <translation>为找到艺术家</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>Found %n artist(s)</source>
        <translation><numerusform>找到 %n 名艺术家</numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+56"/>
        <source>%n playlist(s) found</source>
        <translation><numerusform>找到 %n 个播放列表</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::GUI_Library</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.cpp" line="+56"/>
        <source>Add artist</source>
        <translation>添加艺术家</translation>
    </message>
</context>
<context>
    <name>TagTextInput</name>
    <message>
        <location filename="../src/Gui/Tagging/TagTextInput.cpp" line="+126"/>
        <source>Very first letter to upper case</source>
        <translation>开头首字母大写</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First letters to upper case</source>
        <translation>首字母大写</translation>
    </message>
</context>
<context>
    <name>Library::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Utils/ContextMenu/LibraryContextMenu.cpp" line="+196"/>
        <source>Play in new tab</source>
        <translation>在新标签播放</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Standard view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cover view</source>
        <translation>封面视图</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbar</source>
        <translation>工具栏</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>The toolbar is visible when there are tracks with differing file types listed in the track view</source>
        <translation>当曲目列表中有不同文件类型的曲目时将显示此工具栏</translation>
    </message>
</context>
<context>
    <name>Gui::ImageSelectionDialog</name>
    <message>
        <location filename="../src/Gui/Utils/ImageSelectionDialog.cpp" line="+57"/>
        <source>Image files</source>
        <translation>图片文件</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Any files</source>
        <translation>任何文件</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open image files</source>
        <translation>打开图像文件</translation>
    </message>
</context>
<context>
    <name>Gui::StreamRecorderPreferenceAction</name>
    <message>
        <location filename="../src/Gui/Utils/PreferenceAction.cpp" line="+163"/>
        <location line="+7"/>
        <source>Stream Recorder</source>
        <translation>流录音机</translation>
    </message>
</context>
<context>
    <name>Gui::ShortcutPreferenceAction</name>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Shortcuts</source>
        <translation>快捷键</translation>
    </message>
</context>
<context>
    <name>Gui::MiniSearcher</name>
    <message>
        <location filename="../src/Gui/Utils/SearchableWidget/MiniSearcher.cpp" line="+92"/>
        <source>Arrow up</source>
        <translation>上箭头</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Previous search result</source>
        <translation>上次的搜索结果</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Arrow down</source>
        <translation>下箭头</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Next search result</source>
        <translation>下个搜索结果</translation>
    </message>
</context>
<context>
    <name>Lang</name>
    <message>
        <location filename="../src/Utils/Language/Language.cpp" line="+79"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>动作</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Actions</source>
        <translation>动作</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Activate</source>
        <translation>激活</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>启用</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add tab</source>
        <translation>添加标签</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album</source>
        <translation>专辑</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artists</source>
        <translation>专辑艺术家</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Albums</source>
        <translation>专辑</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Append</source>
        <translation>扩展</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Application</source>
        <translation>应用程序</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>应用</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artist</source>
        <translation>艺术家</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artists</source>
        <translation>艺术家</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ascending</source>
        <translation>升序</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic</source>
        <translation>自动</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bitrate</source>
        <translation>比特率</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bookmarks</source>
        <translation>书签</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Broadcast</source>
        <translation>广播</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>by</source>
        <translation>通过</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot find Lame MP3 encoder</source>
        <translation>找不到 Lame MP3 编码器</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>清除</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear selection</source>
        <translation>清除选中</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close others</source>
        <translation>关闭其他</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close tab</source>
        <translation>关闭标签</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment</source>
        <translation>备注</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue</source>
        <translation>继续</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Covers</source>
        <translation>封面</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created</source>
        <translation>已创建</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create new directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Create a new library</source>
        <translation>创建新的媒体库</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark Mode</source>
        <translation>暗色模式</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Days</source>
        <translation>天</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>d</source>
        <translation>d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+84"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location line="-82"/>
        <source>Descending</source>
        <translation>降序</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory</source>
        <translation>目录</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directories</source>
        <translation>目录</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disc</source>
        <translation>盘号</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Duration</source>
        <translation>时长</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dur.</source>
        <translation>时长</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dynamic playback</source>
        <translation>动态播放</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Empty input</source>
        <translation>清空输入</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter name</source>
        <translation>输入名称</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please enter new name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter URL</source>
        <translation>输入 URL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry</source>
        <translation>菜单项</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entries</source>
        <translation>菜单项</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fast</source>
        <translation>快速</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filename</source>
        <translation>文件名</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Files</source>
        <translation>文件</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filesize</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File type</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filter</source>
        <translation>筛选</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>1st</source>
        <translation>第1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Font</source>
        <translation>字体</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation>字体</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fulltext</source>
        <translation>全文</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Gapless playback</source>
        <translation>无缝播放</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genre</source>
        <translation>流派</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genres</source>
        <translation>流派</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hide</source>
        <translation>隐藏</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hours</source>
        <translation>小时</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import directory</source>
        <translation>导入目录</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import files</source>
        <translation>导入文件</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation>未启用</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading</source>
        <translation>正在载入</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading %1</source>
        <translation>正在载入 %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid characters</source>
        <translation>无效字符</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+f</source>
        <translation>Ctrl+f</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library</source>
        <translation>媒体库</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library path</source>
        <translation>媒体库路径</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library view type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Listen</source>
        <translation>倾听</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Live Search</source>
        <translation>实时搜索</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Logger</source>
        <translation>日志记录器</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lyrics</source>
        <translation>歌词</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minutes</source>
        <translation>分</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing</source>
        <translation>丢失</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Modified</source>
        <translation>被更改</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Months</source>
        <translation>月</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute</source>
        <translation>静音</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute off</source>
        <translation>取消静音</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next page</source>
        <translation>下一页</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next track</source>
        <translation>下一曲目</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No albums</source>
        <translation>无专辑</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+144"/>
        <source>Tracks</source>
        <translation>曲目</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Move down</source>
        <translation>下移</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Move up</source>
        <translation>上移</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation>好</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>on</source>
        <translation>于</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open directory</source>
        <translation>打开目录</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>or</source>
        <translation>或</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Overwrite</source>
        <translation>覆盖</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pause</source>
        <translation>暂停</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playing time</source>
        <translation>播放时间</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play in new tab</source>
        <translation>在新标签播放</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlist</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlists</source>
        <translation>播放列表</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play next</source>
        <translation>播放下一首</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play/Pause</source>
        <translation>播放/暂停</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugin</source>
        <translation>插件</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Podcasts</source>
        <translation>播客</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences</source>
        <translation>首选项</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous page</source>
        <translation>上一页</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous track</source>
        <translation>上一曲目</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Purchase Url</source>
        <translation>购买链接</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio</source>
        <translation>广播</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio Station</source>
        <translation>广播电台</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rating</source>
        <translation>评分</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Really</source>
        <translation>确定吗</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload Library</source>
        <translation>重载媒体库</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat 1</source>
        <translation>单曲循环</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat all</source>
        <translation>列表循环</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Replace</source>
        <translation>替换</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reverse order</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Sampler</source>
        <translation>采样器</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shuffle</source>
        <translation>随机</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shutdown</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save as</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save to file</source>
        <translation>保存到文件</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan for audio files</source>
        <translation>扫描音频文件</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search next</source>
        <translation>下一个</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search previous</source>
        <translation>上一个</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2nd</source>
        <translation>第2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seconds</source>
        <translation>秒</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>s</source>
        <translation> 秒</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek backward</source>
        <translation>向后检索</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek forward</source>
        <translation>向前检索</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show</source>
        <translation>显示</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Album Artists</source>
        <translation>显示专辑艺术家</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Covers</source>
        <translation>显示封面</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Library</source>
        <translation>显示媒体库</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Similar artists</source>
        <translation>相似艺术家</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by</source>
        <translation>排序</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streams</source>
        <translation>流</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream URL</source>
        <translation>流 URL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>th</source>
        <translation>.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3rd</source>
        <translation>第三</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track</source>
        <translation>曲目</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track number</source>
        <translation>曲目编号</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>track on</source>
        <translation>曲目于</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Tree</source>
        <translation>树</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Undo</source>
        <translation>撤消</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown album</source>
        <translation>未知专辑</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown artist</source>
        <translation>未知艺术家</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown title</source>
        <translation>未知标题</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown genre</source>
        <translation>未知流派</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown placeholder</source>
        <translation>未知占位符</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown year</source>
        <translation>未知年份</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Various</source>
        <translation>群星</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various albums</source>
        <translation>多专辑</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various artists</source>
        <translation>多艺术家</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various tracks</source>
        <translation>多曲目</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume down</source>
        <translation>减小音量</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume up</source>
        <translation>增大音量</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Weeks</source>
        <translation>周</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Year</source>
        <translation>年</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Years</source>
        <translation>年</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>No directories</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n directory(s)</source>
        <translation type="unfinished"><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No files</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n file(s)</source>
        <translation type="unfinished"><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No playlists</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n playlist(s)</source>
        <translation type="unfinished"><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks</source>
        <translation>无歌曲</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s)</source>
        <translation type="unfinished"><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks found</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s) found</source>
        <translation type="unfinished"><numerusform></numerusform></translation>
    </message>
</context>
</TS>