<?xml version="1.0" ?><!DOCTYPE TS><TS language="es_ES" sourcelanguage="en" version="2.1">
<context>
    <name>GUI_AlternativeCovers</name>
    <message>
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.ui" line="+27"/>
        <source>Online Search</source>
        <translation>Buscar en línea</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Automatic search</source>
        <translation>Búsqueda automática</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Text search</source>
        <translation>Buscar texto</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Server</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Local Search</source>
        <translation>Búsqueda local</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Select and preview multiple covers</source>
        <translation>Seleccionar y ver múltiples carátulas</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Alternative Cover</source>
        <translation>Carátula alternativa</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Start search automatically</source>
        <translation>Iniciar la búsqueda automáticamente</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.cpp" line="+283"/>
        <source>%n cover(s) found</source>
        <translation><numerusform>%n carátula(s) encontradas</numerusform><numerusform>%n carátula(s) encontradas</numerusform></translation>
    </message>
    <message>
        <location line="+133"/>
        <location line="+31"/>
        <source>Also save cover to %1</source>
        <translation>Guardar también la carátula en %1</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Cover web search is not enabled</source>
        <translation>La búsqueda web de portada no está habilitada</translation>
    </message>
</context>
<context>
    <name>GUI_History</name>
    <message>
        <location filename="../src/Gui/History/GUI_History.ui" line="+14"/>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Newer</source>
        <translation>más nuevo</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Older</source>
        <translation>más antiguo </translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.ui" line="+20"/>
        <source>Info / Edit</source>
        <translation>Info / Editar</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Loading files...</source>
        <translation>Cargando archivos...</translation>
    </message>
</context>
<context>
    <name>GUI_Lyrics</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.ui" line="+28"/>
        <source>Save Lyrics</source>
        <translation>Guardar letras</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>switch</source>
        <translation>Cambiar</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Zoom</source>
        <translation>Ampliación</translation>
    </message>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.cpp" line="+303"/>
        <source>Save lyrics not supported</source>
        <translation>Guardar letras no está soportado</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Overwrite lyrics</source>
        <translation>Sobreescribir letras</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save lyrics</source>
        <translation>Guardar letras</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source</source>
        <translation>Origen</translation>
    </message>
</context>
<context>
    <name>GUI_ImportDialog</name>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.ui" line="+14"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Import tracks to library</source>
        <translation>Importar pistas a la biblioteca</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select target folder</source>
        <translation>Seleccionar destino</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.cpp" line="+126"/>
        <source>Loading tracks</source>
        <translation>Carga pistas</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No tracks</source>
        <translation>Sin pistas</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Importing</source>
        <translation>Importando</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>Finalizado</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Rollback</source>
        <translation>Revertir</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cancelled</source>
        <translation>Cancelado</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Choose target directory</source>
        <translation>Elegir carpeta de destino</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>%1&lt;br /&gt;is no library directory</source>
        <translation>%1&lt;br /&gt; no es una carpeta de biblioteca</translation>
    </message>
</context>
<context>
    <name>GUI_LocalLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.ui" line="+17"/>
        <source>Library</source>
        <translation>Biblioteca</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.cpp" line="+352"/>
        <source>Audio files</source>
        <translation>Archivos de audio</translation>
    </message>
</context>
<context>
    <name>GUI_Controls</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Controls.ui" line="+318"/>
        <source>Written by Michael Lugmair (Lucio Carreras)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Player</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Player.ui" line="+14"/>
        <source>Sayonara Player</source>
        <translation>Reproductor Sayonara</translation>
    </message>
</context>
<context>
    <name>GUI_AudioConverter</name>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.ui" line="+45"/>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="+277"/>
        <source>Start</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Stop</source>
        <translation>Detener</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>#Threads</source>
        <translation>#Hilos</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+187"/>
        <source>Quality</source>
        <translation>Calidad</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+52"/>
        <location line="+89"/>
        <source>Bitrate</source>
        <translation>Tasa de bits</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="-164"/>
        <source>Audio Converter</source>
        <translation>Conversor de audio</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Threads</source>
        <translation>Hilos</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+123"/>
        <source>Cannot find encoder</source>
        <translation>No se puede encontrar el codificador</translation>
    </message>
    <message>
        <location line="-113"/>
        <location line="+14"/>
        <source>Playlist does not contain tracks which are supported by the converter</source>
        <translation>La lista de reproducción no contiene pistas compatibles con el convertidor</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>No track will be converted.</source>
        <translation>No se convertirá ninguna pista.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>These tracks will be ignored</source>
        <translation>Estas pistas serán ignoradas</translation>
    </message>
    <message numerus="yes">
        <location line="+38"/>
        <source>Failed to convert %n track(s)</source>
        <translation><numerusform>Error al convertir %n pista(s)</numerusform><numerusform>Error al convertir %n pista(s)</numerusform></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please check the log files</source>
        <translation>Por favor revise los archivos de registro</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>All tracks could be converted</source>
        <translation>Todas las pistas pueden ser convertidas</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully finished</source>
        <translation>Finalizado con éxito</translation>
    </message>
</context>
<context>
    <name>GUI_Bookmarks</name>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.ui" line="+116"/>
        <source>Loop</source>
        <translation>Bucle</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.cpp" line="+77"/>
        <location line="+53"/>
        <source>No bookmarks found</source>
        <translation>No se encontraron marcadores.</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Sorry, bookmarks can only be set for library tracks at the moment.</source>
        <translation>Disculpa. De momento, sólo pueden crearse marcadores para las pistas de la biblioteca.</translation>
    </message>
</context>
<context>
    <name>GUI_Broadcast</name>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.ui" line="+88"/>
        <source>Cannot Broadcast</source>
        <translation>No se puede emitir</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.cpp" line="+104"/>
        <source>Dismiss</source>
        <translation>Descartar</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dismiss all</source>
        <translation>Descartar todo</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%n listener(s)</source>
        <translation><numerusform>%n oyente(s)</numerusform><numerusform>%n oyente(s)</numerusform></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot broadcast on port %1</source>
        <translation>No se puede emitir por el puerto %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maybe another application is using this port?</source>
        <translation>Otra aplicación puede estar usando ese puerto.</translation>
    </message>
</context>
<context>
    <name>GUI_Speed</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.ui" line="+48"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+134"/>
        <source>Speed</source>
        <translation>Velocidad</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Preserve pitch</source>
        <translation>Preservar tono</translation>
    </message>
    <message>
        <location line="+30"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+1"/>
        <source>Pitch</source>
        <translation>Tono</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 y %2</translation>
    </message>
    <message>
        <location line="+70"/>
        <location line="+1"/>
        <source>%1 not found</source>
        <translation>%1: No encontrado</translation>
    </message>
</context>
<context>
    <name>GUI_Style</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Style.ui" line="+14"/>
        <source>Style</source>
        <translation>Estilo</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Spectrum</source>
        <translation>Espectro</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Vert. spacing</source>
        <translation>Espaciado vertical entre rectángulos</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+88"/>
        <source>Rect height</source>
        <translation>Altura de los rectángulos</translation>
    </message>
    <message>
        <location line="-68"/>
        <location line="+44"/>
        <source>Hor. spacing</source>
        <translation>Espaciado horizontal entre rectángulos</translation>
    </message>
    <message>
        <location line="-30"/>
        <location line="+75"/>
        <source>Fading steps</source>
        <translation>Pasos para desvanecerse</translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Level</source>
        <translation>Nivel</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Rect width</source>
        <translation>Anchura de los rectángulos</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ver. spacing</source>
        <translation>Espaciado vertical</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Style settings</source>
        <translation>Opciones de estilo</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Color 2</source>
        <translation>Color 2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 1</source>
        <translation>Color 1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color 3</source>
        <translation>Color 3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 4</source>
        <translation>Color 4</translation>
    </message>
</context>
<context>
    <name>GUI_TargetPlaylistDialog</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_TargetPlaylistDialog.ui" line="+14"/>
        <source>Choose target playlist</source>
        <translation>Escoger destino para la lista</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>relative filepaths</source>
        <translation>rutas de archivo relativas</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save playlist as...</source>
        <translation>Guardar lista como...</translation>
    </message>
</context>
<context>
    <name>GUI_StationSearcher</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.ui" line="+14"/>
        <source>Search Radio Station</source>
        <translation>Buscar Emisora de Radio</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.cpp" line="+58"/>
        <source>Show radio stations from %1 to %2</source>
        <translation>Mostrar estaciones de radio desde %1 hasta %2</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Country</source>
        <translation>País</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
</context>
<context>
    <name>GUI_BroadcastPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.ui" line="+20"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ask for permission</source>
        <translation>Pedir permiso</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.cpp" line="+161"/>
        <source>Port %1 already in use</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_CoverPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Covers/GUI_CoverPreferences.ui" line="+62"/>
        <source>Inactive</source>
        <translation>Inactivo</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>Activar</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Delete all covers from the database</source>
        <translation>Eliminar todas las carátulas de la base de datos</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear cache</source>
        <translation>Limpiar cache</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete all covers from the Sayonara directory</source>
        <translation>Eliminar todas las carátulas de la carpeta de Sayonara</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete files</source>
        <translation>Eliminar archivos</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Save found covers into the library directory where the audio files are located</source>
        <translation>Guardar las carátulas encontradas en la carpeta de la biblioteca donde se encuentran los archivos de audio</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers into library</source>
        <translation>Guardar en la biblioteca las carátulas encontradas</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fetch missing covers from the internet</source>
        <translation>Buscar en Internet las carátulas faltantes</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Saving covers to the database leads to significantly faster access but results in a bigger database</source>
        <translation>Guardar las carátulas a base de datos conlleva un acceso más rápido pero genera una base de datos más pesada</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers to database</source>
        <translation>Guardar en la base de datos las carátulas encontradas</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save found covers into Sayonara directory</source>
        <translation>Guardar las carátulas encontradas en la carpeta de Sayonara</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cover name</source>
        <translation>Nombre de la carátula</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name of cover file</source>
        <translation>Nombre del archivo de la carátula</translation>
    </message>
</context>
<context>
    <name>GUI_LanguagePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.ui" line="+29"/>
        <source>Check for update</source>
        <translation>Verificar por actualizaciones</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Download update</source>
        <translation>Descargar actualización</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>English</source>
        <translation>Inglés</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>For new languages I am always looking for translators</source>
        <translation>Siempre busco traductores a más idiomas</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.cpp" line="+63"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Cannot check for language update</source>
        <translation>No se puede verificar la actualización del idioma</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Language update available</source>
        <translation>Hay una actualización disponible</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Language is up to date</source>
        <translation>El idioma está actualizado</translation>
    </message>
    <message>
        <location line="+41"/>
        <location line="+24"/>
        <source>Cannot fetch language update</source>
        <translation>No se pudo obtener una actualización del idioma</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Language was updated successfully</source>
        <translation>El idioma fue actualizado con éxito</translation>
    </message>
</context>
<context>
    <name>GUI_LastFmPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.ui" line="+27"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Scrobble time</source>
        <translation>Scrobble time</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Login now</source>
        <translation>Acceder</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.cpp" line="+163"/>
        <source>Logged in</source>
        <translation>Registrado</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not logged in</source>
        <translation>No registrado</translation>
    </message>
</context>
<context>
    <name>GUI_LibraryPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.ui" line="+27"/>
        <source>Libraries</source>
        <translation>Bibliotecas</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Library-Playlist Interaction</source>
        <translation>Interacción Biblioteca-Lista de reproducción</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>When drag and drop into playlist </source>
        <translation>Al arrastrar y soltar en la lista</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+36"/>
        <source>do nothing (default)</source>
        <translation>no hacer nada (predeterminado)</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>start if stopped and playlist is empty</source>
        <translation>reproducir si está parado y la lista vacía</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>On double click, create playlist and</source>
        <translation>Con doble clic, crear lista y</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start playback if stopped</source>
        <translation>reproducir si está parado</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>start playback immediately</source>
        <translation>reproducir inmediatamente</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(this is ignored when playlist is in &apos;append mode&apos;)</source>
        <translation>(esto es ignorado cuando la lista de reproducción esta en &apos;Modo añadir&apos;)</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Other</source>
        <translation>Otra</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show &quot;Clear selection&quot; buttons</source>
        <translation>Mostrar los botones de Limpiar Selección.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Ignore English article &quot;The&quot; in artist name</source>
        <translation>Ignorar el artículo &quot;The&quot; en el nombre del artista</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.cpp" line="+147"/>
        <source>Cannot edit library</source>
        <translation>No se puede editar la biblioteca</translation>
    </message>
</context>
<context>
    <name>GUI_NotificationPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.ui" line="+17"/>
        <source>Timeout (ms)</source>
        <translation>Mostrar durante (ms)</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.cpp" line="+98"/>
        <source>Notifications</source>
        <translation>Notificaciones</translation>
    </message>
</context>
<context>
    <name>GUI_PlayerPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.ui" line="+44"/>
        <source>Show system tray icon</source>
        <translation>Mostrar icono en la bandeja del sistema</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Hide instead of close</source>
        <translation>Ocultar en lugar de cerrar</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Start hidden</source>
        <translation>Inicio oculto</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update notifications</source>
        <translation>Notificar si hay actualizaciones</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.cpp" line="+107"/>
        <source>This might cause Sayonara not to show up again.</source>
        <translation>Esto puede causar que Sayonara no vuelva a mostrarse de nuevo.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>In this case use the &apos;--show&apos; option at the next startup.</source>
        <translation>En dicho caso use la opción &quot;--mostrar&quot; en el siguiente reinicio.</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.ui" line="+36"/>
        <source>Behavior</source>
        <translation>Comportamiento</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start up</source>
        <translation>Al iniciar</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load temporary playlists</source>
        <translation>Cargar listas provisionales (no guardadas)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load saved playlists</source>
        <translation>Cargar listas guardadas</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start playing</source>
        <translation>Reproducir inmediatamente</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load last track on startup</source>
        <translation>Cargar la última pista reproducida antes de cerrar</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remember time of last track</source>
        <translation>Recordar dónde quedó la última pista</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stop behaviour</source>
        <translation>Detener comportamiento</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load last track after pressing stop</source>
        <translation>Cargar la última pista luego de presionar Detener</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Look</source>
        <translation>Vista</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show numbers</source>
        <translation>Mostrar número de pista en la lista</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>&apos;italic text&apos;</source>
        <translation>&apos;en itálica&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Example</source>
        <translation>Ejemplo</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>*bold text*</source>
        <translation>*en negrita*</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Show clear button</source>
        <translation>Mostrar botón transparente</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Show covers</source>
        <translation>Mostrar portadas</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show rating</source>
        <translation>Mostrar calificación</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.cpp" line="+216"/>
        <source>Playlist look: Invalid expression</source>
        <translation>Apariencia de la Lista de Reproducción. Expresión inválida</translation>
    </message>
</context>
<context>
    <name>GUI_PreferenceDialog</name>
    <message>
        <location filename="../src/Gui/Preferences/PreferenceDialog/GUI_PreferenceDialog.ui" line="+136"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
</context>
<context>
    <name>GUI_ProxyPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.ui" line="+20"/>
        <source>Host</source>
        <translation>Servidor</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Username</source>
        <translation>Nombre de usuario</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Save username/password</source>
        <translation>Guardar nombre de usuario/contraseñá</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Active</source>
        <translation>Activar</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Automatic search</source>
        <translation>Búsqueda automática</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.cpp" line="+57"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
</context>
<context>
    <name>GUI_RemoteControlPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.ui" line="+36"/>
        <source>Detectable via UDP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Remote control URL</source>
        <translation>Url para control remoto</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Port</source>
        <translation>Puerto</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>UDP port</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.cpp" line="+59"/>
        <source>If activated, Sayonara will answer an UDP request that it is remote controlable</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+36"/>
        <source>Remote control</source>
        <translation>Control remoto</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Port %1 already in use</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_SearchPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Search/GUI_SearchPreferences.ui" line="+23"/>
        <source>Example</source>
        <translation>Ejemplo</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Case insensitive</source>
        <translation>Insensible a mayúsculas</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Option</source>
        <translation>Opción</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Ignore accents</source>
        <translation>Ignorar acentos</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Ignore special characters</source>
        <translation>Ignorar caracteres especiales</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.ui" line="+44"/>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="+149"/>
        <source>Press shortcut</source>
        <translation>Teclée atajo</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="-48"/>
        <source>Shortcuts</source>
        <translation>Atajos</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Double shortcuts found</source>
        <translation>Atajo ya usado</translation>
    </message>
</context>
<context>
    <name>GUI_StreamRecorderPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.ui" line="+30"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Target directory</source>
        <translation>Carpeta de destino</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Automatic recording</source>
        <translation>Grabación automática</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Create session directory</source>
        <translation>Crear carpeta de sesión</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Directory</source>
        <translation>Carpeta de sesión</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Choose available placeholders</source>
        <translation>Elija un marcador de posición disponibles</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Path template</source>
        <translation>Plantilla de la ruta</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Example</source>
        <translation>Ejemplo</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.cpp" line="+172"/>
        <source>Choose target directory</source>
        <translation>Elegir carpeta de destino</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Target directory is empty</source>
        <translation>La carpeta de destino está vacía</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Please choose another directory</source>
        <translation>Por favor elige otra carpeta</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cannot create %1</source>
        <translation>No se puede crear% 1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Template path is not valid</source>
        <translation>Plantilla de la ruta non válida</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stream recorder</source>
        <translation>Grabador de emisión</translation>
    </message>
</context>
<context>
    <name>GUI_StreamPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.ui" line="+17"/>
        <source> ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Buffer size</source>
        <translation>Tamaño del buffer</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show history</source>
        <translation>Mostrar historia</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Streams in new tab</source>
        <translation>Abrir emisiones en una pestaña nueva</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.cpp" line="+57"/>
        <source>%1 and %2</source>
        <translation>%1 y %2</translation>
    </message>
</context>
<context>
    <name>GUI_CssEditor</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_CssEditor.ui" line="+97"/>
        <source>Edit style sheet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Dark mode</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_FontPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.ui" line="+84"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Font size</source>
        <translation>Tamaño de letra</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Font name</source>
        <translation>Nombre de la fuente</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.cpp" line="+129"/>
        <location line="+1"/>
        <source>Inherit</source>
        <translation>La del programa</translation>
    </message>
</context>
<context>
    <name>GUI_IconPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.ui" line="+20"/>
        <source>Also apply this icon theme to the dark style</source>
        <translation>Aplicar también el icono del tema al estilo &apos;dark&apos;</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.cpp" line="+96"/>
        <location line="+104"/>
        <source>System theme</source>
        <translation>Tema del sistema</translation>
    </message>
    <message>
        <location line="-96"/>
        <source>Icons</source>
        <translation>Iconos</translation>
    </message>
</context>
<context>
    <name>GUI_UiPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.ui" line="+57"/>
        <source>Fading cover</source>
        <translation>Activar fading de carátula</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit style sheet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.cpp" line="+48"/>
        <source>User Interface</source>
        <translation>Interfaz de usuario</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show large cover</source>
        <translation>Mostrar portada grande</translation>
    </message>
</context>
<context>
    <name>GUI_Shutdown</name>
    <message>
        <location filename="../src/Gui/Shutdown/GUI_Shutdown.ui" line="+14"/>
        <location line="+69"/>
        <source>Shutdown</source>
        <translation>Apagar</translation>
    </message>
    <message>
        <location line="-56"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>minutes</source>
        <translation>minutos</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shutdown after playlist finished</source>
        <translation>Apagar al acabar la lista</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Shutdown after</source>
        <translation>Apagar tras</translation>
    </message>
</context>
<context>
    <name>GUI_SomaFM</name>
    <message>
        <location filename="../src/Gui/SomaFM/GUI_SomaFM.ui" line="+42"/>
        <source>Donate to Soma.fm</source>
        <translation>Donar a Soma.fm</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Streams</source>
        <translation>Emisiones</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.ui" line="+14"/>
        <source>Search Soundcloud</source>
        <translation>Buscar en Soundcloud</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Search artist</source>
        <translation>Buscar artista</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudLibrary</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.ui" line="+26"/>
        <source>Library</source>
        <translation>Biblioteca</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Search for title, interprets and albums</source>
        <translation>Buscar por título, intérpretes y álbumes</translation>
    </message>
</context>
<context>
    <name>GUI_CoverEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.ui" line="+57"/>
        <source>Replace</source>
        <translation>Sustituir</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="+223"/>
        <source>Original</source>
        <translation>Original</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="-56"/>
        <location line="+9"/>
        <source>File has no cover</source>
        <translation>El archivo no tiene carátula</translation>
    </message>
</context>
<context>
    <name>GUI_FailMessageBox</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.ui" line="+22"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.cpp" line="+55"/>
        <source>File exists</source>
        <translation>El archivo existe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Writeable</source>
        <translation>Escribible</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Some files could not be saved</source>
        <translation>Algunos archivos no pudieron ser guardados</translation>
    </message>
</context>
<context>
    <name>GUI_TagEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.ui" line="+115"/>
        <source>Discnumber</source>
        <translation>Disco número</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Album artist</source>
        <translation>Artista del álbum</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>all</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tag from path</source>
        <translation>Etiquetar con la ruta</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Undo all</source>
        <translation>Deshacer todo</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Read only file</source>
        <translation>Archivo de sólo lectura</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.cpp" line="+161"/>
        <source>Load complete album</source>
        <translation>Cargar álbum completo</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Metadata</source>
        <translation>Metadata</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tags from path</source>
        <translation>Etiquetas desde la ruta</translation>
    </message>
    <message numerus="yes">
        <location line="+81"/>
        <source>Cannot apply expression to %n track(s)</source>
        <translation><numerusform>No se puede aplicar la expresión a %n pistas(s)</numerusform><numerusform>No se puede aplicar la expresión a %n pistas(s)</numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore these tracks?</source>
        <translation>¿Ignorar estas pistas?</translation>
    </message>
    <message>
        <location line="+394"/>
        <source>All changes will be lost</source>
        <translation>Todos los cambios se perderán</translation>
    </message>
</context>
<context>
    <name>GUI_TagFromPath</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.ui" line="+17"/>
        <source>Expression</source>
        <translation>Expresión</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Disc Nr</source>
        <translation>Nº disco</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Apply to all</source>
        <translation>Aplicar a todas</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.cpp" line="+92"/>
        <source>Tag</source>
        <translation>Etiqueta</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Please select text first</source>
        <translation>Por favor, seleccione texto primero</translation>
    </message>
</context>
<context>
    <name>StreamServer</name>
    <message>
        <location filename="../src/Components/Broadcasting/StreamServer.cpp" line="+228"/>
        <source>%1 wants to listen to your music.</source>
        <translation>%1 desea escuchar su música.</translation>
    </message>
</context>
<context>
    <name>AbstractLibrary</name>
    <message>
        <location filename="../src/Components/Library/AbstractLibrary.cpp" line="+849"/>
        <source>All %1 could be removed</source>
        <translation>Todo(a)s lo(a)s %1 podrían ser borrados</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 %3 could not be removed</source>
        <translation>%1 de %2 %3 no pudieron quitarse</translation>
    </message>
</context>
<context>
    <name>Library::Importer</name>
    <message>
        <location filename="../src/Components/Library/Importer/LibraryImporter.cpp" line="+228"/>
        <source>Cannot import tracks</source>
        <translation>No se pueden importar pistas</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>All files could be imported</source>
        <translation>Todos los archivos pueden importarse</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 files could be imported</source>
        <translation>%1 de %2 archivos pueden importarse</translation>
    </message>
</context>
<context>
    <name>Library::ReloadThread</name>
    <message>
        <location filename="../src/Components/Library/Threads/ReloadThread.cpp" line="+219"/>
        <source>Looking for covers</source>
        <translation>Buscando carátulas</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Reading files</source>
        <translation>Leyendo</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Deleting orphaned tracks</source>
        <translation>Eliminar pistas huérfanas</translation>
    </message>
</context>
<context>
    <name>Lyrics::LookupThread</name>
    <message>
        <location filename="../src/Components/Lyrics/LyricLookup.cpp" line="+171"/>
        <location line="+35"/>
        <source>Cannot fetch lyrics from %1</source>
        <translation>No se puede recuperar la letra de% 1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No lyrics found</source>
        <translation>Letras no hallados</translation>
    </message>
</context>
<context>
    <name>Shutdown</name>
    <message>
        <location filename="../src/Components/Shutdown/Shutdown.cpp" line="+100"/>
        <source>Computer will shutdown after playlist has finished</source>
        <translation>El ordenador se apagará al acabar la lista</translation>
    </message>
    <message numerus="yes">
        <location line="+29"/>
        <location line="+35"/>
        <source>Computer will shutdown in %n minute(s)</source>
        <translation><numerusform>La computadora se apagará en %n minutos(s)</numerusform><numerusform>La computadora se apagará en %n minutos(s)</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::JsonParser</name>
    <message>
        <location filename="../src/Components/Streaming/Soundcloud/SoundcloudJsonParser.cpp" line="+123"/>
        <source>Website</source>
        <translation>Sitio web</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+236"/>
        <source>Permalink Url</source>
        <translation>Url Permalink</translation>
    </message>
    <message>
        <location line="-223"/>
        <source>Followers/Following</source>
        <translation>Seguidores/Siguiendo</translation>
    </message>
    <message>
        <location line="+93"/>
        <location line="+134"/>
        <source>Purchase Url</source>
        <translation>Url para compras</translation>
    </message>
</context>
<context>
    <name>Gui::CoverButton</name>
    <message>
        <location filename="../src/Gui/Covers/CoverButton.cpp" line="+119"/>
        <source>Search an alternative cover</source>
        <translation>Buscar una portada alternativa</translation>
    </message>
</context>
<context>
    <name>HistoryContainer</name>
    <message>
        <location filename="../src/Gui/History/HistoryContainer.cpp" line="+25"/>
        <source>History</source>
        <translation>Historial</translation>
    </message>
</context>
<context>
    <name>GUI_InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.cpp" line="+92"/>
        <source>Write cover to tracks</source>
        <translation>Guardar la carátula junto al archivo de audio</translation>
    </message>
</context>
<context>
    <name>Library::CoverViewContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/CoverViewContextMenu.cpp" line="+217"/>
        <source>Toolbar</source>
        <translation>Barra de herramientas</translation>
    </message>
</context>
<context>
    <name>Library::GUI_CoverView</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/GUI_CoverView.cpp" line="+214"/>
        <source>Use Ctrl + mouse wheel to zoom</source>
        <translation>Ampliación: Ctrl+Rueda</translation>
    </message>
</context>
<context>
    <name>Directory::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryContextMenu.cpp" line="+295"/>
        <source>Rename by metadata</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Collapse all</source>
        <translation>Colapsar todo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to another library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to another library</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Directory::TreeView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryTreeView.cpp" line="+410"/>
        <source>Copy here</source>
        <translation>Copiar aquí</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move here</source>
        <translation>Mover aquí</translation>
    </message>
</context>
<context>
    <name>GUI_DirectoryView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/GUI_DirectoryView.cpp" line="+183"/>
        <source>Could not create directory</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::GenreView</name>
    <message>
        <location filename="../src/Gui/Library/GenreView.cpp" line="+114"/>
        <source>Updating genres</source>
        <translation>Actualizando géneros</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Do you really want to remove %1 from all tracks?</source>
        <translation>¿Realmente desear remover %1 de todas las pistas?</translation>
    </message>
</context>
<context>
    <name>Library::GUI_EmptyLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_EmptyLibrary.cpp" line="+94"/>
        <source>Please choose a name for your library</source>
        <translation>Por favor escoge un nombre para tu biblioteca</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Please choose another name for your library</source>
        <translation>Por favor escoge otro nombre para tu biblioteca</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>The file path is invalid</source>
        <translation>La ruta del archivo es inválida</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library with the same file path already exists</source>
        <translation>Ya existe una biblioteca con la misma ruta</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library which contains this file path already exists</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::HeaderView</name>
    <message>
        <location filename="../src/Gui/Library/Header/HeaderView.cpp" line="+198"/>
        <source>Resize columns</source>
        <translation>Redimensionar columnas</translation>
    </message>
</context>
<context>
    <name>Library::TrackModel</name>
    <message>
        <location filename="../src/Gui/Library/TableView/TrackModel.cpp" line="+144"/>
        <source>kBit/s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_DeleteDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_DeleteDialog.cpp" line="+87"/>
        <source>Only from library</source>
        <translation>Sólo de la biblioteca</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>You are about to delete %n file(s)</source>
        <translation><numerusform>Estás a punto de eliminar %n archivo(s)</numerusform><numerusform>Estás a punto de eliminar %n archivo(s)</numerusform></translation>
    </message>
</context>
<context>
    <name>Library::GUI_LibraryReloadDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_ReloadLibraryDialog.cpp" line="+78"/>
        <source>Fast scan</source>
        <translation>Búsqueda rápida</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deep scan</source>
        <translation>Búsqueda profunda</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Only scan for new and deleted files</source>
        <translation>Sólo buscar archivos nuevos o borrados</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scan all files in your library directory</source>
        <translation>Escanear todos los archivos en tu carpeta de biblioteca</translation>
    </message>
</context>
<context>
    <name>Library::LocalLibraryMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/LocalLibraryMenu.cpp" line="+154"/>
        <source>Statistics</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit library</source>
        <translation>Modificar biblioteca</translation>
    </message>
</context>
<context>
    <name>Gui::MergeMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/MergeMenu.cpp" line="+52"/>
        <location line="+53"/>
        <source>Merge</source>
        <translation>Fusionar</translation>
    </message>
</context>
<context>
    <name>GUI_ControlsBase</name>
    <message>
        <location filename="../src/Gui/Player/GUI_ControlsBase.cpp" line="+71"/>
        <source>Sayonara Player</source>
        <translation>Reproductor Sayonara</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright</source>
        <translation>Derechos de autor</translation>
    </message>
</context>
<context>
    <name>GUI_Logger</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.cpp" line="+275"/>
        <source>Cannot open file</source>
        <translation>No se puede abrir el archivo</translation>
    </message>
</context>
<context>
    <name>Menubar</name>
    <message>
        <location filename="../src/Gui/Player/GUI_PlayerMenubar.cpp" line="+323"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+18"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Show large cover</source>
        <translation>Mostrar portada grande</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fullscreen</source>
        <translation>Pantalla completa</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Media files</source>
        <translation>Archivos multimedia</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Abrir archivos multimedia</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please visit the forum at</source>
        <translation>Por favor, visite el foro en</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>FAQ</source>
        <translation>FAQ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>About Sayonara</source>
        <translation>Acerca de Sayonara</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Donate</source>
        <translation>Donar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Special thanks to all the brave translators</source>
        <translation>Especial agradecimiento para todos los valientes traductores</translation>
    </message>
</context>
<context>
    <name>TrayIconContextMenu</name>
    <message>
        <location filename="../src/Gui/Player/GUI_TrayIcon.cpp" line="+176"/>
        <source>Current song</source>
        <translation>Canción actual</translation>
    </message>
</context>
<context>
    <name>VersionChecker</name>
    <message>
        <location filename="../src/Gui/Player/VersionChecker.cpp" line="+77"/>
        <source>A new version is available!</source>
        <translation>¡Hay una nueva versión disponible!</translation>
    </message>
</context>
<context>
    <name>GUI_Playlist</name>
    <message>
        <location filename="../src/Gui/Playlist/GUI_Playlist.cpp" line="+241"/>
        <source>Playlist empty</source>
        <translation>Lista vacía</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Media files</source>
        <translation>Archivos multimedia</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Abrir archivos multimedia</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Playlist name already exists</source>
        <translation>Este nombre de lista ya existe</translation>
    </message>
</context>
<context>
    <name>Playlist::ActionMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistActionMenu.cpp" line="+193"/>
        <source>Please set library path first</source>
        <translation>Seleccione primero la ruta de la biblioteca</translation>
    </message>
</context>
<context>
    <name>Playlist::BottomBar</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistBottomBar.cpp" line="+242"/>
        <source>Please set library path first</source>
        <translation>Seleccione primero la ruta de la biblioteca</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel shutdown?</source>
        <translation>¿Cancelar el apagado?</translation>
    </message>
</context>
<context>
    <name>Playlist::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistContextMenu.cpp" line="+171"/>
        <source>Jump to current track</source>
        <translation>Saltar a la pista actual.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show track in library</source>
        <translation>Mostrar pista en la biblioteca</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Playlist mode</source>
        <translation>Modo lista de reproducción</translation>
    </message>
</context>
<context>
    <name>Playlist::Model</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistModel.cpp" line="+474"/>
        <source>Goto row</source>
        <translation>Ir a la fila</translation>
    </message>
</context>
<context>
    <name>GUI_Crossfader</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Crossfader.cpp" line="+75"/>
        <location line="+12"/>
        <source>Crossfader</source>
        <translation>Solapamiento</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 y %2</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Crossfader does not work with Alsa</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Gapless playback does not work with Alsa</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Equalizer</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Equalizer.cpp" line="+137"/>
        <location line="+40"/>
        <source>Linked sliders</source>
        <translation>Sliders enlazados</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Equalizer</source>
        <translation>Ecualizador</translation>
    </message>
    <message>
        <location line="+248"/>
        <source>Name %1 not allowed</source>
        <translation>El nombre %1 no está permitido</translation>
    </message>
</context>
<context>
    <name>GUI_LevelPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_LevelPainter.cpp" line="+157"/>
        <source>Level</source>
        <translation>Nivel</translation>
    </message>
</context>
<context>
    <name>GUI_SpectrogramPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_SpectrogramPainter.cpp" line="+77"/>
        <source>Spectrogram</source>
        <translation>Spectograma </translation>
    </message>
</context>
<context>
    <name>GUI_Spectrum</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Spectrum.cpp" line="+150"/>
        <source>Spectrum</source>
        <translation>Espectro</translation>
    </message>
</context>
<context>
    <name>GUI_StyleSettings</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_StyleSettings.cpp" line="+203"/>
        <source>There are some unsaved settings&lt;br /&gt;Save now?</source>
        <translation>Hay opciones sin guardar.&lt;br /&gt;¿Guardar ahora?</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Please specify a name</source>
        <translation>Por favor, especifique un nombre</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Save changes?</source>
        <translation>¿Guardar los cambios?</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistChooser</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_PlaylistChooser.cpp" line="+91"/>
        <location line="+38"/>
        <source>No playlists found</source>
        <translation>0 listas halladas</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Could not rename playlist</source>
        <translation>No se pudo renombrar la lista de reprodución</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Name is invalid</source>
        <translation>El nombre es inválido</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Do you really want to delete %1?</source>
        <translation>Realmente deseas eliminar %1?</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not delete playlist %1</source>
        <translation>No se pudo eliminar la lista de reproducción %1</translation>
    </message>
</context>
<context>
    <name>Gui::AbstractStationPlugin</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/AbstractStationPlugin.cpp" line="+243"/>
        <source>Cannot open stream</source>
        <translation>No se puede abrir la fuente</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please choose another name</source>
        <translation>Por favor, escoge otro nombre</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Do you really want to delete %1</source>
        <translation>¿Realmente quiere borrar %1?</translation>
    </message>
</context>
<context>
    <name>GUI_Podcasts</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Podcasts.cpp" line="+65"/>
        <source>Podcast</source>
        <translation>Podcast</translation>
    </message>
</context>
<context>
    <name>GUI_Stream</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Stream.cpp" line="+68"/>
        <source>Search radio station</source>
        <translation>Buscar estación de radio</translation>
    </message>
</context>
<context>
    <name>GUI_EnginePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Engine/GUI_EnginePreferences.cpp" line="+53"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutEntry</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutEntry.cpp" line="+55"/>
        <source>Enter shortcut</source>
        <translation>Teclée el atajo</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Shortcut already in use</source>
        <translation>Atajo ya usado</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Test</source>
        <translation>Probar</translation>
    </message>
</context>
<context>
    <name>SomaFM::StationModel</name>
    <message>
        <location filename="../src/Gui/SomaFM/SomaFMStationModel.cpp" line="+126"/>
        <source>Cannot fetch stations</source>
        <translation>No se encuentran estaciones (revisar conexión)</translation>
    </message>
</context>
<context>
    <name>SC::GUI_ArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.cpp" line="+79"/>
        <source>Query too short</source>
        <translation>Solicitud muy corta</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>No artists found</source>
        <translation>Artistas no hallados</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>Found %n artist(s)</source>
        <translation><numerusform>Se encontraron %n artistas(s)</numerusform><numerusform>Se encontraron %n artistas(s)</numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+56"/>
        <source>%n playlist(s) found</source>
        <translation><numerusform>Se encontraron %n listas(s) de reproducción</numerusform><numerusform>Se encontraron %n listas(s) de reproducción</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::GUI_Library</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.cpp" line="+56"/>
        <source>Add artist</source>
        <translation>Añadir artista</translation>
    </message>
</context>
<context>
    <name>TagTextInput</name>
    <message>
        <location filename="../src/Gui/Tagging/TagTextInput.cpp" line="+126"/>
        <source>Very first letter to upper case</source>
        <translation>Sólo la primera letra en mayúscula</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First letters to upper case</source>
        <translation>Las primeras letras, en mayúscula</translation>
    </message>
</context>
<context>
    <name>Library::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Utils/ContextMenu/LibraryContextMenu.cpp" line="+196"/>
        <source>Play in new tab</source>
        <translation>Reproducir en una nueva pestaña</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Standard view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cover view</source>
        <translation>Carátulas</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>The toolbar is visible when there are tracks with differing file types listed in the track view</source>
        <translation>La barra de herramientas está visible cuando hay pistas con diferentes tipos de archivos enumerados en la vista de pista</translation>
    </message>
</context>
<context>
    <name>Gui::ImageSelectionDialog</name>
    <message>
        <location filename="../src/Gui/Utils/ImageSelectionDialog.cpp" line="+57"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Any files</source>
        <translation>Cualquier archivo</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open image files</source>
        <translation>Abrir archivos de imagen</translation>
    </message>
</context>
<context>
    <name>Gui::StreamRecorderPreferenceAction</name>
    <message>
        <location filename="../src/Gui/Utils/PreferenceAction.cpp" line="+163"/>
        <location line="+7"/>
        <source>Stream Recorder</source>
        <translation>Grabador de emisión</translation>
    </message>
</context>
<context>
    <name>Gui::ShortcutPreferenceAction</name>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Shortcuts</source>
        <translation>Atajos</translation>
    </message>
</context>
<context>
    <name>Gui::MiniSearcher</name>
    <message>
        <location filename="../src/Gui/Utils/SearchableWidget/MiniSearcher.cpp" line="+92"/>
        <source>Arrow up</source>
        <translation>Flecha envío</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Previous search result</source>
        <translation>Resultado de búsqueda previa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Arrow down</source>
        <translation>Flecha abajo</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Next search result</source>
        <translation>Próximo resultado de búsqueda</translation>
    </message>
</context>
<context>
    <name>Lang</name>
    <message>
        <location filename="../src/Utils/Language/Language.cpp" line="+79"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>Acción</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Actions</source>
        <translation>Acciones</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Activate</source>
        <translation>Activar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>Activar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add tab</source>
        <translation>Añadir pestaña</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album</source>
        <translation>Álbum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artists</source>
        <translation>Artistas del álbum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Albums</source>
        <translation>Álbumes</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All</source>
        <translation>Todas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Append</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Application</source>
        <translation>Aplicación</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>Aplicar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artist</source>
        <translation>Artista</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artists</source>
        <translation>Artistas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ascending</source>
        <translation>Ascendente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic</source>
        <translation>Automático</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bitrate</source>
        <translation>Tasa de bits</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Broadcast</source>
        <translation>Emisión</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>by</source>
        <translation>por</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot find Lame MP3 encoder</source>
        <translation>No se puede encontrar cofificador LAME MP3</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>Borrar todo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear selection</source>
        <translation>Limpiar selección</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close others</source>
        <translation>Cerrar las otras</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close tab</source>
        <translation>Cerrar pestaña</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue</source>
        <translation>Continuar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Covers</source>
        <translation>Carátulas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created</source>
        <translation>Creado</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create new directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Create a new library</source>
        <translation>Crear una nueva biblioteca</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark Mode</source>
        <translation>Modo oscuro</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Days</source>
        <translation>Días</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>d</source>
        <translation>d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Default</source>
        <translation>Por defecto</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+84"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location line="-82"/>
        <source>Descending</source>
        <translation>Descendente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory</source>
        <translation>Directorio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directories</source>
        <translation>Directorios</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disc</source>
        <translation>Nº disco</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dur.</source>
        <translation>Dur.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dynamic playback</source>
        <translation>Reproducción dinámica (se añade una pista nueva automáticamente y la lista no se acaba)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Empty input</source>
        <translation>Vacío entrada</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter name</source>
        <translation>Introduzca un nombre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please enter new name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter URL</source>
        <translation>Introduzca un URL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry</source>
        <translation>Entrada</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entries</source>
        <translation>Entradas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fast</source>
        <translation>Rápida</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filename</source>
        <translation>Nombre del archivo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Files</source>
        <translation>Archivos</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filesize</source>
        <translation>Tamaño de archivo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File type</source>
        <translation>Tipo de archivo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filter</source>
        <translation>Filtro</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>1st</source>
        <translation>1º</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Font</source>
        <translation>Tipo de letra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation>Tipos de letra</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fulltext</source>
        <translation>Texto completo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Gapless playback</source>
        <translation>Enlazar pistas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genre</source>
        <translation>Género</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genres</source>
        <translation>Géneros</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hide</source>
        <translation>Ocultar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import directory</source>
        <translation>Importar directorio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import files</source>
        <translation>Importar archivos</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation>Inactivo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading</source>
        <translation>Cargando</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading %1</source>
        <translation>Cargando: %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid characters</source>
        <translation>Carácters no válido</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+f</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shift</source>
        <translation>Mayús</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Backspace</source>
        <translation>Retroceso</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library</source>
        <translation>Biblioteca</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library path</source>
        <translation>Ruta de la biblioteca</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library view type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Listen</source>
        <translation>Oír</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Live Search</source>
        <translation>Busqueda en vivo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Logger</source>
        <translation>Registro</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lyrics</source>
        <translation>Letras</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Menu</source>
        <translation>Menú</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>Minimizar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing</source>
        <translation>Faltante</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Modified</source>
        <translation>Modificado</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Months</source>
        <translation>Meses</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute</source>
        <translation>Silencio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute off</source>
        <translation>Quitar silenciar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation>Nueva</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next page</source>
        <translation>Página siguiente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next track</source>
        <translation>Pista siguiente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No albums</source>
        <translation>Ningún album</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+144"/>
        <source>Tracks</source>
        <translation>Pistas</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Move down</source>
        <translation>Mover abajo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Move up</source>
        <translation>Mover arriba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation>Aceptar</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>on</source>
        <translation>el</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Abierta</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open directory</source>
        <translation>Directorio abierto</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open file</source>
        <translation>Abrir archivo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>or</source>
        <translation>o</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Overwrite</source>
        <translation>Sobrescribir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pause</source>
        <translation>Pausa</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playing time</source>
        <translation>Tiempo total de reproducción</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play in new tab</source>
        <translation>Reproducir en nueva pestaña</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlist</source>
        <translation>Lista de reproducción</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlists</source>
        <translation>Listas de reproducción</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play next</source>
        <translation>Reproducir siguiente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play/Pause</source>
        <translation>Reproducir/Pausa</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugin</source>
        <translation>Complemento</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Podcasts</source>
        <translation>Podcasts</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous page</source>
        <translation>Página anterior</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous track</source>
        <translation>Pista anterior</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Purchase Url</source>
        <translation>Url para compras</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio</source>
        <translation>Radio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio Station</source>
        <translation>Emisora de Radio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rating</source>
        <translation>Clasificación</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Really</source>
        <translation>Realmente</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Refresh</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload Library</source>
        <translation>Recargar biblioteca</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename</source>
        <translation>Cambiar de nombre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat 1</source>
        <translation>Repetir una</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat all</source>
        <translation>Repetir todas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Replace</source>
        <translation>Sustituir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation>Reiniciar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation>Reintentar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reverse order</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Sampler</source>
        <translation>Muestreo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shuffle</source>
        <translation>Activar reproducción aleatoria</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shutdown</source>
        <translation>Apagar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save as</source>
        <translation>Guardar como</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save to file</source>
        <translation>Guardar a archivo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan for audio files</source>
        <translation>Buscar archivos de audio</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search next</source>
        <translation>Buscar más</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search previous</source>
        <translation>Buscar anterior</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2nd</source>
        <translation>2º</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek backward</source>
        <translation>Búsqueda hacia atrás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek forward</source>
        <translation>Búsqueda hacia adelante</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Album Artists</source>
        <translation>Mostrar artista del álbum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Covers</source>
        <translation>Mostrar carátulas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Library</source>
        <translation>Mostrar la biblioteca</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Similar artists</source>
        <translation>Artistas similares</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by</source>
        <translation>Ordenar por</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stop</source>
        <translation>Detener</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streams</source>
        <translation>Emisiones</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream URL</source>
        <translation>Emisiones Url</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Success</source>
        <translation>Éxito</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>th</source>
        <translation>º</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3rd</source>
        <translation>3º</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track</source>
        <translation>Pista</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track number</source>
        <translation>Número de pista</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>track on</source>
        <translation>pista en</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Tree</source>
        <translation>Árbol</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Undo</source>
        <translation>Deshacer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown album</source>
        <translation>Album desconocido</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown artist</source>
        <translation>Artista desconocido</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown title</source>
        <translation>Título desconocido</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown genre</source>
        <translation>Género desconocido</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown placeholder</source>
        <translation>Desconocido marcador de posición</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown year</source>
        <translation>Año desconocido</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Various</source>
        <translation>Varios</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various albums</source>
        <translation>Varias álbumes</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various artists</source>
        <translation>Varios artistas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various tracks</source>
        <translation>Varias pistas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume down</source>
        <translation>Bajar volumen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume up</source>
        <translation>Subir volumen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation>Advertencia</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Weeks</source>
        <translation>Semanas</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Year</source>
        <translation>Año</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Years</source>
        <translation>Años</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Sí</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>Ampliación</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>No directories</source>
        <translation>Sin directorios</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n directory(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No files</source>
        <translation>Sin archivos</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n file(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No playlists</source>
        <translation>Sin listas de reproducción</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n playlist(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks</source>
        <translation>Sin pistas</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks found</source>
        <translation>No se encontraron pistas</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s) found</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
</context>
</TS>