<?xml version="1.0" ?><!DOCTYPE TS><TS language="tr_TR" sourcelanguage="en" version="2.1">
<context>
    <name>GUI_AlternativeCovers</name>
    <message>
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.ui" line="+27"/>
        <source>Online Search</source>
        <translation>Çevrimiçi Ara</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Automatic search</source>
        <translation>Otomatik ara</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Text search</source>
        <translation>Metin ara</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Server</source>
        <translation>Sunucu</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Local Search</source>
        <translation>Yerel Arama</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Select and preview multiple covers</source>
        <translation>Birden fazla kapağı seç ve önizle</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Alternative Cover</source>
        <translation>Alternatif Kapak</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Start search automatically</source>
        <translation>Aramayı otomatik başlat</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.cpp" line="+283"/>
        <source>%n cover(s) found</source>
        <translation><numerusform>%n kapak bulundu</numerusform><numerusform>%n kapak bulundu</numerusform></translation>
    </message>
    <message>
        <location line="+133"/>
        <location line="+31"/>
        <source>Also save cover to %1</source>
        <translation>Ayrıca kapağı %1’e kaydet</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Cover web search is not enabled</source>
        <translation>Kapak web araması etkin değil</translation>
    </message>
</context>
<context>
    <name>GUI_History</name>
    <message>
        <location filename="../src/Gui/History/GUI_History.ui" line="+14"/>
        <source>Dialog</source>
        <translation>Diyalog</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Newer</source>
        <translation>Daha yeni</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Older</source>
        <translation>Daha eski</translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.ui" line="+20"/>
        <source>Info / Edit</source>
        <translation>Bilgi / Düzenle</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Loading files...</source>
        <translation>Dosyalar yükleniyor...</translation>
    </message>
</context>
<context>
    <name>GUI_Lyrics</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.ui" line="+28"/>
        <source>Save Lyrics</source>
        <translation>Şarkı Sözlerini Kaydet</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>switch</source>
        <translation>düğme</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Zoom</source>
        <translation>Yakınlaştır</translation>
    </message>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.cpp" line="+303"/>
        <source>Save lyrics not supported</source>
        <translation>Desteklenmeyen şarkı sözlerini kaydet</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Overwrite lyrics</source>
        <translation>Sözlerin üzerine yaz</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save lyrics</source>
        <translation>Şarkı Sözlerini Kaydet</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source</source>
        <translation>Kaynak</translation>
    </message>
</context>
<context>
    <name>GUI_ImportDialog</name>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.ui" line="+14"/>
        <source>Import</source>
        <translation>İçe aktar</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Import tracks to library</source>
        <translation>Parçaları kütüphaneye aktar</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select target folder</source>
        <translation>Hedef dizini seç</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.cpp" line="+126"/>
        <source>Loading tracks</source>
        <translation>Parçalar yükleniyor</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No tracks</source>
        <translation>Parça yok</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Importing</source>
        <translation>İçe aktarıldı</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>Tamamlandı</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Rollback</source>
        <translation>Geri al</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cancelled</source>
        <translation>İptal edildi</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Choose target directory</source>
        <translation>Hedef dizini seç</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>%1&lt;br /&gt;is no library directory</source>
        <translation>%1&lt;br /&gt;kütüphane rehberi yok</translation>
    </message>
</context>
<context>
    <name>GUI_LocalLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.ui" line="+17"/>
        <source>Library</source>
        <translation>Kütüphane</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.cpp" line="+352"/>
        <source>Audio files</source>
        <translation>Ses dosyaları</translation>
    </message>
</context>
<context>
    <name>GUI_Controls</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Controls.ui" line="+318"/>
        <source>Written by Michael Lugmair (Lucio Carreras)</source>
        <translation>Yazan: Michael Lugmair (Lucio Carreras)</translation>
    </message>
</context>
<context>
    <name>GUI_Player</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Player.ui" line="+14"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
</context>
<context>
    <name>GUI_AudioConverter</name>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.ui" line="+45"/>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="+277"/>
        <source>Start</source>
        <translation>Başlat</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Stop</source>
        <translation>Dur</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>#Threads</source>
        <translation>#Konular</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+187"/>
        <source>Quality</source>
        <translation>Kalite</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+52"/>
        <location line="+89"/>
        <source>Bitrate</source>
        <translation>Bit hızı</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="-164"/>
        <source>Audio Converter</source>
        <translation>Ses Dönüştürücü</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Threads</source>
        <translation>Konular</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+123"/>
        <source>Cannot find encoder</source>
        <translation>Çözücü bulunamadı</translation>
    </message>
    <message>
        <location line="-113"/>
        <location line="+14"/>
        <source>Playlist does not contain tracks which are supported by the converter</source>
        <translation>Oynatma listesi, dönüştürücü tarafından desteklenen parçaları içermiyor</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>No track will be converted.</source>
        <translation>Hiçbir parça dönüştürülmeyecek.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>These tracks will be ignored</source>
        <translation>Bu parçalar göz ardı edilecek</translation>
    </message>
    <message numerus="yes">
        <location line="+38"/>
        <source>Failed to convert %n track(s)</source>
        <translation><numerusform>%n parça dönüştürülemedi</numerusform><numerusform>%n parçalar dönüştürülemedi</numerusform></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please check the log files</source>
        <translation>Lütfen günlük dosyalarını kontrol edin</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>All tracks could be converted</source>
        <translation>Tüm parçalar dönüştürülebilir</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully finished</source>
        <translation>Başarıyla tamamlandı</translation>
    </message>
</context>
<context>
    <name>GUI_Bookmarks</name>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.ui" line="+116"/>
        <source>Loop</source>
        <translation>Döngü</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.cpp" line="+77"/>
        <location line="+53"/>
        <source>No bookmarks found</source>
        <translation>Yerimi bulunamadı</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Sorry, bookmarks can only be set for library tracks at the moment.</source>
        <translation>Üzgünüz, yer işaretleri şu an yalnızca kitaplık parçaları için ayarlanabilir.</translation>
    </message>
</context>
<context>
    <name>GUI_Broadcast</name>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.ui" line="+88"/>
        <source>Cannot Broadcast</source>
        <translation>Yayın Yapılamıyor</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.cpp" line="+104"/>
        <source>Dismiss</source>
        <translation>Reddet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dismiss all</source>
        <translation>Tümünü reddet</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%n listener(s)</source>
        <translation><numerusform>%n dinleyici</numerusform><numerusform>%n dinleyici</numerusform></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot broadcast on port %1</source>
        <translation>%1 bağlantı noktası üzerinden yayın yapılamıyor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maybe another application is using this port?</source>
        <translation>Belki başka bir uygulama bu bağlantı noktasını kullanıyor?</translation>
    </message>
</context>
<context>
    <name>GUI_Speed</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.ui" line="+48"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+134"/>
        <source>Speed</source>
        <translation>Hız</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Preserve pitch</source>
        <translation>Aralığı koru</translation>
    </message>
    <message>
        <location line="+30"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+1"/>
        <source>Pitch</source>
        <translation>Aralık</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 ve %2</translation>
    </message>
    <message>
        <location line="+70"/>
        <location line="+1"/>
        <source>%1 not found</source>
        <translation>%1 bulunamadı</translation>
    </message>
</context>
<context>
    <name>GUI_Style</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Style.ui" line="+14"/>
        <source>Style</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Spectrum</source>
        <translation>Spektrum</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Vert. spacing</source>
        <translation>Dikey aralık</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+88"/>
        <source>Rect height</source>
        <translation>Dikdörtgen yüksekliği</translation>
    </message>
    <message>
        <location line="-68"/>
        <location line="+44"/>
        <source>Hor. spacing</source>
        <translation>Yatay aralık</translation>
    </message>
    <message>
        <location line="-30"/>
        <location line="+75"/>
        <source>Fading steps</source>
        <translation>Solma adımları</translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Level</source>
        <translation>Seviye</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Rect width</source>
        <translation>Doğrultma genişliği</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ver. spacing</source>
        <translation>Dikey aralık</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Style settings</source>
        <translation>Görünüm ayarları</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Color 2</source>
        <translation>Renk 2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 1</source>
        <translation>Renk 1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color 3</source>
        <translation>Renk 3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 4</source>
        <translation>Renk 4</translation>
    </message>
</context>
<context>
    <name>GUI_TargetPlaylistDialog</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_TargetPlaylistDialog.ui" line="+14"/>
        <source>Choose target playlist</source>
        <translation>Hedef oynatma listesini seç</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>relative filepaths</source>
        <translation>göreceli yol</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save playlist as...</source>
        <translation>Oynatma listesini farklı kaydet...</translation>
    </message>
</context>
<context>
    <name>GUI_StationSearcher</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.ui" line="+14"/>
        <source>Search Radio Station</source>
        <translation>Radyo İstasyonu Ara</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.cpp" line="+58"/>
        <source>Show radio stations from %1 to %2</source>
        <translation>%1 ile %2 arasındaki radyo istasyonlarını göster</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Country</source>
        <translation>Ülke</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Type</source>
        <translation>Tür</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
</context>
<context>
    <name>GUI_BroadcastPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.ui" line="+20"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ask for permission</source>
        <translation>İzin iste</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.cpp" line="+161"/>
        <source>Port %1 already in use</source>
        <translation>Bağlantı noktası %1 zaten kullanımda</translation>
    </message>
</context>
<context>
    <name>GUI_CoverPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Covers/GUI_CoverPreferences.ui" line="+62"/>
        <source>Inactive</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>Etkin</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Delete all covers from the database</source>
        <translation>Tüm kapakları veritabanından sil</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear cache</source>
        <translation>Önbelleği temizle</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete all covers from the Sayonara directory</source>
        <translation>Tüm kapakları Sayonara dizininden sil</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete files</source>
        <translation>Dosyaları sil</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Save found covers into the library directory where the audio files are located</source>
        <translation>Bulunan kapakları ses dosyalarının bulunduğu kütüphane dizinine kaydet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers into library</source>
        <translation> Bulunan kapakları kütüphaneye kaydet</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fetch missing covers from the internet</source>
        <translation>İnternetten eksik albüm kapaklarını alın</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Saving covers to the database leads to significantly faster access but results in a bigger database</source>
        <translation> Kapakları veritabanına kaydetmek, önemli ölçüde daha hızlı erişim sağlar ancak daha büyük bir veritabanı oluşturur</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers to database</source>
        <translation> Bulunan kapakları veritabanına kaydet</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save found covers into Sayonara directory</source>
        <translation> Bulunan kapakları Sayonara dizinine kaydet</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cover name</source>
        <translation>Kapak adı</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name of cover file</source>
        <translation>Kapak dosyasının adı</translation>
    </message>
</context>
<context>
    <name>GUI_LanguagePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.ui" line="+29"/>
        <source>Check for update</source>
        <translation>Güncellemeleri kontrol et</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Download update</source>
        <translation>Güncellemeyi indir</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>English</source>
        <translation>İngilizce</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>For new languages I am always looking for translators</source>
        <translation>Yeni diller için her zaman çevirmen arıyorum</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.cpp" line="+63"/>
        <source>Language</source>
        <translation>Dil</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Cannot check for language update</source>
        <translation>Dil güncellemesi kontrol edilemiyor</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Language update available</source>
        <translation>Dil güncellemesi mevcut</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Language is up to date</source>
        <translation>Dil güncel</translation>
    </message>
    <message>
        <location line="+41"/>
        <location line="+24"/>
        <source>Cannot fetch language update</source>
        <translation>Dil güncellemesi alınamıyor</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Language was updated successfully</source>
        <translation>Dil başarıyla güncellendi</translation>
    </message>
</context>
<context>
    <name>GUI_LastFmPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.ui" line="+27"/>
        <source>Username</source>
        <translation>Kullanıcı adı</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Parola</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Scrobble time</source>
        <translation>Giriş süresi</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Login now</source>
        <translation>Şimdi gir</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.cpp" line="+163"/>
        <source>Logged in</source>
        <translation>Giriş yapıldı</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not logged in</source>
        <translation>Giriş yapılmadı</translation>
    </message>
</context>
<context>
    <name>GUI_LibraryPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.ui" line="+27"/>
        <source>Libraries</source>
        <translation>Kütüphaneler</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Library-Playlist Interaction</source>
        <translation>Kütüphane-Oynatma Listesi Etkileşimi</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>When drag and drop into playlist </source>
        <translation>Oynatma listesine sürükleyip bırakıldığında </translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+36"/>
        <source>do nothing (default)</source>
        <translation>hiçbir şey yapma (varsayılan)</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>start if stopped and playlist is empty</source>
        <translation>durdurulursa ve oynatma listesi boşsa başla</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>On double click, create playlist and</source>
        <translation>Çift tıklayın, oynatma listesi oluştur ve</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start playback if stopped</source>
        <translation>durdurulursa oynatmayı başlat</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>start playback immediately</source>
        <translation>hemen oynatmayı başlat</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(this is ignored when playlist is in &apos;append mode&apos;)</source>
        <translation>(oynatma listesi &apos;ekleme kipinde&apos; olduğunda bu dikkate alınmaz)</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Other</source>
        <translation>Diğer</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show &quot;Clear selection&quot; buttons</source>
        <translation>&quot;Seçimi temizle&quot; düğmelerini göster</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Ignore English article &quot;The&quot; in artist name</source>
        <translation>Sanatçı adına İngilizce &quot;The&quot; makalesini yoksay</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.cpp" line="+147"/>
        <source>Cannot edit library</source>
        <translation>Kütüphane düzenlenemiyor</translation>
    </message>
</context>
<context>
    <name>GUI_NotificationPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.ui" line="+17"/>
        <source>Timeout (ms)</source>
        <translation>Zaman aşımı (ms)</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.cpp" line="+98"/>
        <source>Notifications</source>
        <translation>Bildirimler</translation>
    </message>
</context>
<context>
    <name>GUI_PlayerPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.ui" line="+44"/>
        <source>Show system tray icon</source>
        <translation>Sistem tepsisi simgesini göster</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Hide instead of close</source>
        <translation>Kapatmak yerine gizle</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Start hidden</source>
        <translation>Gizli başlat</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update notifications</source>
        <translation>Güncelleme bildirimleri</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.cpp" line="+107"/>
        <source>This might cause Sayonara not to show up again.</source>
        <translation>Bu, Sayonara&apos;nın tekrar ortaya çıkmamasına neden olabilir.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>In this case use the &apos;--show&apos; option at the next startup.</source>
        <translation>Bu durumda bir sonraki açılışta &apos;--göster&apos; seçeneğini kullanın.</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.ui" line="+36"/>
        <source>Behavior</source>
        <translation>Davranış</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start up</source>
        <translation>Başlangıç</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load temporary playlists</source>
        <translation>Geçici oynatma listelerini yükle</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load saved playlists</source>
        <translation>Kaydedilen oynatma listelerini yükle</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start playing</source>
        <translation>Oynatmaya başla</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load last track on startup</source>
        <translation>Başlangıçta son parçayı yükle</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remember time of last track</source>
        <translation>Son parçanın zamanını hatırla</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stop behaviour</source>
        <translation>Davranışı durdur</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load last track after pressing stop</source>
        <translation>Durdurma işleminden sonra son parçayı yükle</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Look</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show numbers</source>
        <translation>Numaraları göster</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>&apos;italic text&apos;</source>
        <translation>&apos;italik metin&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Example</source>
        <translation>Örnek</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>*bold text*</source>
        <translation>*kalın metin*</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Show clear button</source>
        <translation>Temizle düğmesini göster</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Show covers</source>
        <translation>Kapakları göster</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show rating</source>
        <translation>Puanı göster</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.cpp" line="+216"/>
        <source>Playlist look: Invalid expression</source>
        <translation>Oynatma listesi görünümü: Geçersiz ifade</translation>
    </message>
</context>
<context>
    <name>GUI_PreferenceDialog</name>
    <message>
        <location filename="../src/Gui/Preferences/PreferenceDialog/GUI_PreferenceDialog.ui" line="+136"/>
        <source>Preferences</source>
        <translation>Tercihler</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Apply</source>
        <translation>Uygula</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
</context>
<context>
    <name>GUI_ProxyPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.ui" line="+20"/>
        <source>Host</source>
        <translation>Bilgisayar</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Username</source>
        <translation>Kullanıcı adı</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Save username/password</source>
        <translation>Kaydet, kullanıcı adı/parola</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Parola</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Active</source>
        <translation>Aktif</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Automatic search</source>
        <translation>Otomatik ara</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.cpp" line="+57"/>
        <source>Proxy</source>
        <translation>Vekil sunucu</translation>
    </message>
</context>
<context>
    <name>GUI_RemoteControlPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.ui" line="+36"/>
        <source>Detectable via UDP</source>
        <translation>UDP ile algılanabilir</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Remote control URL</source>
        <translation>Uzaktan kontrol URL&apos;si</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>UDP port</source>
        <translation>UDP bağlantı noktası</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.cpp" line="+59"/>
        <source>If activated, Sayonara will answer an UDP request that it is remote controlable</source>
        <translation>Etkinleştirilirse, Sayonara uzaktan kumanda edilebilir bir UDP isteğine yanıt verecektir</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Remote control</source>
        <translation>Uzaktan kontrol</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Port %1 already in use</source>
        <translation>Bağlantı noktası %1 zaten kullanımda</translation>
    </message>
</context>
<context>
    <name>GUI_SearchPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Search/GUI_SearchPreferences.ui" line="+23"/>
        <source>Example</source>
        <translation>Örnek</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Case insensitive</source>
        <translation>Büyük/Küçük duyarsız</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Option</source>
        <translation>Seçenek</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Ignore accents</source>
        <translation>Aksanları yoksay</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Ignore special characters</source>
        <translation>Özel karakterleri yoksay</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.ui" line="+44"/>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="+149"/>
        <source>Press shortcut</source>
        <translation>Kısayol tuşuna bas</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="-48"/>
        <source>Shortcuts</source>
        <translation>Kısayollar</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Double shortcuts found</source>
        <translation>Çift kısayol bulundu</translation>
    </message>
</context>
<context>
    <name>GUI_StreamRecorderPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.ui" line="+30"/>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Target directory</source>
        <translation>Hedef dizin</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Automatic recording</source>
        <translation>Otomatik kayıt</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Create session directory</source>
        <translation>Oturum dizini oluştur</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Directory</source>
        <translation>Oturum Dizini</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Choose available placeholders</source>
        <translation>Kullanılabilir yer tutucuları seçin</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Path template</source>
        <translation>Şablon yolu</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Example</source>
        <translation>Örnek</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.cpp" line="+172"/>
        <source>Choose target directory</source>
        <translation>Hedef dizini seç</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Target directory is empty</source>
        <translation>Hedef dizin boş</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Please choose another directory</source>
        <translation>Lütfen başka bir dizin seçin</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cannot create %1</source>
        <translation>Oluşturulamıyor %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Template path is not valid</source>
        <translation>Şablon yolu geçersiz</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stream recorder</source>
        <translation>Akış kaydedici</translation>
    </message>
</context>
<context>
    <name>GUI_StreamPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.ui" line="+17"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Buffer size</source>
        <translation>Tampon boyutu</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show history</source>
        <translation>Geçmişi göster</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Streams in new tab</source>
        <translation>Akışları yeni sekmede aç</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.cpp" line="+57"/>
        <source>%1 and %2</source>
        <translation>%1 ve %2</translation>
    </message>
</context>
<context>
    <name>GUI_CssEditor</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_CssEditor.ui" line="+97"/>
        <source>Edit style sheet</source>
        <translation>Görünüm sayfasını düzenle</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Dark mode</source>
        <translation>Koyu kip</translation>
    </message>
</context>
<context>
    <name>GUI_FontPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.ui" line="+84"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Font size</source>
        <translation>Yazı tipi boyutu</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bold</source>
        <translation>Kalın</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Font name</source>
        <translation>Yazı tipi adı</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.cpp" line="+129"/>
        <location line="+1"/>
        <source>Inherit</source>
        <translation>Devral</translation>
    </message>
</context>
<context>
    <name>GUI_IconPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.ui" line="+20"/>
        <source>Also apply this icon theme to the dark style</source>
        <translation>Ayrıca bu simge temasını karanlık biçime uygula</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.cpp" line="+96"/>
        <location line="+104"/>
        <source>System theme</source>
        <translation>Sistem teması</translation>
    </message>
    <message>
        <location line="-96"/>
        <source>Icons</source>
        <translation>Simgeler</translation>
    </message>
</context>
<context>
    <name>GUI_UiPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.ui" line="+57"/>
        <source>Fading cover</source>
        <translation>Soluk kapak</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit style sheet</source>
        <translation>Görünüm sayfasını düzenle</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.cpp" line="+48"/>
        <source>User Interface</source>
        <translation>Kullanıcı Arayüzü</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>General</source>
        <translation>Genel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show large cover</source>
        <translation>Büyük kapak göster</translation>
    </message>
</context>
<context>
    <name>GUI_Shutdown</name>
    <message>
        <location filename="../src/Gui/Shutdown/GUI_Shutdown.ui" line="+14"/>
        <location line="+69"/>
        <source>Shutdown</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location line="-56"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>minutes</source>
        <translation>dakika</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shutdown after playlist finished</source>
        <translation>Oynatma listesi bittikten sonra kapat</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Shutdown after</source>
        <translation>Sonra kapat</translation>
    </message>
</context>
<context>
    <name>GUI_SomaFM</name>
    <message>
        <location filename="../src/Gui/SomaFM/GUI_SomaFM.ui" line="+42"/>
        <source>Donate to Soma.fm</source>
        <translation>Soma.fm&apos;ye bağış yapın</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Streams</source>
        <translation>Akışlar</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.ui" line="+14"/>
        <source>Search Soundcloud</source>
        <translation>Soundcloud&apos;da Ara</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add</source>
        <translation>Ekle</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Search artist</source>
        <translation>Sanatçı ara</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudLibrary</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.ui" line="+26"/>
        <source>Library</source>
        <translation>Kütüphane</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Search for title, interprets and albums</source>
        <translation>Başlık, yorum ve albüm ara</translation>
    </message>
</context>
<context>
    <name>GUI_CoverEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.ui" line="+57"/>
        <source>Replace</source>
        <translation>Değiştir</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="+223"/>
        <source>Original</source>
        <translation>Orjinal</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="-56"/>
        <location line="+9"/>
        <source>File has no cover</source>
        <translation>Dosyanın kapağı yok</translation>
    </message>
</context>
<context>
    <name>GUI_FailMessageBox</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.ui" line="+22"/>
        <source>Details</source>
        <translation>Ayrıntılar</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.cpp" line="+55"/>
        <source>File exists</source>
        <translation>Dosya var</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Writeable</source>
        <translation>Yazılabilir</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Some files could not be saved</source>
        <translation>Bazı dosyalar kaydedilemedi</translation>
    </message>
</context>
<context>
    <name>GUI_TagEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.ui" line="+115"/>
        <source>Discnumber</source>
        <translation>Disk numarası</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Album artist</source>
        <translation>Albüm sanatçısı</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Comment</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>all</source>
        <translation>tümü</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tag from path</source>
        <translation>Yoldan etiket</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Undo all</source>
        <translation>Tümünü geri al</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Read only file</source>
        <translation>Sadece okunabilir dosya</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.cpp" line="+161"/>
        <source>Load complete album</source>
        <translation>Albümün tamamını yükle</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Metadata</source>
        <translation>Üstveri</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tags from path</source>
        <translation>Yoldaki etiketler</translation>
    </message>
    <message numerus="yes">
        <location line="+81"/>
        <source>Cannot apply expression to %n track(s)</source>
        <translation><numerusform>İfade %n parçasına uygulanamıyor</numerusform><numerusform>İfade %n parçasına uygulanamıyor</numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore these tracks?</source>
        <translation>Bu parçaları yoksay?</translation>
    </message>
    <message>
        <location line="+394"/>
        <source>All changes will be lost</source>
        <translation>Tüm değişiklikler kaybolacak</translation>
    </message>
</context>
<context>
    <name>GUI_TagFromPath</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.ui" line="+17"/>
        <source>Expression</source>
        <translation>İfade</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Disc Nr</source>
        <translation>Disk No</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Apply to all</source>
        <translation>Tümüne uygula</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.cpp" line="+92"/>
        <source>Tag</source>
        <translation>Etiket</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Please select text first</source>
        <translation>Lütfen önce metni seçin</translation>
    </message>
</context>
<context>
    <name>StreamServer</name>
    <message>
        <location filename="../src/Components/Broadcasting/StreamServer.cpp" line="+228"/>
        <source>%1 wants to listen to your music.</source>
        <translation>%1 müzik dinlemek istiyor.</translation>
    </message>
</context>
<context>
    <name>AbstractLibrary</name>
    <message>
        <location filename="../src/Components/Library/AbstractLibrary.cpp" line="+849"/>
        <source>All %1 could be removed</source>
        <translation>Tümü %1 kaldırılabilir</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 %3 could not be removed</source>
        <translation>%1 %2 %3 kaldırılamadı</translation>
    </message>
</context>
<context>
    <name>Library::Importer</name>
    <message>
        <location filename="../src/Components/Library/Importer/LibraryImporter.cpp" line="+228"/>
        <source>Cannot import tracks</source>
        <translation>Parçalar alınamıyor</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>All files could be imported</source>
        <translation>Tüm dosyalar içe aktarılabilir</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 files could be imported</source>
        <translation>%2 dosyanın %1&apos;i içe aktarıldı</translation>
    </message>
</context>
<context>
    <name>Library::ReloadThread</name>
    <message>
        <location filename="../src/Components/Library/Threads/ReloadThread.cpp" line="+219"/>
        <source>Looking for covers</source>
        <translation>Kapaklar aranıyor</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Reading files</source>
        <translation>Dosyalar okunuyor</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Deleting orphaned tracks</source>
        <translation>Artık parçalar siliniyor</translation>
    </message>
</context>
<context>
    <name>Lyrics::LookupThread</name>
    <message>
        <location filename="../src/Components/Lyrics/LyricLookup.cpp" line="+171"/>
        <location line="+35"/>
        <source>Cannot fetch lyrics from %1</source>
        <translation>Şarkı sözleri getirilemiyor %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No lyrics found</source>
        <translation>Şarkı sözü bulunamadı</translation>
    </message>
</context>
<context>
    <name>Shutdown</name>
    <message>
        <location filename="../src/Components/Shutdown/Shutdown.cpp" line="+100"/>
        <source>Computer will shutdown after playlist has finished</source>
        <translation>Oynatma listesi bittikten sonra bilgisayar kapanacak</translation>
    </message>
    <message numerus="yes">
        <location line="+29"/>
        <location line="+35"/>
        <source>Computer will shutdown in %n minute(s)</source>
        <translation><numerusform>Bilgisayar %n dakika içinde kapanacak</numerusform><numerusform>Bilgisayar %n dakika içinde kapanacak</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::JsonParser</name>
    <message>
        <location filename="../src/Components/Streaming/Soundcloud/SoundcloudJsonParser.cpp" line="+123"/>
        <source>Website</source>
        <translation>İnternet Sitesi</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+236"/>
        <source>Permalink Url</source>
        <translation>Kalıcı Adresi</translation>
    </message>
    <message>
        <location line="-223"/>
        <source>Followers/Following</source>
        <translation>Takipçiler/Takip Edilenler</translation>
    </message>
    <message>
        <location line="+93"/>
        <location line="+134"/>
        <source>Purchase Url</source>
        <translation>Satın Alma Adresi</translation>
    </message>
</context>
<context>
    <name>Gui::CoverButton</name>
    <message>
        <location filename="../src/Gui/Covers/CoverButton.cpp" line="+119"/>
        <source>Search an alternative cover</source>
        <translation>Alternatif bir kapak ara</translation>
    </message>
</context>
<context>
    <name>HistoryContainer</name>
    <message>
        <location filename="../src/Gui/History/HistoryContainer.cpp" line="+25"/>
        <source>History</source>
        <translation>Geçmiş</translation>
    </message>
</context>
<context>
    <name>GUI_InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.cpp" line="+92"/>
        <source>Write cover to tracks</source>
        <translation>Parçalara kapak yaz</translation>
    </message>
</context>
<context>
    <name>Library::CoverViewContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/CoverViewContextMenu.cpp" line="+217"/>
        <source>Toolbar</source>
        <translation>Araç çubuğu</translation>
    </message>
</context>
<context>
    <name>Library::GUI_CoverView</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/GUI_CoverView.cpp" line="+214"/>
        <source>Use Ctrl + mouse wheel to zoom</source>
        <translation>Yakınlaştırmak için Ctrl + fare tekerleğini kullanın</translation>
    </message>
</context>
<context>
    <name>Directory::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryContextMenu.cpp" line="+295"/>
        <source>Rename by metadata</source>
        <translation>Üst verilere göre adlandır</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Collapse all</source>
        <translation>Tümünü daralt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to another library</source>
        <translation>Başka bir kütüphaneye taşı</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to another library</source>
        <translation>Başka bir kütüphaneye kopyala</translation>
    </message>
</context>
<context>
    <name>Directory::TreeView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryTreeView.cpp" line="+410"/>
        <source>Copy here</source>
        <translation>Buraya kopyala</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move here</source>
        <translation>Buraya taşı</translation>
    </message>
</context>
<context>
    <name>GUI_DirectoryView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/GUI_DirectoryView.cpp" line="+183"/>
        <source>Could not create directory</source>
        <translation>Dizin oluşturulamadı</translation>
    </message>
</context>
<context>
    <name>Library::GenreView</name>
    <message>
        <location filename="../src/Gui/Library/GenreView.cpp" line="+114"/>
        <source>Updating genres</source>
        <translation>Türler güncelleniyor</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Do you really want to remove %1 from all tracks?</source>
        <translation>%1 ögesini tüm parçalardan kaldırmak istiyor musunuz?</translation>
    </message>
</context>
<context>
    <name>Library::GUI_EmptyLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_EmptyLibrary.cpp" line="+94"/>
        <source>Please choose a name for your library</source>
        <translation>Lütfen kütüphaneniz için bir isim seçin</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Please choose another name for your library</source>
        <translation>Lütfen kütüphaneniz için başka bir isim seçin</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>The file path is invalid</source>
        <translation>Dosya yolu geçersiz</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library with the same file path already exists</source>
        <translation>Aynı dosya yoluna sahip bir kütüphane zaten var</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library which contains this file path already exists</source>
        <translation>Bu dosya yolunu içeren bir kütüphane zaten var</translation>
    </message>
</context>
<context>
    <name>Library::HeaderView</name>
    <message>
        <location filename="../src/Gui/Library/Header/HeaderView.cpp" line="+198"/>
        <source>Resize columns</source>
        <translation>Sütunları boyutlandır</translation>
    </message>
</context>
<context>
    <name>Library::TrackModel</name>
    <message>
        <location filename="../src/Gui/Library/TableView/TrackModel.cpp" line="+144"/>
        <source>kBit/s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_DeleteDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_DeleteDialog.cpp" line="+87"/>
        <source>Only from library</source>
        <translation>Sadece kütüphaneden</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>You are about to delete %n file(s)</source>
        <translation><numerusform>%n dosyaları silmek üzeresiniz</numerusform><numerusform>%n dosyaları silmek üzeresiniz</numerusform></translation>
    </message>
</context>
<context>
    <name>Library::GUI_LibraryReloadDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_ReloadLibraryDialog.cpp" line="+78"/>
        <source>Fast scan</source>
        <translation>Hızlı tara</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deep scan</source>
        <translation>Ayrıntılı tara</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Only scan for new and deleted files</source>
        <translation>Sadece yeni ve silinen dosyaları tara</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scan all files in your library directory</source>
        <translation>Kütüphane dizinindeki tüm dosyaları tara</translation>
    </message>
</context>
<context>
    <name>Library::LocalLibraryMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/LocalLibraryMenu.cpp" line="+154"/>
        <source>Statistics</source>
        <translation>İstatistik</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit library</source>
        <translation>Kütüphaneyi düzenle</translation>
    </message>
</context>
<context>
    <name>Gui::MergeMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/MergeMenu.cpp" line="+52"/>
        <location line="+53"/>
        <source>Merge</source>
        <translation>Birleştir</translation>
    </message>
</context>
<context>
    <name>GUI_ControlsBase</name>
    <message>
        <location filename="../src/Gui/Player/GUI_ControlsBase.cpp" line="+71"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Written by %1</source>
        <translation>Yazan %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright</source>
        <translation>Telif hakkı</translation>
    </message>
</context>
<context>
    <name>GUI_Logger</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.cpp" line="+275"/>
        <source>Cannot open file</source>
        <translation>Dosya açılamıyor</translation>
    </message>
</context>
<context>
    <name>Menubar</name>
    <message>
        <location filename="../src/Gui/Player/GUI_PlayerMenubar.cpp" line="+323"/>
        <source>View</source>
        <translation>Görünüm</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+18"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Show large cover</source>
        <translation>Büyük kapak göster</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fullscreen</source>
        <translation>Tam ekran </translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Media files</source>
        <translation>Medya dosyaları</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Medya dosyalarını aç</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please visit the forum at</source>
        <translation>Lütfen forumu ziyaret edin</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>FAQ</source>
        <translation>SSS</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>About Sayonara</source>
        <translation>Sayonara Hakkında</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Written by %1</source>
        <translation>Yazan %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>License</source>
        <translation>Lisans</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Donate</source>
        <translation>Bağış</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Special thanks to all the brave translators</source>
        <translation>Çeviren: Serdar Sağlam</translation>
    </message>
</context>
<context>
    <name>TrayIconContextMenu</name>
    <message>
        <location filename="../src/Gui/Player/GUI_TrayIcon.cpp" line="+176"/>
        <source>Current song</source>
        <translation>Mevcut parça</translation>
    </message>
</context>
<context>
    <name>VersionChecker</name>
    <message>
        <location filename="../src/Gui/Player/VersionChecker.cpp" line="+77"/>
        <source>A new version is available!</source>
        <translation>Yeni bir sürüm mevcut!</translation>
    </message>
</context>
<context>
    <name>GUI_Playlist</name>
    <message>
        <location filename="../src/Gui/Playlist/GUI_Playlist.cpp" line="+241"/>
        <source>Playlist empty</source>
        <translation>Oynatma listesi boş</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Media files</source>
        <translation>Medya dosyaları</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Medya dosyalarını aç</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Playlist name already exists</source>
        <translation>Oynatma listesi adı zaten var</translation>
    </message>
</context>
<context>
    <name>Playlist::ActionMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistActionMenu.cpp" line="+193"/>
        <source>Please set library path first</source>
        <translation>Lütfen öncelikle kütüphane yolunu ayarlayın</translation>
    </message>
</context>
<context>
    <name>Playlist::BottomBar</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistBottomBar.cpp" line="+242"/>
        <source>Please set library path first</source>
        <translation>Lütfen öncelikle kütüphane yolunu ayarlayın</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel shutdown?</source>
        <translation>Kapatma iptal edilsin mi?</translation>
    </message>
</context>
<context>
    <name>Playlist::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistContextMenu.cpp" line="+171"/>
        <source>Jump to current track</source>
        <translation>Mevcut parçaya atla</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show track in library</source>
        <translation>Parçayı kütüphanede göster</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Playlist mode</source>
        <translation>Oynatma listesi kipi</translation>
    </message>
</context>
<context>
    <name>Playlist::Model</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistModel.cpp" line="+474"/>
        <source>Goto row</source>
        <translation>Satıra git</translation>
    </message>
</context>
<context>
    <name>GUI_Crossfader</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Crossfader.cpp" line="+75"/>
        <location line="+12"/>
        <source>Crossfader</source>
        <translation>Geçiş yumuşatıcı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 ve %2</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Crossfader does not work with Alsa</source>
        <translation>Geçiş yumuşatıcı Alsa ile birlikte çalışmıyor</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gapless playback does not work with Alsa</source>
        <translation>Aralıksız oynatma Alsa ile birlikte çalışmıyor</translation>
    </message>
</context>
<context>
    <name>GUI_Equalizer</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Equalizer.cpp" line="+137"/>
        <location line="+40"/>
        <source>Linked sliders</source>
        <translation>Bağlantılı kaydırıcılar</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Equalizer</source>
        <translation>Ekolayzer</translation>
    </message>
    <message>
        <location line="+248"/>
        <source>Name %1 not allowed</source>
        <translation>%1 adına izin verilmiyor</translation>
    </message>
</context>
<context>
    <name>GUI_LevelPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_LevelPainter.cpp" line="+157"/>
        <source>Level</source>
        <translation>Seviye</translation>
    </message>
</context>
<context>
    <name>GUI_SpectrogramPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_SpectrogramPainter.cpp" line="+77"/>
        <source>Spectrogram</source>
        <translation>Spektrogram</translation>
    </message>
</context>
<context>
    <name>GUI_Spectrum</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Spectrum.cpp" line="+150"/>
        <source>Spectrum</source>
        <translation>Spektrum</translation>
    </message>
</context>
<context>
    <name>GUI_StyleSettings</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_StyleSettings.cpp" line="+203"/>
        <source>There are some unsaved settings&lt;br /&gt;Save now?</source>
        <translation>Kaydedilmemiş bazı ayarlar var&lt;br /&gt;Şimdi kaydedilsin mi?</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Please specify a name</source>
        <translation>Lütfen bir isim belirtin</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Save changes?</source>
        <translation>Değişiklikler kaydedilsin mi?</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistChooser</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_PlaylistChooser.cpp" line="+91"/>
        <location line="+38"/>
        <source>No playlists found</source>
        <translation>Oynatma listesi bulunamadı</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Could not rename playlist</source>
        <translation>Oynatma listesi yeniden adlandırılamadı</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Name is invalid</source>
        <translation>Geçersiz isim</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Do you really want to delete %1?</source>
        <translation>%1 gerçekten silmek istiyor musunuz?</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not delete playlist %1</source>
        <translation>%1 oynatma listesi silinemedi</translation>
    </message>
</context>
<context>
    <name>Gui::AbstractStationPlugin</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/AbstractStationPlugin.cpp" line="+243"/>
        <source>Cannot open stream</source>
        <translation>Akış açılamıyor</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please choose another name</source>
        <translation>Lütfen başka bir isim seç</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Do you really want to delete %1</source>
        <translation>%1 gerçekten silmek istiyor musunuz</translation>
    </message>
</context>
<context>
    <name>GUI_Podcasts</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Podcasts.cpp" line="+65"/>
        <source>Podcast</source>
        <translation>Podcast</translation>
    </message>
</context>
<context>
    <name>GUI_Stream</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Stream.cpp" line="+68"/>
        <source>Search radio station</source>
        <translation>Radyo istasyonu ara</translation>
    </message>
</context>
<context>
    <name>GUI_EnginePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Engine/GUI_EnginePreferences.cpp" line="+53"/>
        <source>Audio</source>
        <translation>Ses</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutEntry</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutEntry.cpp" line="+55"/>
        <source>Enter shortcut</source>
        <translation>Kısayolu gir</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Shortcut already in use</source>
        <translation>Kısayol zaten kullanımda</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Test</source>
        <translation>Test</translation>
    </message>
</context>
<context>
    <name>SomaFM::StationModel</name>
    <message>
        <location filename="../src/Gui/SomaFM/SomaFMStationModel.cpp" line="+126"/>
        <source>Cannot fetch stations</source>
        <translation>İstasyonlar getirilemiyor</translation>
    </message>
</context>
<context>
    <name>SC::GUI_ArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.cpp" line="+79"/>
        <source>Query too short</source>
        <translation>Sorgu çok kısa</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>No artists found</source>
        <translation>Hiçbir sanatçı bulunamadı</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>Found %n artist(s)</source>
        <translation><numerusform>%n sanatçı bulundu</numerusform><numerusform>%n sanatçı bulundu</numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+56"/>
        <source>%n playlist(s) found</source>
        <translation><numerusform>%n oynatma listesi bulundu</numerusform><numerusform>%n oynatma listesi bulundu</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::GUI_Library</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.cpp" line="+56"/>
        <source>Add artist</source>
        <translation>Sanatçı ekle</translation>
    </message>
</context>
<context>
    <name>TagTextInput</name>
    <message>
        <location filename="../src/Gui/Tagging/TagTextInput.cpp" line="+126"/>
        <source>Very first letter to upper case</source>
        <translation>İlk harfler çok büyük</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First letters to upper case</source>
        <translation>İlk harfler büyük</translation>
    </message>
</context>
<context>
    <name>Library::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Utils/ContextMenu/LibraryContextMenu.cpp" line="+196"/>
        <source>Play in new tab</source>
        <translation>Yeni sekmede oynat</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Standard view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cover view</source>
        <translation>Kapak görünümü</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbar</source>
        <translation>Araç çubuğu</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>The toolbar is visible when there are tracks with differing file types listed in the track view</source>
        <translation>Parça görünümünde listelenen farklı dosya türlerine sahip parçalar olduğunda araç çubuğu görünür.</translation>
    </message>
</context>
<context>
    <name>Gui::ImageSelectionDialog</name>
    <message>
        <location filename="../src/Gui/Utils/ImageSelectionDialog.cpp" line="+57"/>
        <source>Image files</source>
        <translation>Resim dosyaları</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Any files</source>
        <translation>Tüm dosyalar</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open image files</source>
        <translation>Resim dosyalarını aç</translation>
    </message>
</context>
<context>
    <name>Gui::StreamRecorderPreferenceAction</name>
    <message>
        <location filename="../src/Gui/Utils/PreferenceAction.cpp" line="+163"/>
        <location line="+7"/>
        <source>Stream Recorder</source>
        <translation>Akış Kaydedici</translation>
    </message>
</context>
<context>
    <name>Gui::ShortcutPreferenceAction</name>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Shortcuts</source>
        <translation>Kısayollar</translation>
    </message>
</context>
<context>
    <name>Gui::MiniSearcher</name>
    <message>
        <location filename="../src/Gui/Utils/SearchableWidget/MiniSearcher.cpp" line="+92"/>
        <source>Arrow up</source>
        <translation>Yukarı ok</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Previous search result</source>
        <translation>Önceki arama sonucu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Arrow down</source>
        <translation>Aşağı ok</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Next search result</source>
        <translation>Sonraki arama sonucu</translation>
    </message>
</context>
<context>
    <name>Lang</name>
    <message>
        <location filename="../src/Utils/Language/Language.cpp" line="+79"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>Eylem</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Actions</source>
        <translation>Eylemler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Activate</source>
        <translation>Etkinleştir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>Etkin</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add</source>
        <translation>Ekle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add tab</source>
        <translation>Sekme ekle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album</source>
        <translation>Albüm</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artists</source>
        <translation>Albüm sanatçısı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Albums</source>
        <translation>Albümler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All</source>
        <translation>Tümü</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Append</source>
        <translation>Sona ekle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Application</source>
        <translation>Uygulama</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>Uygula</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artist</source>
        <translation>Sanatçı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artists</source>
        <translation>Sanatçılar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ascending</source>
        <translation>Yükselen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic</source>
        <translation>Otomatik</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bitrate</source>
        <translation>Bit hızı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bookmarks</source>
        <translation>Yer imi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Broadcast</source>
        <translation>Yayın</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>by</source>
        <translation>tarafından</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot find Lame MP3 encoder</source>
        <translation>Lame MP3 kodlayıcı bulunamıdı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>Temizle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear selection</source>
        <translation>Seçileni temizle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close others</source>
        <translation>Diğerlerini kapat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close tab</source>
        <translation>Sekmeyi kapat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment</source>
        <translation>Açıklama</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue</source>
        <translation>Devam</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Covers</source>
        <translation>Kapaklar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created</source>
        <translation>Oluşturuldu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create new directory</source>
        <translation>Yeni dizin oluştur</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create a new library</source>
        <translation>Yeni bir kütüphane oluştur</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark Mode</source>
        <translation>Koyu Kip</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date</source>
        <translation>Tarih</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Days</source>
        <translation>Gün</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>d</source>
        <translation>g</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Default</source>
        <translation>Varsayılan</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+84"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location line="-82"/>
        <source>Descending</source>
        <translation>Azalan</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory</source>
        <translation>Dizin</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directories</source>
        <translation>Dizinler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disc</source>
        <translation>Disk</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Duration</source>
        <translation>Süre</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dur.</source>
        <translation>Sür.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dynamic playback</source>
        <translation>Dinamik oynat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Empty input</source>
        <translation>Boş giriş</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter name</source>
        <translation>İsim gir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please enter new name</source>
        <translation>Lütfen yeni bir isim gir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter URL</source>
        <translation>URL gir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry</source>
        <translation>Girdi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entries</source>
        <translation>Girdiler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fast</source>
        <translation>Hızlı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation>Dosya</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filename</source>
        <translation>Dosya adı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Files</source>
        <translation>Dosyalar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filesize</source>
        <translation>Dosya boyutu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File type</source>
        <translation>Dosya tipi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filter</source>
        <translation>Süzgeç</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>1st</source>
        <translation>1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Font</source>
        <translation>Yazı tipi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation>Yazı tipleri</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fulltext</source>
        <translation>Tam metin</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Gapless playback</source>
        <translation>Aralıksız oynat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genre</source>
        <translation>Tür</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genres</source>
        <translation>Türler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hide</source>
        <translation>Gizle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hours</source>
        <translation>Saat</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>h</source>
        <translation>s</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import directory</source>
        <translation>Dizini içe aktar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import files</source>
        <translation>Dosyaları içeri aktar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation>Devre dışı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Info</source>
        <translation>Bilgi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading</source>
        <translation>Yükleniyor</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading %1</source>
        <translation>Yükleniyor %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid characters</source>
        <translation>Geçersiz karakterler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+f</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab</source>
        <translation>Sekme</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library</source>
        <translation>Kütüphane</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library path</source>
        <translation>Kütüphane yolu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library view type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Listen</source>
        <translation>Dinle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Live Search</source>
        <translation>Canlı Arama</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Logger</source>
        <translation>Günlükler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lyrics</source>
        <translation>Şarkı sözleri</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>Küçült</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minutes</source>
        <translation>Dakika</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>m</source>
        <translation>dk</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing</source>
        <translation>Eksik</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Modified</source>
        <translation>Düzenlendi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Months</source>
        <translation>Ay</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute</source>
        <translation>Sessiz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute off</source>
        <translation>Sesi aç</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation>Yeni</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next page</source>
        <translation>Sonraki sayfa</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next track</source>
        <translation>Sonraki parça</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation>Hayır</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No albums</source>
        <translation>Albüm yok</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+144"/>
        <source>Tracks</source>
        <translation>Parçalar</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Move down</source>
        <translation>Aşağı taşı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Move up</source>
        <translation>Yukarı taşı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>on</source>
        <translation>açık</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Aç</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open directory</source>
        <translation>Dizin aç</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open file</source>
        <translation>Dosya aç</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>or</source>
        <translation>veya</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Overwrite</source>
        <translation>Üzerine yaz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pause</source>
        <translation>Duraklat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play</source>
        <translation>Oynat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playing time</source>
        <translation>Oynatma süresi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play in new tab</source>
        <translation>Yeni sekmede oynat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlist</source>
        <translation>Oynatma listesi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlists</source>
        <translation>Oynatma listeleri</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play next</source>
        <translation>Sonrakini oynat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play/Pause</source>
        <translation>Oynat/Duraklat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugin</source>
        <translation>Eklenti</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Podcasts</source>
        <translation>Podcast&apos;ler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences</source>
        <translation>Tercihler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous page</source>
        <translation>Önceki sayfa</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous track</source>
        <translation>Önceki parça</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Purchase Url</source>
        <translation>Satın Alma Adresi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Quit</source>
        <translation>Çık</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio</source>
        <translation>Radyo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio Station</source>
        <translation>Radyo İstasyonu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rating</source>
        <translation>Puan</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Really</source>
        <translation>Gerçekten</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Refresh</source>
        <translation>Yenile</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload Library</source>
        <translation>Kütüphaneyi Yeniden Yükle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove</source>
        <translation>Kaldır</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename</source>
        <translation>Yeniden adlandır</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat 1</source>
        <translation>1 tekrar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat all</source>
        <translation>Tümünü tekrarla</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Replace</source>
        <translation>Değiştir</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation>Sıfırla</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation>Tekrar dene</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reverse order</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Sampler</source>
        <translation>Örnekleyici</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shuffle</source>
        <translation>Karıştır</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shutdown</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Kaydet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save as</source>
        <translation>Farklı kaydet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save to file</source>
        <translation>Dosyaya kaydet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan for audio files</source>
        <translation>Ses dosyalarını tara</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Search</source>
        <translation>Ara</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search next</source>
        <translation>Sonrakini ara</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search previous</source>
        <translation>Öncekini ara</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2nd</source>
        <translation>2.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seconds</source>
        <translation>Saniye</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>s</source>
        <translation>sn</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek backward</source>
        <translation>Geriye doğru ara</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek forward</source>
        <translation>İleriye doğru ara</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show</source>
        <translation>Göster</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Album Artists</source>
        <translation>Albüm Sanatçılarını Göster</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Covers</source>
        <translation>Kapakları Göster</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Library</source>
        <translation>Kütüphaneyi Göster</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Similar artists</source>
        <translation>Benzer sanatçılar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by</source>
        <translation>Sırala</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stop</source>
        <translation>Dur</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streams</source>
        <translation>Akışlar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream URL</source>
        <translation>Akış URL&apos;si</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Success</source>
        <translation>Başarı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>th</source>
        <translation>.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3rd</source>
        <translation>3.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Title</source>
        <translation>Başlık</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track</source>
        <translation>Parça</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track number</source>
        <translation>Parça numarası</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>track on</source>
        <translation>takip et</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Tree</source>
        <translation>Ağaç</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Undo</source>
        <translation>Geri al</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown album</source>
        <translation>Bilinmeyen albüm</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown artist</source>
        <translation>Bilinmeyen sanatçı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown title</source>
        <translation>Bilinmeyen başlık</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown genre</source>
        <translation>Bilinmeyen tür</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown placeholder</source>
        <translation>Bilinmeyen yer tutucu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown year</source>
        <translation>Bilinmeyen yıl</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Various</source>
        <translation>Çeşitli</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various albums</source>
        <translation>Çeşitli albümler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various artists</source>
        <translation>Çeşitli sanatçılar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various tracks</source>
        <translation>Çeşitli parçalar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Version</source>
        <translation>Sürüm</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume down</source>
        <translation>Sesi azalt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume up</source>
        <translation>Sesi yükselt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation>Uyarı</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Weeks</source>
        <translation>Hafta</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Year</source>
        <translation>Yıl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Years</source>
        <translation>Yıl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Evet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>Yakınlaştır</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>No directories</source>
        <translation>Dizin yok</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n directory(s)</source>
        <translation><numerusform>%n dizin</numerusform><numerusform>%n dizin</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No files</source>
        <translation>Dosya yok</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n file(s)</source>
        <translation><numerusform>%n dosya</numerusform><numerusform>%n dosya</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No playlists</source>
        <translation>Oynatma listesi yok</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n playlist(s)</source>
        <translation><numerusform>%n oynatma listesi</numerusform><numerusform>%n oynatma listesi</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks</source>
        <translation>Parça yok</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s)</source>
        <translation><numerusform>%n parça</numerusform><numerusform>%n parça</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks found</source>
        <translation>Parça bulunamadı</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s) found</source>
        <translation><numerusform>%n parça bulundu</numerusform><numerusform>%n parça bulundu</numerusform></translation>
    </message>
</context>
</TS>