<?xml version="1.0" ?><!DOCTYPE TS><TS language="en_GB" sourcelanguage="en" version="2.1">
<context>
    <name>GUI_AlternativeCovers</name>
    <message>
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.ui" line="+27"/>
        <source>Online Search</source>
        <translation>Online search</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Automatic search</source>
        <translation>Automatic search</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Text search</source>
        <translation>Text search</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Local Search</source>
        <translation>Local Search</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Select and preview multiple covers</source>
        <translation>Select and preview multiple covers</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Alternative Cover</source>
        <translation>Alternative Cover</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Start search automatically</source>
        <translation>Start search automatically</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/Gui/Covers/GUI_AlternativeCovers.cpp" line="+283"/>
        <source>%n cover(s) found</source>
        <translation><numerusform>1 cover found</numerusform><numerusform>%n covers found</numerusform></translation>
    </message>
    <message>
        <location line="+133"/>
        <location line="+31"/>
        <source>Also save cover to %1</source>
        <translation>Also save cover to %1</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Cover web search is not enabled</source>
        <translation>Cover web search is not enabled</translation>
    </message>
</context>
<context>
    <name>GUI_History</name>
    <message>
        <location filename="../src/Gui/History/GUI_History.ui" line="+14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Newer</source>
        <translation>Newer</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Older</source>
        <translation>Older</translation>
    </message>
</context>
<context>
    <name>InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.ui" line="+20"/>
        <source>Info / Edit</source>
        <translation>Info / Edit</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>Loading files...</source>
        <translation>Loading files...</translation>
    </message>
</context>
<context>
    <name>GUI_Lyrics</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.ui" line="+28"/>
        <source>Save Lyrics</source>
        <translation>Save Lyrics</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>switch</source>
        <translation>switch</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_Lyrics.cpp" line="+303"/>
        <source>Save lyrics not supported</source>
        <translation>Save lyrics not supported</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Overwrite lyrics</source>
        <translation>Overwrite lyrics</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save lyrics</source>
        <translation>Save lyrics</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
</context>
<context>
    <name>GUI_ImportDialog</name>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.ui" line="+14"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Import tracks to library</source>
        <translation>Import tracks to library</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select target folder</source>
        <translation>Select target folder</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_ImportDialog.cpp" line="+126"/>
        <source>Loading tracks</source>
        <translation>Loading tracks</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No tracks</source>
        <translation>No tracks</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Importing</source>
        <translation>Importing</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>Finished</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Rollback</source>
        <translation>Rollback</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cancelled</source>
        <translation>Cancelled</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Choose target directory</source>
        <translation>Choose target directory</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>%1&lt;br /&gt;is no library directory</source>
        <translation>%1&lt;br /&gt;is no library directory</translation>
    </message>
</context>
<context>
    <name>GUI_LocalLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.ui" line="+17"/>
        <source>Library</source>
        <translation>Library</translation>
    </message>
    <message>
        <location filename="../src/Gui/Library/GUI_LocalLibrary.cpp" line="+352"/>
        <source>Audio files</source>
        <translation>Audio files</translation>
    </message>
</context>
<context>
    <name>GUI_Controls</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Controls.ui" line="+318"/>
        <source>Written by Michael Lugmair (Lucio Carreras)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_Player</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Player.ui" line="+14"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
</context>
<context>
    <name>GUI_AudioConverter</name>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.ui" line="+45"/>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="+277"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>#Threads</source>
        <translation>#Threads</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+187"/>
        <source>Quality</source>
        <translation>Quality</translation>
    </message>
    <message>
        <location line="-164"/>
        <location line="+52"/>
        <location line="+89"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/AudioConverter/GUI_AudioConverter.cpp" line="-164"/>
        <source>Audio Converter</source>
        <translation>Audio Converter</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Threads</source>
        <translation>Threads</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+123"/>
        <source>Cannot find encoder</source>
        <translation>Cannot find encoder</translation>
    </message>
    <message>
        <location line="-113"/>
        <location line="+14"/>
        <source>Playlist does not contain tracks which are supported by the converter</source>
        <translation>Playlist does not contain tracks which are supported by the converter</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>No track will be converted.</source>
        <translation>No track will be converted.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>These tracks will be ignored</source>
        <translation>These tracks will be ignored</translation>
    </message>
    <message numerus="yes">
        <location line="+38"/>
        <source>Failed to convert %n track(s)</source>
        <translation><numerusform>Failed to convert 1 track</numerusform><numerusform>Failed to convert %n tracks</numerusform></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Please check the log files</source>
        <translation>Please check the log files</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>All tracks could be converted</source>
        <translation>All tracks could be converted</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully finished</source>
        <translation>Successfully finished</translation>
    </message>
</context>
<context>
    <name>GUI_Bookmarks</name>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.ui" line="+116"/>
        <source>Loop</source>
        <translation>Loop</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Bookmarks/GUI_Bookmarks.cpp" line="+77"/>
        <location line="+53"/>
        <source>No bookmarks found</source>
        <translation>No bookmarks found</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Sorry, bookmarks can only be set for library tracks at the moment.</source>
        <translation>Sorry, bookmarks can only be set for library tracks at the moment.</translation>
    </message>
</context>
<context>
    <name>GUI_Broadcast</name>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.ui" line="+88"/>
        <source>Cannot Broadcast</source>
        <translation>Cannot Broadcast</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Broadcasting/GUI_Broadcast.cpp" line="+104"/>
        <source>Dismiss</source>
        <translation>Dismiss</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dismiss all</source>
        <translation>Dismiss all</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%n listener(s)</source>
        <translation><numerusform>1 listener</numerusform><numerusform>%n listeners</numerusform></translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Cannot broadcast on port %1</source>
        <translation>Cannot broadcast on port %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maybe another application is using this port?</source>
        <translation>Maybe another application is using this port?</translation>
    </message>
</context>
<context>
    <name>GUI_Speed</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.ui" line="+48"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+134"/>
        <source>Speed</source>
        <translation>Speed</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Preserve pitch</source>
        <translation>Preserve pitch</translation>
    </message>
    <message>
        <location line="+30"/>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+1"/>
        <source>Pitch</source>
        <translation>Pitch</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Speed.cpp" line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 and %2</translation>
    </message>
    <message>
        <location line="+70"/>
        <location line="+1"/>
        <source>%1 not found</source>
        <translation>%1 not found</translation>
    </message>
</context>
<context>
    <name>GUI_Style</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Style.ui" line="+14"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Spectrum</source>
        <translation>Spectrum</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Vert. spacing</source>
        <translation>Vert. spacing</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+88"/>
        <source>Rect height</source>
        <translation>Rect height</translation>
    </message>
    <message>
        <location line="-68"/>
        <location line="+44"/>
        <source>Hor. spacing</source>
        <translation>Hor. spacing</translation>
    </message>
    <message>
        <location line="-30"/>
        <location line="+75"/>
        <source>Fading steps</source>
        <translation>Fading steps</translation>
    </message>
    <message>
        <location line="-51"/>
        <source>Level</source>
        <translation>Level</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Rect width</source>
        <translation>Rect width</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ver. spacing</source>
        <translation>Ver. spacing</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Style settings</source>
        <translation>Style settings</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Color 2</source>
        <translation>Color 2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 1</source>
        <translation>Color 1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Color 3</source>
        <translation>Color 3</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Color 4</source>
        <translation>Color 4</translation>
    </message>
</context>
<context>
    <name>GUI_TargetPlaylistDialog</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_TargetPlaylistDialog.ui" line="+14"/>
        <source>Choose target playlist</source>
        <translation>Choose target playlist</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>relative filepaths</source>
        <translation>relative filepaths</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save playlist as...</source>
        <translation>Save playlist as...</translation>
    </message>
</context>
<context>
    <name>GUI_StationSearcher</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.ui" line="+14"/>
        <source>Search Radio Station</source>
        <translation>Search Radio Station</translation>
    </message>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_StationSearcher.cpp" line="+58"/>
        <source>Show radio stations from %1 to %2</source>
        <translation>Show radio stations from %1 to %2</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Country</source>
        <translation>Country</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Url</source>
        <translation>Url</translation>
    </message>
</context>
<context>
    <name>GUI_BroadcastPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.ui" line="+20"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Ask for permission</source>
        <translation>Ask for permission</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Broadcast/GUI_BroadcastPreferences.cpp" line="+161"/>
        <source>Port %1 already in use</source>
        <translation>Port %1 already in use</translation>
    </message>
</context>
<context>
    <name>GUI_CoverPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Covers/GUI_CoverPreferences.ui" line="+62"/>
        <source>Inactive</source>
        <translation>Inactive</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Active</source>
        <translation>Active</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Delete all covers from the database</source>
        <translation>Delete all covers from the database</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear cache</source>
        <translation>Clear cache</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Delete all covers from the Sayonara directory</source>
        <translation>Delete all covers from the Sayonara directory</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete files</source>
        <translation>Delete files</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Save found covers into the library directory where the audio files are located</source>
        <translation>Save found covers into the library directory where the audio files are located</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers into library</source>
        <translation>Save found covers into library</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fetch missing covers from the internet</source>
        <translation>Fetch missing covers from the internet</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Saving covers to the database leads to significantly faster access but results in a bigger database</source>
        <translation>Saving covers to the database leads to significantly faster access but results in a bigger database</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save found covers to database</source>
        <translation>Save found covers to database</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Save found covers into Sayonara directory</source>
        <translation>Save found covers into Sayonara directory</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cover name</source>
        <translation>Cover name</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Name of cover file</source>
        <translation>Name of cover file</translation>
    </message>
</context>
<context>
    <name>GUI_LanguagePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.ui" line="+29"/>
        <source>Check for update</source>
        <translation>Check for update</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Download update</source>
        <translation>Download update</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>For new languages I am always looking for translators</source>
        <translation>For new languages I am always looking for translators</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Language/GUI_LanguagePreferences.cpp" line="+63"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Cannot check for language update</source>
        <translation>Cannot check for language update</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Language update available</source>
        <translation>Language update available</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Language is up to date</source>
        <translation>Language is up to date</translation>
    </message>
    <message>
        <location line="+41"/>
        <location line="+24"/>
        <source>Cannot fetch language update</source>
        <translation>Cannot fetch language update</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Language was updated successfully</source>
        <translation>Language was updated successfully</translation>
    </message>
</context>
<context>
    <name>GUI_LastFmPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.ui" line="+27"/>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Scrobble time</source>
        <translation>Scrobble time</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Login now</source>
        <translation>Login now</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/LastFM/GUI_LastFmPreferences.cpp" line="+163"/>
        <source>Logged in</source>
        <translation>Logged in</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Not logged in</source>
        <translation>Not logged in</translation>
    </message>
</context>
<context>
    <name>GUI_LibraryPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.ui" line="+27"/>
        <source>Libraries</source>
        <translation>Libraries</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Library-Playlist Interaction</source>
        <translation>Library-Playlist Interaction</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>When drag and drop into playlist </source>
        <translation>When drag and drop into playlist </translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+36"/>
        <source>do nothing (default)</source>
        <translation>do nothing (default)</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>start if stopped and playlist is empty</source>
        <translation>start if stopped and playlist is empty</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>On double click, create playlist and</source>
        <translation>On double click, create playlist and</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>start playback if stopped</source>
        <translation>start playback if stopped</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>start playback immediately</source>
        <translation>start playback immediately</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(this is ignored when playlist is in &apos;append mode&apos;)</source>
        <translation>(this is ignored when playlist is in &apos;append mode&apos;)</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Other</source>
        <translation>Other</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show &quot;Clear selection&quot; buttons</source>
        <translation>Show &quot;Clear selection&quot; buttons</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Ignore English article &quot;The&quot; in artist name</source>
        <translation>Ignore English article &quot;The&quot; in artist name</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Library/GUI_LibraryPreferences.cpp" line="+147"/>
        <source>Cannot edit library</source>
        <translation>Cannot edit library</translation>
    </message>
</context>
<context>
    <name>GUI_NotificationPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.ui" line="+17"/>
        <source>Timeout (ms)</source>
        <translation>Timeout (ms)</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Notifications/GUI_NotificationPreferences.cpp" line="+98"/>
        <source>Notifications</source>
        <translation>Notifications</translation>
    </message>
</context>
<context>
    <name>GUI_PlayerPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.ui" line="+44"/>
        <source>Show system tray icon</source>
        <translation>Show system tray icon</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Hide instead of close</source>
        <translation>Hide instead of close</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Start hidden</source>
        <translation>Start hidden</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Update notifications</source>
        <translation>Update notifications</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Player/GUI_PlayerPreferences.cpp" line="+107"/>
        <source>This might cause Sayonara not to show up again.</source>
        <translation>This might cause Sayonara not to show up again.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>In this case use the &apos;--show&apos; option at the next startup.</source>
        <translation>In this case use the &apos;--show&apos; option at the next startup.</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.ui" line="+36"/>
        <source>Behavior</source>
        <translation>Behavior</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Start up</source>
        <translation>Start up</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load temporary playlists</source>
        <translation>Load temporary playlists</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load saved playlists</source>
        <translation>Load saved playlists</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Start playing</source>
        <translation>Start playing</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Load last track on startup</source>
        <translation>Load last track on startup</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remember time of last track</source>
        <translation>Remember time of last track</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Stop behaviour</source>
        <translation>Stop behaviour</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load last track after pressing stop</source>
        <translation>Load last track after pressing stop</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Look</source>
        <translation>Look</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show numbers</source>
        <translation>Show numbers</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>&apos;italic text&apos;</source>
        <translation>&apos;italic text&apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Example</source>
        <translation>Example</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>*bold text*</source>
        <translation>*bold text*</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Show clear button</source>
        <translation>Show clear button</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Show covers</source>
        <translation>Show covers</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show rating</source>
        <translation>Show rating</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Playlist/GUI_PlaylistPreferences.cpp" line="+216"/>
        <source>Playlist look: Invalid expression</source>
        <translation>Playlist look: Invalid expression</translation>
    </message>
</context>
<context>
    <name>GUI_PreferenceDialog</name>
    <message>
        <location filename="../src/Gui/Preferences/PreferenceDialog/GUI_PreferenceDialog.ui" line="+136"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>GUI_ProxyPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.ui" line="+20"/>
        <source>Host</source>
        <translation>Host</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Save username/password</source>
        <translation>Save username/password</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Active</source>
        <translation>Active</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Automatic search</source>
        <translation>Automatic search</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Proxy/GUI_ProxyPreferences.cpp" line="+57"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
</context>
<context>
    <name>GUI_RemoteControlPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.ui" line="+36"/>
        <source>Detectable via UDP</source>
        <translation>Detectable via UDP</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Remote control URL</source>
        <translation>Remote control URL</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>UDP port</source>
        <translation>UDP port</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/RemoteControl/GUI_RemoteControlPreferences.cpp" line="+59"/>
        <source>If activated, Sayonara will answer an UDP request that it is remote controlable</source>
        <translation>If activated, Sayonara will answer an UDP request that it is remote controlable</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Remote control</source>
        <translation>Remote control</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Port %1 already in use</source>
        <translation>Port %1 already in use</translation>
    </message>
</context>
<context>
    <name>GUI_SearchPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Search/GUI_SearchPreferences.ui" line="+23"/>
        <source>Example</source>
        <translation>Example</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Case insensitive</source>
        <translation>Case insensitive</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Option</source>
        <translation>Option</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Ignore accents</source>
        <translation>Ignore accents</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Ignore special characters</source>
        <translation>Ignore special characters</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.ui" line="+44"/>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="+149"/>
        <source>Press shortcut</source>
        <translation>Press shortcut</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutPreferences.cpp" line="-48"/>
        <source>Shortcuts</source>
        <translation>Shortcuts</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Double shortcuts found</source>
        <translation>Double shortcuts found</translation>
    </message>
</context>
<context>
    <name>GUI_StreamRecorderPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.ui" line="+30"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Target directory</source>
        <translation>Target directory</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Automatic recording</source>
        <translation>Automatic recording</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Create session directory</source>
        <translation>Create session directory</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Directory</source>
        <translation>Session Directory</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Choose available placeholders</source>
        <translation>Choose available placeholders</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Path template</source>
        <translation>Path template</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Example</source>
        <translation>Example</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/StreamRecorder/GUI_StreamRecorderPreferences.cpp" line="+172"/>
        <source>Choose target directory</source>
        <translation>Choose target directory</translation>
    </message>
    <message>
        <location line="+65"/>
        <source>Target directory is empty</source>
        <translation>Target directory is empty</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Please choose another directory</source>
        <translation>Please choose another directory</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cannot create %1</source>
        <translation>Cannot create %1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Template path is not valid</source>
        <translation>Template path is not valid</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stream recorder</source>
        <translation>Stream recorder</translation>
    </message>
</context>
<context>
    <name>GUI_StreamPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.ui" line="+17"/>
        <source> ms</source>
        <translation> ms</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Buffer size</source>
        <translation>Buffer size</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show history</source>
        <translation>Show history</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Streams in new tab</source>
        <translation>Open Streams in new tab</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/Streams/GUI_StreamPreferences.cpp" line="+57"/>
        <source>%1 and %2</source>
        <translation>%1 and %2</translation>
    </message>
</context>
<context>
    <name>GUI_CssEditor</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_CssEditor.ui" line="+97"/>
        <source>Edit style sheet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Dark mode</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_FontPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.ui" line="+84"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>Font size</source>
        <translation>Font size</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bold</source>
        <translation>Bold</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Font name</source>
        <translation>Font name</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_FontPreferences.cpp" line="+129"/>
        <location line="+1"/>
        <source>Inherit</source>
        <translation>Inherit</translation>
    </message>
</context>
<context>
    <name>GUI_IconPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.ui" line="+20"/>
        <source>Also apply this icon theme to the dark style</source>
        <translation>Also apply this icon theme to the dark style</translation>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_IconPreferences.cpp" line="+96"/>
        <location line="+104"/>
        <source>System theme</source>
        <translation>System theme</translation>
    </message>
    <message>
        <location line="-96"/>
        <source>Icons</source>
        <translation>Icons</translation>
    </message>
</context>
<context>
    <name>GUI_UiPreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.ui" line="+57"/>
        <source>Fading cover</source>
        <translation>Fading cover</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Edit style sheet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/Gui/Preferences/UiPreferences/GUI_UiPreferences.cpp" line="+48"/>
        <source>User Interface</source>
        <translation>User Interface</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show large cover</source>
        <translation>Show large cover</translation>
    </message>
</context>
<context>
    <name>GUI_Shutdown</name>
    <message>
        <location filename="../src/Gui/Shutdown/GUI_Shutdown.ui" line="+14"/>
        <location line="+69"/>
        <source>Shutdown</source>
        <translation>Shutdown</translation>
    </message>
    <message>
        <location line="-56"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>minutes</source>
        <translation>minutes</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Shutdown after playlist finished</source>
        <translation>Shutdown after playlist finished</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Shutdown after</source>
        <translation>Shutdown after</translation>
    </message>
</context>
<context>
    <name>GUI_SomaFM</name>
    <message>
        <location filename="../src/Gui/SomaFM/GUI_SomaFM.ui" line="+42"/>
        <source>Donate to Soma.fm</source>
        <translation>Donate to Soma.fm</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Streams</source>
        <translation>Streams</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.ui" line="+14"/>
        <source>Search Soundcloud</source>
        <translation>Search Soundcloud</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Search artist</source>
        <translation>Search artist</translation>
    </message>
</context>
<context>
    <name>GUI_SoundcloudLibrary</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.ui" line="+26"/>
        <source>Library</source>
        <translation>Library</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Search for title, interprets and albums</source>
        <translation>Search for title, interprets and albums</translation>
    </message>
</context>
<context>
    <name>GUI_CoverEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.ui" line="+57"/>
        <source>Replace</source>
        <translation>Replace</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="+223"/>
        <source>Original</source>
        <translation>Original</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_CoverEdit.cpp" line="-56"/>
        <location line="+9"/>
        <source>File has no cover</source>
        <translation>File has no cover</translation>
    </message>
</context>
<context>
    <name>GUI_FailMessageBox</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.ui" line="+22"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_FailMessageBox.cpp" line="+55"/>
        <source>File exists</source>
        <translation>File exists</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Writeable</source>
        <translation>Writeable</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Some files could not be saved</source>
        <translation>Some files could not be saved</translation>
    </message>
</context>
<context>
    <name>GUI_TagEdit</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.ui" line="+115"/>
        <source>Discnumber</source>
        <translation>Discnumber</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Album artist</source>
        <translation>Album artist</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Comment</source>
        <translation>Comment</translation>
    </message>
    <message>
        <location line="+150"/>
        <source>all</source>
        <translation>all</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Tag from path</source>
        <translation>Tag from path</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Undo all</source>
        <translation>Undo all</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Read only file</source>
        <translation>Read only file</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagEdit.cpp" line="+161"/>
        <source>Load complete album</source>
        <translation>Load complete album</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Metadata</source>
        <translation>Metadata</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tags from path</source>
        <translation>Tags from path</translation>
    </message>
    <message numerus="yes">
        <location line="+81"/>
        <source>Cannot apply expression to %n track(s)</source>
        <translation><numerusform>Cannot apply expression to 1 track</numerusform><numerusform>Cannot apply expression to %n tracks</numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore these tracks?</source>
        <translation>Ignore these tracks?</translation>
    </message>
    <message>
        <location line="+394"/>
        <source>All changes will be lost</source>
        <translation>All changes will be lost</translation>
    </message>
</context>
<context>
    <name>GUI_TagFromPath</name>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.ui" line="+17"/>
        <source>Expression</source>
        <translation>Expression</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Disc Nr</source>
        <translation>Disc Nr</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Apply to all</source>
        <translation>Apply to all</translation>
    </message>
    <message>
        <location filename="../src/Gui/Tagging/GUI_TagFromPath.cpp" line="+92"/>
        <source>Tag</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Please select text first</source>
        <translation>Please select text first</translation>
    </message>
</context>
<context>
    <name>StreamServer</name>
    <message>
        <location filename="../src/Components/Broadcasting/StreamServer.cpp" line="+228"/>
        <source>%1 wants to listen to your music.</source>
        <translation>%1 wants to listen to your music.</translation>
    </message>
</context>
<context>
    <name>AbstractLibrary</name>
    <message>
        <location filename="../src/Components/Library/AbstractLibrary.cpp" line="+849"/>
        <source>All %1 could be removed</source>
        <translation>All %1 could be removed</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 %3 could not be removed</source>
        <translation>%1 of %2 %3 could not be removed</translation>
    </message>
</context>
<context>
    <name>Library::Importer</name>
    <message>
        <location filename="../src/Components/Library/Importer/LibraryImporter.cpp" line="+228"/>
        <source>Cannot import tracks</source>
        <translation>Cannot import tracks</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>All files could be imported</source>
        <translation>All files could be imported</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of %2 files could be imported</source>
        <translation>%1 of %2 files could be imported</translation>
    </message>
</context>
<context>
    <name>Library::ReloadThread</name>
    <message>
        <location filename="../src/Components/Library/Threads/ReloadThread.cpp" line="+219"/>
        <source>Looking for covers</source>
        <translation>Looking for covers</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Reading files</source>
        <translation>Reading files</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Deleting orphaned tracks</source>
        <translation>Deleting orphaned tracks</translation>
    </message>
</context>
<context>
    <name>Lyrics::LookupThread</name>
    <message>
        <location filename="../src/Components/Lyrics/LyricLookup.cpp" line="+171"/>
        <location line="+35"/>
        <source>Cannot fetch lyrics from %1</source>
        <translation>Cannot fetch lyrics from %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No lyrics found</source>
        <translation>No lyrics found</translation>
    </message>
</context>
<context>
    <name>Shutdown</name>
    <message>
        <location filename="../src/Components/Shutdown/Shutdown.cpp" line="+100"/>
        <source>Computer will shutdown after playlist has finished</source>
        <translation>Computer will shutdown after playlist has finished</translation>
    </message>
    <message numerus="yes">
        <location line="+29"/>
        <location line="+35"/>
        <source>Computer will shutdown in %n minute(s)</source>
        <translation><numerusform>Computer will shutdown in 1 minute</numerusform><numerusform>Computer will shutdown in %n minutes</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::JsonParser</name>
    <message>
        <location filename="../src/Components/Streaming/Soundcloud/SoundcloudJsonParser.cpp" line="+123"/>
        <source>Website</source>
        <translation>Website</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+236"/>
        <source>Permalink Url</source>
        <translation>Permalink Url</translation>
    </message>
    <message>
        <location line="-223"/>
        <source>Followers/Following</source>
        <translation>Followers/Following</translation>
    </message>
    <message>
        <location line="+93"/>
        <location line="+134"/>
        <source>Purchase Url</source>
        <translation>Purchase Url</translation>
    </message>
</context>
<context>
    <name>Gui::CoverButton</name>
    <message>
        <location filename="../src/Gui/Covers/CoverButton.cpp" line="+119"/>
        <source>Search an alternative cover</source>
        <translation>Search an alternative cover</translation>
    </message>
</context>
<context>
    <name>HistoryContainer</name>
    <message>
        <location filename="../src/Gui/History/HistoryContainer.cpp" line="+25"/>
        <source>History</source>
        <translation>History</translation>
    </message>
</context>
<context>
    <name>GUI_InfoDialog</name>
    <message>
        <location filename="../src/Gui/InfoDialog/GUI_InfoDialog.cpp" line="+92"/>
        <source>Write cover to tracks</source>
        <translation>Write cover to tracks</translation>
    </message>
</context>
<context>
    <name>Library::CoverViewContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/CoverViewContextMenu.cpp" line="+217"/>
        <source>Toolbar</source>
        <translation>Toolbar</translation>
    </message>
</context>
<context>
    <name>Library::GUI_CoverView</name>
    <message>
        <location filename="../src/Gui/Library/CoverView/GUI_CoverView.cpp" line="+214"/>
        <source>Use Ctrl + mouse wheel to zoom</source>
        <translation>Use Ctrl + mouse wheel to zoom</translation>
    </message>
</context>
<context>
    <name>Directory::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryContextMenu.cpp" line="+295"/>
        <source>Rename by metadata</source>
        <translation>Rename by metadata</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Collapse all</source>
        <translation>Collapse all</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move to another library</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to another library</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Directory::TreeView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/DirectoryTreeView.cpp" line="+410"/>
        <source>Copy here</source>
        <translation>Copy here</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move here</source>
        <translation>Move here</translation>
    </message>
</context>
<context>
    <name>GUI_DirectoryView</name>
    <message>
        <location filename="../src/Gui/Library/DirectoryView/GUI_DirectoryView.cpp" line="+183"/>
        <source>Could not create directory</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::GenreView</name>
    <message>
        <location filename="../src/Gui/Library/GenreView.cpp" line="+114"/>
        <source>Updating genres</source>
        <translation>Updating genres</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Do you really want to remove %1 from all tracks?</source>
        <translation>Do you really want to remove %1 from all tracks?</translation>
    </message>
</context>
<context>
    <name>Library::GUI_EmptyLibrary</name>
    <message>
        <location filename="../src/Gui/Library/GUI_EmptyLibrary.cpp" line="+94"/>
        <source>Please choose a name for your library</source>
        <translation>Please choose a name for your library</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Please choose another name for your library</source>
        <translation>Please choose another name for your library</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>The file path is invalid</source>
        <translation>The file path is invalid</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library with the same file path already exists</source>
        <translation>A library with the same file path already exists</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A library which contains this file path already exists</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Library::HeaderView</name>
    <message>
        <location filename="../src/Gui/Library/Header/HeaderView.cpp" line="+198"/>
        <source>Resize columns</source>
        <translation>Resize columns</translation>
    </message>
</context>
<context>
    <name>Library::TrackModel</name>
    <message>
        <location filename="../src/Gui/Library/TableView/TrackModel.cpp" line="+144"/>
        <source>kBit/s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GUI_DeleteDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_DeleteDialog.cpp" line="+87"/>
        <source>Only from library</source>
        <translation>Only from library</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>You are about to delete %n file(s)</source>
        <translation><numerusform>You are about to delete 1 file</numerusform><numerusform>You are about to delete %n files</numerusform></translation>
    </message>
</context>
<context>
    <name>Library::GUI_LibraryReloadDialog</name>
    <message>
        <location filename="../src/Gui/Library/Utils/GUI_ReloadLibraryDialog.cpp" line="+78"/>
        <source>Fast scan</source>
        <translation>Fast scan</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Deep scan</source>
        <translation>Deep scan</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Only scan for new and deleted files</source>
        <translation>Only scan for new and deleted files</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Scan all files in your library directory</source>
        <translation>Scan all files in your library directory</translation>
    </message>
</context>
<context>
    <name>Library::LocalLibraryMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/LocalLibraryMenu.cpp" line="+154"/>
        <source>Statistics</source>
        <translation>Statistics</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit library</source>
        <translation>Edit library</translation>
    </message>
</context>
<context>
    <name>Gui::MergeMenu</name>
    <message>
        <location filename="../src/Gui/Library/Utils/MergeMenu.cpp" line="+52"/>
        <location line="+53"/>
        <source>Merge</source>
        <translation>Merge</translation>
    </message>
</context>
<context>
    <name>GUI_ControlsBase</name>
    <message>
        <location filename="../src/Gui/Player/GUI_ControlsBase.cpp" line="+71"/>
        <source>Sayonara Player</source>
        <translation>Sayonara Player</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
</context>
<context>
    <name>GUI_Logger</name>
    <message>
        <location filename="../src/Gui/Player/GUI_Logger.cpp" line="+275"/>
        <source>Cannot open file</source>
        <translation>Cannot open file</translation>
    </message>
</context>
<context>
    <name>Menubar</name>
    <message>
        <location filename="../src/Gui/Player/GUI_PlayerMenubar.cpp" line="+323"/>
        <source>View</source>
        <translation>View</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+18"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location line="-17"/>
        <source>Plugins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Show large cover</source>
        <translation>Show large cover</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fullscreen</source>
        <translation>Fullscreen</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Media files</source>
        <translation>Media files</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Open Media files</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please visit the forum at</source>
        <translation>Please visit the forum at</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>FAQ</source>
        <translation>FAQ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>About Sayonara</source>
        <translation>About Sayonara</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Written by %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>License</source>
        <translation>License</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Donate</source>
        <translation>Donate</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Special thanks to all the brave translators</source>
        <translation>Special thanks to all the brave translators</translation>
    </message>
</context>
<context>
    <name>TrayIconContextMenu</name>
    <message>
        <location filename="../src/Gui/Player/GUI_TrayIcon.cpp" line="+176"/>
        <source>Current song</source>
        <translation>Current song</translation>
    </message>
</context>
<context>
    <name>VersionChecker</name>
    <message>
        <location filename="../src/Gui/Player/VersionChecker.cpp" line="+77"/>
        <source>A new version is available!</source>
        <translation>A new version is available!</translation>
    </message>
</context>
<context>
    <name>GUI_Playlist</name>
    <message>
        <location filename="../src/Gui/Playlist/GUI_Playlist.cpp" line="+241"/>
        <source>Playlist empty</source>
        <translation>Playlist empty</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Media files</source>
        <translation>Media files</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open Media files</source>
        <translation>Open Media files</translation>
    </message>
    <message>
        <location line="+208"/>
        <source>Playlist name already exists</source>
        <translation>Playlist name already exists</translation>
    </message>
</context>
<context>
    <name>Playlist::ActionMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistActionMenu.cpp" line="+193"/>
        <source>Please set library path first</source>
        <translation>Please set library path first</translation>
    </message>
</context>
<context>
    <name>Playlist::BottomBar</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistBottomBar.cpp" line="+242"/>
        <source>Please set library path first</source>
        <translation>Please set library path first</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Cancel shutdown?</source>
        <translation>Cancel shutdown?</translation>
    </message>
</context>
<context>
    <name>Playlist::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistContextMenu.cpp" line="+171"/>
        <source>Jump to current track</source>
        <translation>Jump to current track</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show track in library</source>
        <translation>Show track in library</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Playlist mode</source>
        <translation>Playlist mode</translation>
    </message>
</context>
<context>
    <name>Playlist::Model</name>
    <message>
        <location filename="../src/Gui/Playlist/PlaylistModel.cpp" line="+474"/>
        <source>Goto row</source>
        <translation>Goto row</translation>
    </message>
</context>
<context>
    <name>GUI_Crossfader</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Crossfader.cpp" line="+75"/>
        <location line="+12"/>
        <source>Crossfader</source>
        <translation>Crossfader</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 and %2</source>
        <translation>%1 and %2</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Crossfader does not work with Alsa</source>
        <translation>Crossfader does not work with Alsa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gapless playback does not work with Alsa</source>
        <translation>Gapless playback does not work with Alsa</translation>
    </message>
</context>
<context>
    <name>GUI_Equalizer</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Equalizer.cpp" line="+137"/>
        <location line="+40"/>
        <source>Linked sliders</source>
        <translation>Linked sliders</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Equalizer</source>
        <translation>Equalizer</translation>
    </message>
    <message>
        <location line="+248"/>
        <source>Name %1 not allowed</source>
        <translation>Name %1 not allowed</translation>
    </message>
</context>
<context>
    <name>GUI_LevelPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_LevelPainter.cpp" line="+157"/>
        <source>Level</source>
        <translation>Level</translation>
    </message>
</context>
<context>
    <name>GUI_SpectrogramPainter</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_SpectrogramPainter.cpp" line="+77"/>
        <source>Spectrogram</source>
        <translation>Spectrogram</translation>
    </message>
</context>
<context>
    <name>GUI_Spectrum</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_Spectrum.cpp" line="+150"/>
        <source>Spectrum</source>
        <translation>Spectrum</translation>
    </message>
</context>
<context>
    <name>GUI_StyleSettings</name>
    <message>
        <location filename="../src/Gui/Plugins/Engine/GUI_StyleSettings.cpp" line="+203"/>
        <source>There are some unsaved settings&lt;br /&gt;Save now?</source>
        <translation>There are some unsaved settings&lt;br /&gt;Save now?</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Please specify a name</source>
        <translation>Please specify a name</translation>
    </message>
    <message>
        <location line="+168"/>
        <source>Save changes?</source>
        <translation>Save changes?</translation>
    </message>
</context>
<context>
    <name>GUI_PlaylistChooser</name>
    <message>
        <location filename="../src/Gui/Plugins/PlaylistChooser/GUI_PlaylistChooser.cpp" line="+91"/>
        <location line="+38"/>
        <source>No playlists found</source>
        <translation>No playlists found</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Could not rename playlist</source>
        <translation>Could not rename playlist</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Name is invalid</source>
        <translation>Name is invalid</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Do you really want to delete %1?</source>
        <translation>Do you really want to delete %1?</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not delete playlist %1</source>
        <translation>Could not delete playlist %1</translation>
    </message>
</context>
<context>
    <name>Gui::AbstractStationPlugin</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/AbstractStationPlugin.cpp" line="+243"/>
        <source>Cannot open stream</source>
        <translation>Cannot open stream</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Please choose another name</source>
        <translation>Please choose another name</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Do you really want to delete %1</source>
        <translation>Do you really want to delete %1</translation>
    </message>
</context>
<context>
    <name>GUI_Podcasts</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Podcasts.cpp" line="+65"/>
        <source>Podcast</source>
        <translation>Podcast</translation>
    </message>
</context>
<context>
    <name>GUI_Stream</name>
    <message>
        <location filename="../src/Gui/Plugins/Stream/GUI_Stream.cpp" line="+68"/>
        <source>Search radio station</source>
        <translation>Search radio station</translation>
    </message>
</context>
<context>
    <name>GUI_EnginePreferences</name>
    <message>
        <location filename="../src/Gui/Preferences/Engine/GUI_EnginePreferences.cpp" line="+53"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
</context>
<context>
    <name>GUI_ShortcutEntry</name>
    <message>
        <location filename="../src/Gui/Preferences/Shortcuts/GUI_ShortcutEntry.cpp" line="+55"/>
        <source>Enter shortcut</source>
        <translation>Enter shortcut</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Shortcut already in use</source>
        <translation>Shortcut already in use</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Test</source>
        <translation>Test</translation>
    </message>
</context>
<context>
    <name>SomaFM::StationModel</name>
    <message>
        <location filename="../src/Gui/SomaFM/SomaFMStationModel.cpp" line="+126"/>
        <source>Cannot fetch stations</source>
        <translation>Cannot fetch stations</translation>
    </message>
</context>
<context>
    <name>SC::GUI_ArtistSearch</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudArtistSearch.cpp" line="+79"/>
        <source>Query too short</source>
        <translation>Query too short</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>No artists found</source>
        <translation>No artists found</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>Found %n artist(s)</source>
        <translation><numerusform>Found 1 artist</numerusform><numerusform>Found %n artists</numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+56"/>
        <source>%n playlist(s) found</source>
        <translation><numerusform>1 playlist found</numerusform><numerusform>%n playlists found</numerusform></translation>
    </message>
</context>
<context>
    <name>SC::GUI_Library</name>
    <message>
        <location filename="../src/Gui/Soundcloud/GUI_SoundcloudLibrary.cpp" line="+56"/>
        <source>Add artist</source>
        <translation>Add artist</translation>
    </message>
</context>
<context>
    <name>TagTextInput</name>
    <message>
        <location filename="../src/Gui/Tagging/TagTextInput.cpp" line="+126"/>
        <source>Very first letter to upper case</source>
        <translation>Very first letter to upper case</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First letters to upper case</source>
        <translation>First letters to upper case</translation>
    </message>
</context>
<context>
    <name>Library::ContextMenu</name>
    <message>
        <location filename="../src/Gui/Utils/ContextMenu/LibraryContextMenu.cpp" line="+196"/>
        <source>Play in new tab</source>
        <translation>Play in new tab</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Standard view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cover view</source>
        <translation>Cover view</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Directory view</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbar</source>
        <translation>Toolbar</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>The toolbar is visible when there are tracks with differing file types listed in the track view</source>
        <translation>The toolbar is visible when there are tracks with differing file types listed in the track view</translation>
    </message>
</context>
<context>
    <name>Gui::ImageSelectionDialog</name>
    <message>
        <location filename="../src/Gui/Utils/ImageSelectionDialog.cpp" line="+57"/>
        <source>Image files</source>
        <translation>Image files</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Any files</source>
        <translation>Any files</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open image files</source>
        <translation>Open image files</translation>
    </message>
</context>
<context>
    <name>Gui::StreamRecorderPreferenceAction</name>
    <message>
        <location filename="../src/Gui/Utils/PreferenceAction.cpp" line="+163"/>
        <location line="+7"/>
        <source>Stream Recorder</source>
        <translation>Stream Recorder</translation>
    </message>
</context>
<context>
    <name>Gui::ShortcutPreferenceAction</name>
    <message>
        <location line="+10"/>
        <location line="+12"/>
        <source>Shortcuts</source>
        <translation>Shortcuts</translation>
    </message>
</context>
<context>
    <name>Gui::MiniSearcher</name>
    <message>
        <location filename="../src/Gui/Utils/SearchableWidget/MiniSearcher.cpp" line="+92"/>
        <source>Arrow up</source>
        <translation>Arrow up</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Previous search result</source>
        <translation>Previous search result</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Arrow down</source>
        <translation>Arrow down</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Next search result</source>
        <translation>Next search result</translation>
    </message>
</context>
<context>
    <name>Lang</name>
    <message>
        <location filename="../src/Utils/Language/Language.cpp" line="+79"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Action</source>
        <translation>Action</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Activate</source>
        <translation>Activate</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>Active</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add tab</source>
        <translation>Add tab</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Album artists</source>
        <translation>Album artists</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Albums</source>
        <translation>Albums</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All</source>
        <translation>All</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Append</source>
        <translation>Append</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Application</source>
        <translation>Application</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artist</source>
        <translation>Artist</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Artists</source>
        <translation>Artists</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ascending</source>
        <translation>Ascending</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Automatic</source>
        <translation>Automatic</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bitrate</source>
        <translation>Bitrate</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bookmarks</source>
        <translation>Bookmarks</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Broadcast</source>
        <translation>Broadcast</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>by</source>
        <translation>by</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot find Lame MP3 encoder</source>
        <translation>Cannot find Lame MP3 encoder</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear selection</source>
        <translation>Clear selection</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close others</source>
        <translation>Close others</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close tab</source>
        <translation>Close tab</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comment</source>
        <translation>Comment</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue</source>
        <translation>Continue</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Covers</source>
        <translation>Covers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created</source>
        <translation>Created</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Create new directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Create a new library</source>
        <translation>Create a new library</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark Mode</source>
        <translation>Dark Mode</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Days</source>
        <translation>Days</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>d</source>
        <translation>d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Default</source>
        <translation>Default</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+84"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location line="-82"/>
        <source>Descending</source>
        <translation>Descending</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory</source>
        <translation>Directory</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Directories</source>
        <translation>Directories</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disc</source>
        <translation>Disc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Duration</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Dur.</source>
        <translation>Dur.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dynamic playback</source>
        <translation>Dynamic playback</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit</source>
        <translation>Edit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Empty input</source>
        <translation>Empty input</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter name</source>
        <translation>Enter name</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please enter new name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter URL</source>
        <translation>Enter URL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entry</source>
        <translation>Entry</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Entries</source>
        <translation>Entries</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fast</source>
        <translation>Fast</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filename</source>
        <translation>Filename</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Files</source>
        <translation>Files</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filesize</source>
        <translation>Filesize</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>File type</source>
        <translation>File type</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>1st</source>
        <translation>1st</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fonts</source>
        <translation>Fonts</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fulltext</source>
        <translation>Fulltext</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Gapless playback</source>
        <translation>Gapless playback</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genre</source>
        <translation>Genre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Genres</source>
        <translation>Genres</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hide</source>
        <translation>Hide</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hours</source>
        <translation>Hours</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import directory</source>
        <translation>Import directory</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Import files</source>
        <translation>Import files</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation>Inactive</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading</source>
        <translation>Loading</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading %1</source>
        <translation>Loading %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid characters</source>
        <translation>Invalid characters</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+f</source>
        <translation>Ctrl+f</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library</source>
        <translation>Library</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library path</source>
        <translation>Library path</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Library view type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Listen</source>
        <translation>Listen</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Live Search</source>
        <translation>Live Search</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Logger</source>
        <translation>Logger</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lyrics</source>
        <translation>Lyrics</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>Minimize</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minutes</source>
        <translation>Minutes</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing</source>
        <translation>Missing</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Modified</source>
        <translation>Modified</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Months</source>
        <translation>Months</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute</source>
        <translation>Mute</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mute off</source>
        <translation>Mute off</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New</source>
        <translation>New</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next page</source>
        <translation>Next page</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Next track</source>
        <translation>Next track</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>No albums</source>
        <translation>No albums</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+144"/>
        <source>Tracks</source>
        <translation>Tracks</translation>
    </message>
    <message>
        <location line="-142"/>
        <source>Move down</source>
        <translation>Move down</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Move up</source>
        <translation>Move up</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>on</source>
        <translation>on</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open directory</source>
        <translation>Open directory</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open file</source>
        <translation>Open file</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>or</source>
        <translation>or</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Overwrite</source>
        <translation>Overwrite</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play</source>
        <translation>Play</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playing time</source>
        <translation>Playing time</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play in new tab</source>
        <translation>Play in new tab</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlist</source>
        <translation>Playlist</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Playlists</source>
        <translation>Playlists</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play next</source>
        <translation>Play next</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Play/Pause</source>
        <translation>Play/Pause</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Podcasts</source>
        <translation>Podcasts</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous page</source>
        <translation>Previous page</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Previous track</source>
        <translation>Previous track</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Purchase Url</source>
        <translation>Purchase Url</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio</source>
        <translation>Radio</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Radio Station</source>
        <translation>Radio Station</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rating</source>
        <translation>Rating</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Really</source>
        <translation>Really</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Refresh</source>
        <translation>Refresh</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reload Library</source>
        <translation>Reload Library</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Rename</source>
        <translation>Rename</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat 1</source>
        <translation>Repeat 1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Repeat all</source>
        <translation>Repeat all</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Replace</source>
        <translation>Replace</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Retry</source>
        <translation>Retry</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reverse order</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Sampler</source>
        <translation>Sampler</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shuffle</source>
        <translation>Shuffle</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shutdown</source>
        <translation>Shutdown</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save as</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Save to file</source>
        <translation>Save to file</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan for audio files</source>
        <translation>Scan for audio files</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+2"/>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search next</source>
        <translation>Search next</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Search previous</source>
        <translation>Search previous</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>2nd</source>
        <translation>2nd</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seconds</source>
        <translation>Seconds</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek backward</source>
        <translation>Seek backward</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seek forward</source>
        <translation>Seek forward</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show</source>
        <translation>Show</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Album Artists</source>
        <translation>Show Album Artists</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Covers</source>
        <translation>Show Covers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Library</source>
        <translation>Show Library</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Similar artists</source>
        <translation>Similar artists</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by</source>
        <translation>Sort by</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stop</source>
        <translation>Stop</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streams</source>
        <translation>Streams</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream URL</source>
        <translation>Stream URL</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Success</source>
        <translation>Success</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>th</source>
        <translation>th</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3rd</source>
        <translation>3rd</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Title</source>
        <translation>Title</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track</source>
        <translation>Track</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Track number</source>
        <translation>Track number</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>track on</source>
        <translation>track on</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Tree</source>
        <translation>Tree</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Undo</source>
        <translation>Undo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown album</source>
        <translation>Unknown album</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown artist</source>
        <translation>Unknown artist</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown title</source>
        <translation>Unknown title</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown genre</source>
        <translation>Unknown genre</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unknown placeholder</source>
        <translation>Unknown placeholder</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown year</source>
        <translation>Unknown year</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Various</source>
        <translation>Various</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various albums</source>
        <translation>Various albums</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various artists</source>
        <translation>Various artists</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Various tracks</source>
        <translation>Various tracks</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume down</source>
        <translation>Volume down</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Volume up</source>
        <translation>Volume up</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Weeks</source>
        <translation>Weeks</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Year</source>
        <translation>Year</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Years</source>
        <translation>Years</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>Zoom</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>No directories</source>
        <translation>No directories</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n directory(s)</source>
        <translation><numerusform>1 directory</numerusform><numerusform>%n directories</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No files</source>
        <translation>No files</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n file(s)</source>
        <translation><numerusform>1 file</numerusform><numerusform>%n files</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No playlists</source>
        <translation>No playlists</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n playlist(s)</source>
        <translation><numerusform>1 playlist</numerusform><numerusform>%n playlists</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks</source>
        <translation>No tracks</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s)</source>
        <translation><numerusform>1 track</numerusform><numerusform>%n tracks</numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>No tracks found</source>
        <translation>No tracks found</translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>%n track(s) found</source>
        <translation><numerusform>1 track found</numerusform><numerusform>%n tracks found</numerusform></translation>
    </message>
</context>
</TS>